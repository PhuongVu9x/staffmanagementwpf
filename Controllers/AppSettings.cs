﻿using Microsoft.Win32;
using Newtonsoft.Json;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using StaffManagement.Models.LisaDb.ModelPattern;
using StaffManagement.Models.LisaDb.ModelView;
using StaffManagement.Models.ModelPatterns;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ModelMaker = StaffManagement.Models.LisaDb.ModelPattern.ModelMaker;

namespace StaffManagement.Controllers
{
    internal class AppSettings
    {
        public static void Debug(object data) =>
            Console.WriteLine(JsonConvert.SerializeObject(data));

        public static string FileName(string name,string fromDate,string toDate, string path)
        {
            var textFormat = @"{0}\{1}-{2}-{3}.pdf";
            var fileStart = name.Replace(" ", "-");
            var fileStartDate = ConvertDate(fromDate);
            var fileEndDate = ConvertDate(toDate);

            return string.Format(textFormat, path, fileStart, fileStartDate, fileEndDate);
        }

        public static void ExportSalaryPdf(SalaryView salaryView, string fromDate, string toDate,string path)
        {
            var file = AppSettings.FileName(salaryView.Name, fromDate, toDate, path);
            if (File.Exists(file))
            {
                return;
            }
            var document = new PdfDocument();
            var page = document.AddPage();
            XGraphics gfx = XGraphics.FromPdfPage(page);
            XFont fontRegular = new XFont("Aria", 11, XFontStyle.Regular);
            XFont fontContent = new XFont("Aria", 13, XFontStyle.Regular);
            XFont fontItalic = new XFont("Aria", 11, XFontStyle.Italic);
            XFont fontBold = new XFont("Aria", 11, XFontStyle.Bold);
            XFont fontTitle = new XFont("Aria", 16, XFontStyle.Bold);
            XFont fontBoldHeader = new XFont("Aria", 11, XFontStyle.Bold);

            // Top left
            gfx.DrawString("ĐƠN VỊ", fontBold, XBrushes.Black, new XRect(40, 40, page.Width, page.Height), XStringFormats.TopLeft);
            gfx.DrawString(": CÔNG TI CỔ PHÂN INCOTRADE", fontRegular, XBrushes.Black, new XRect(85, 40, page.Width, page.Height), XStringFormats.TopLeft);

            gfx.DrawString("ĐMST", fontBold, XBrushes.Black, new XRect(40, 55, page.Width, page.Height), XStringFormats.TopLeft);
            gfx.DrawString(": 0317126724", fontRegular, XBrushes.Black, new XRect(75, 55, page.Width, page.Height), XStringFormats.TopLeft);

            gfx.DrawString("ĐỊA CHỈ", fontBold, XBrushes.Black, new XRect(40, 70, page.Width, page.Height), XStringFormats.TopLeft);
            gfx.DrawString(": SỐ 3, ĐƯỜNG 53, KDC TÂN QUY", fontRegular, XBrushes.Black, new XRect(85, 70, page.Width, page.Height), XStringFormats.TopLeft);
            gfx.DrawString("ĐÔNG, P. TÂN PHONG, Q7", fontRegular, XBrushes.Black, new XRect(40, 85, page.Width, page.Height), XStringFormats.TopLeft);

            // Top right
            gfx.DrawString("Mẫu số: C41-BB", fontBold, XBrushes.Black, new XRect(-100, 40, page.Width, page.Height), XStringFormats.TopRight);
            gfx.DrawString("(Ban hành kèm theo Thông tư số", fontItalic, XBrushes.Black, new XRect(-55, 55, page.Width, page.Height), XStringFormats.TopRight);
            gfx.DrawString("79/2019/TT-BTC", fontItalic, XBrushes.Black, new XRect(-100, 70, page.Width, page.Height), XStringFormats.TopRight);
            gfx.DrawString("ngày 12/11/2019 của Bộ Tài Chính)", fontItalic, XBrushes.Black, new XRect(-50, 85, page.Width, page.Height), XStringFormats.TopRight);

            // Header
            gfx.DrawString("PHIẾU CHI", fontTitle, XBrushes.Black, new XRect(0, 120, page.Width, page.Height), XStringFormats.TopCenter);
            gfx.DrawString($"Ngày: ", fontRegular, XBrushes.Black, new XRect(200, 135, page.Width, page.Height), XStringFormats.TopLeft);
            gfx.DrawString($"Nợ: ", fontRegular, XBrushes.Black, new XRect(-200, 135, page.Width, page.Height), XStringFormats.TopRight);
            gfx.DrawString($"Số: ", fontRegular, XBrushes.Black, new XRect(200, 150, page.Width, page.Height), XStringFormats.TopLeft);
            gfx.DrawString($"Có: ", fontRegular, XBrushes.Black, new XRect(-200, 150, page.Width, page.Height), XStringFormats.TopRight);

            gfx.DrawString($"Họ & Tên: {salaryView.Name}", fontContent, XBrushes.Black, new XRect(40, 180, page.Width, page.Height), XStringFormats.TopLeft);
            gfx.DrawString($"Địa chỉ: {salaryView.Address}", fontContent, XBrushes.Black, new XRect(40, 200, page.Width, page.Height), XStringFormats.TopLeft);
            gfx.DrawString($"Nội dung: Lương tháng {DateTime.Now.Month}", fontContent, XBrushes.Black, new XRect(40, 220, page.Width, page.Height), XStringFormats.TopLeft);
            gfx.DrawString($"Đã nhận số tiền: {salaryView.TotalSalary}", fontContent, XBrushes.Black, new XRect(40, 240, page.Width, page.Height), XStringFormats.TopLeft);
            gfx.DrawString($"(Viết bằng chữ): {MoneyToString(salaryView.CurrentSalary)}", fontContent, XBrushes.Black, new XRect(40, 260, page.Width, page.Height), XStringFormats.TopLeft);

            // Sign
            gfx.DrawString("KẾ TOÁN TRƯỞNG", fontBoldHeader, XBrushes.Black, new XRect(50, 280, page.Width, page.Height), XStringFormats.TopLeft);
            gfx.DrawString("(Kí,họ tên) ", fontContent, XBrushes.Black, new XRect(70, 295, page.Width, page.Height), XStringFormats.TopLeft);

            gfx.DrawString("THỦ QUỸ", fontBoldHeader, XBrushes.Black, new XRect(0, 280, page.Width, page.Height), XStringFormats.TopCenter);
            gfx.DrawString("(Kí,họ tên) ", fontContent, XBrushes.Black, new XRect(0, 295, page.Width, page.Height), XStringFormats.TopCenter);

            gfx.DrawString("NGƯỜI NHẬN TIỀN", fontBoldHeader, XBrushes.Black, new XRect(-50, 280, page.Width, page.Height), XStringFormats.TopRight);
            gfx.DrawString("(Kí,họ tên) ", fontContent, XBrushes.Black, new XRect(-70, 295, page.Width, page.Height), XStringFormats.TopRight);
            
            //Save PDF File
            document.Save(file);

            // Open document in default PDF viewer app
            //Process.Start(file);

            var ls = ModelViewMaker.Instance.SearchSalarys(fromDate, toDate)
                                            .Select(item => new Models.LisaDb.Entities.Receipt(item, fromDate, toDate, path)).ToList();

            ModelMaker.Instance.CreateReceipt(ls);
        }

        public static void ExportSalaryPdf(List<SalaryView> salaryViews, string fromDate, string toDate,string path)
        {
            foreach (var item in salaryViews)
            {
                ExportSalaryPdf(item, fromDate, toDate, path);
            }
        }
        
        public static string MoneyToString(decimal inputNumber, bool suffix = true)
        {
            string[] unitNumbers = new string[] { "không", "một", "hai", "ba", "bốn", "năm", "sáu", "bảy", "tám", "chín" };
            string[] placeValues = new string[] { "", "nghìn", "triệu", "tỷ" };
            bool isNegative = false;

            // -12345678.3445435 => "-12345678"
            string sNumber = inputNumber.ToString("#");
            double number = Convert.ToDouble(sNumber);
            if (number < 0)
            {
                number = -number;
                sNumber = number.ToString();
                isNegative = true;
            }


            int ones, tens, hundreds;

            int positionDigit = sNumber.Length;   // last -> first

            string result = " ";


            if (positionDigit == 0)
                result = unitNumbers[0] + result;
            else
            {
                // 0:       ###
                // 1: nghìn ###,###
                // 2: triệu ###,###,###
                // 3: tỷ    ###,###,###,###
                int placeValue = 0;

                while (positionDigit > 0)
                {
                    // Check last 3 digits remain ### (hundreds tens ones)
                    tens = hundreds = -1;
                    ones = Convert.ToInt32(sNumber.Substring(positionDigit - 1, 1));
                    positionDigit--;
                    if (positionDigit > 0)
                    {
                        tens = Convert.ToInt32(sNumber.Substring(positionDigit - 1, 1));
                        positionDigit--;
                        if (positionDigit > 0)
                        {
                            hundreds = Convert.ToInt32(sNumber.Substring(positionDigit - 1, 1));
                            positionDigit--;
                        }
                    }

                    if ((ones > 0) || (tens > 0) || (hundreds > 0) || (placeValue == 3))
                        result = placeValues[placeValue] + result;

                    placeValue++;
                    if (placeValue > 3) placeValue = 1;

                    if ((ones == 1) && (tens > 1))
                        result = "một " + result;
                    else
                    {
                        if ((ones == 5) && (tens > 0))
                            result = "lăm " + result;
                        else if (ones > 0)
                            result = unitNumbers[ones] + " " + result;
                    }
                    if (tens < 0)
                        break;
                    else
                    {
                        if ((tens == 0) && (ones > 0)) result = "lẻ " + result;
                        if (tens == 1) result = "mười " + result;
                        if (tens > 1) result = unitNumbers[tens] + " mươi " + result;
                    }
                    if (hundreds < 0) break;
                    else
                    {
                        if ((hundreds > 0) || (tens > 0) || (ones > 0))
                            result = unitNumbers[hundreds] + " trăm " + result;
                    }
                    result = " " + result;
                }
            }
            result = result[0].ToString().ToUpper() +  result.Trim().Substring(1);
            if (isNegative) result = "Âm " + result;
            return result + (suffix ? " đồng chẵn" : "");
        }

        public static string ConvertDate(string date)
        {
            var tmpDate = DateTime.Parse(date);
            var textFormat = "{0}{1}{2}";
            var data = string.Format(textFormat, $"{tmpDate.Day:00}", $"{tmpDate.Month:00}", tmpDate.Year);

            return data;
        }

    }
}
