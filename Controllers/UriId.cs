﻿using System;

namespace StaffManagement.Controllers
{
    internal class UriId
    {
        private static readonly Lazy<UriId> lazy = new Lazy<UriId>(() => new UriId());
        public static UriId Instance => lazy.Value;

        private string _uri = @"F:\Aptech\CSharp\DeskApplication\StaffManagement\Models\LisaDb\Db\{0}";

        public string PositionTable => string.Format(_uri, "Position.txt");
        public string StaffTable => string.Format(_uri, "Staff.txt");
        public string ShilfTable => string.Format(_uri, "Shilf.txt");
        public string ShilfPmTable => string.Format(_uri, "ShilfPm.txt");
        public string TimekeepingTable => string.Format(_uri, "Timekeeping.txt");
        public string AppConfig => string.Format(_uri, "AppConfig.txt");
        public string ReceiptTable => string.Format(_uri, "Receipt.txt");
    }
}
