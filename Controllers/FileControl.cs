﻿using Newtonsoft.Json;
using System;
using System.IO;

namespace StaffManagement.Controllers
{
    internal class FileControl
    {
        private static readonly Lazy<FileControl> lazy = new Lazy<FileControl>(() => new FileControl());
        public static FileControl Instance => lazy.Value;

        public object Read<T>(string uri)
        {
            var json = File.ReadAllText(uri);
            try
            {
                return JsonConvert.DeserializeObject<T>(json);
            }
            catch (JsonSerializationException ex)
            {
                System.Console.WriteLine(ex);
                return null;
            }
        }

        public void Write(string uri, object data)
        {
            File.WriteAllText(uri, JsonConvert.SerializeObject(data));
        }
    }
}
