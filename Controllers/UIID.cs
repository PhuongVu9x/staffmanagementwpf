﻿using StaffManagement.Views.UserControls;
using System;

namespace StaffManagement.Controllers
{
    internal class UIID
    {
        private static readonly Lazy<UIID> lazy = new Lazy<UIID>(() => new UIID());
        public static UIID Instance => lazy.Value;

        public Navigator Navigator => new Navigator();
        public Shilfs Shilfs => new Shilfs();
        public Staffs Staffs => new Staffs();
        public Salary Salary => new Salary();
        public Receipt Receipts => new Receipt();
        public AccountView AccountView => new AccountView();
        public StaffView StaffView => new StaffView();
        public PositionView PositionView => new PositionView();
    }
}
