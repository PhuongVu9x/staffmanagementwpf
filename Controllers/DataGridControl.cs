﻿using MahApps.Metro.Controls;
using StaffManagement.Components;
using StaffManagement.Models.LisaDb.Entities;
using StaffManagement.Models.LisaDb.ModelView;
using StaffManagement.Models.ModelPatterns;
using StaffManagement.Models.ModelView;
using StaffManagement.Views.UserControls;
using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Position = StaffManagement.Models.LisaDb.Entities.Position;
using Receipt = StaffManagement.Models.LisaDb.Entities.Receipt;
using StaffView = StaffManagement.Models.LisaDb.ModelView.StaffView;

namespace StaffManagement.Controllers
{
    internal class DataGridControl
    {
        private static readonly Lazy<DataGridControl> lazy = new Lazy<DataGridControl>(() => new DataGridControl());
        public static DataGridControl Instance { get { return lazy.Value; } }

        private DataGrid _dgStaff;
        private DataGrid _dgShilfStaff;
        private DataGrid _dgShilfPmStaff;
        private DataGrid _dgHeader;
        private DataGrid _dgPosition;
        private DataGrid _dgSalary;
        private DataGrid _dgReceipt;
        private StackPanel _spShilfView;
        public DataGrid DgStaff { get { return _dgStaff; } set { _dgStaff = value; _dgStaff.IsReadOnly = true; } }
        public DataGrid DgShilfStaff { get { return _dgShilfStaff; } set { _dgShilfStaff = value; _dgShilfStaff.IsReadOnly = true; } }
        public DataGrid DgShilfPmStaff { get { return _dgShilfPmStaff; } set { _dgShilfPmStaff = value; _dgShilfPmStaff.IsReadOnly = true; } }
        public DataGrid DgHeader { get { return _dgHeader; } set { _dgHeader = value; _dgHeader.IsReadOnly = true; } }
        public DataGrid DgPosition { get { return _dgPosition; } set { _dgPosition = value; _dgPosition.IsReadOnly = true; } }
        public DataGrid DgSalary { get { return _dgSalary; } set { _dgSalary = value; _dgSalary.IsReadOnly = true; } }
        public DataGrid DgReceipt { get { return _dgReceipt; } set { _dgReceipt = value; _dgReceipt.IsReadOnly = true; } }
        public StackPanel SpShilfView { get { return _spShilfView; } set { _spShilfView = value; } }

        static public StackPanel SpShilf { get; set; }

        //public void AddAll(List<Position> data)
        //{
        //    if (data is null) return;
        //    _dgPosition.Items.Clear();

        //    foreach (var item in data)
        //        _dgPosition.Items.Add(item);
        //}

        public void AddAll(List<Position> data)
        {
            if (data is null) return;
            _dgPosition.Items.Clear();

            foreach (var item in data)
                _dgPosition.Items.Add(item);
        }

        public void AddLayoutDgShilf()
        {
            _spShilfView.Children.Clear();
            if (Shilfs.IsSwitchShilf)
                _spShilfView.Children.Add(new DgShilf());
            else
                _spShilfView.Children.Add(new DgShilfPm());
        }

        public void AddAll(List<Models.ModelView.StaffView> data)
        {

            if (data is null) return;
            //var date = EventControl.GetData<List<Models.ModelView.StaffView>>("Test2");
            //_dgShilfStaff.Items.Clear();
            //_dgShilfPmStaff.Items.Clear();
            
            _spShilfView.Invoke(new Action(() =>
            {
                _spShilfView.Children.Clear();
                if (Shilfs.IsSwitchShilf)
                    _spShilfView.Children.Add(new DgShilf(data));
                else
                    _spShilfView.Children.Add(new DgShilfPm(data));
            }));

            _dgHeader.Invoke(new Action(() =>
            {
                _dgHeader.Items.Clear();
                foreach (var item in data)
                {
                    //_dgShilfStaff.Items.Add(item);
                    //_dgShilfPmStaff.Items.Add(item);
                    _dgHeader.Items.Add(item);
                }
            }));
        }

        public void AddAll(List<Models.ModelView.StaffView> data, int id = 1)
        {
            if (data is null) return;
            _dgStaff.Items.Clear();

            foreach (var item in data)
                _dgStaff.Items.Add(item);
        }

        public void AddAll(List<SalaryView> data)
        {
            if (data is null) return;
            _dgSalary.Items.Clear();

            foreach (var item in data)
                _dgSalary.Items.Add(item);
        }

        public void AddAll(List<Receipt> data)
        {
            if (data is null) return;
            _dgReceipt.Items.Clear();

            foreach (var item in data)
                _dgReceipt.Items.Add(item);
        }
    }
}
