﻿using System;
using System.Windows;

namespace StaffManagement.Controllers
{
    internal class MessageControl
    {
        private static readonly Lazy<MessageControl> lazy = new Lazy<MessageControl>(() => new MessageControl());
        public static MessageControl Instance { get { return lazy.Value; } }

        public void Success()
        {
            MessageBox.Show("Successfull.");
        }

        public void Fail()
        {
            MessageBox.Show("Fail.");
        }

        public void Error()
        {
            MessageBox.Show("Error.");
        }

        public void Exists()
        {
            MessageBox.Show("Already Exists.");
        }
    }
}
