﻿using System;
using System.Windows.Controls;

namespace StaffManagement.Controllers
{
    internal class UIFlow
    {
        private static readonly Lazy<UIFlow> lazy = new Lazy<UIFlow>(() => new UIFlow());
        public static UIFlow Instance { get { return lazy.Value; } }

        private Grid _currentMainView;
        private Grid _currentSubView;

        public void SetupNavigator(Grid grid)
        {
            grid.Children.Clear();
            grid.Children.Add(UIID.Instance.Navigator);
        }

        public void SetupMainView(Grid grid)
        {
            _currentMainView = grid;
        }

        public void SwitchMainView(UserControl page)
        {
            _currentMainView.Children.Clear();
            _currentMainView.Children.Add(page);
        }

        public void SetupSubView(Grid page) => _currentSubView = page;

        public void SwitchSubView(UserControl page)
        {
            _currentSubView.Children.Clear();
            _currentSubView.Children.Add(page);
        }
    }
}
