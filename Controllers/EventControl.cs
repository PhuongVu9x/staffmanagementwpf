﻿using Newtonsoft.Json;
using System;
using System.Windows;

namespace StaffManagement.Controllers
{
    internal class EventControl
    {
        private static Lazy<EventControl> lazy = new Lazy<EventControl>(() => new EventControl());
        public static EventControl Instance => lazy.Value;

        static private string uriFormat = @"C:\{0}";

        static public void EmitData(string eventName, object data)
        {
            var json = JsonConvert.SerializeObject(data);
            App.SetCookie(GetUri(eventName), json);
            //if (Application.Current.Resources.Contains(eventName))
            //    Application.Current.Resources.Remove(eventName);
            //Application.Current.Resources.Add(eventName, json);
        }

        static public T GetData<T>(string eventName)
        {
            var json = App.GetCookie(GetUri(eventName));
            //string json = (string)Application.Current.Resources[eventName];

            object data = null;
            try
            {
                data = JsonConvert.DeserializeObject<T>(json);
            }
            catch (Exception)
            {
                return (T)data;
            }
            return (T)data;
        }

        static private Uri GetUri(string eventName)
        {
            var uriName = string.Format(uriFormat, eventName);
            return new Uri(uriName);
        }
    }
}
