﻿using MahApps.Metro.Controls;
using StaffManagement.Controllers;
using StaffManagement.Models.LisaDb.Entities;
using StaffManagement.Models.ModelPatterns;
using StaffManagement.Settings;
using StaffManagement.Views.UserControls;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using Orientation = System.Windows.Controls.Orientation;
using TextBox = System.Windows.Controls.TextBox;
using ModelMaker = StaffManagement.Models.LisaDb.ModelPattern.ModelMaker;
using System;
using System.Text.RegularExpressions;

namespace StaffManagement.Components
{
    internal class DgShilfPm : DataGrid
    {
        public DgShilfPm()
        {
            IsReadOnly = true;
            Background = null;
            Height = 505;

            HorizontalScrollBarVisibility = ScrollBarVisibility.Hidden;
            VerticalScrollBarVisibility = ScrollBarVisibility.Hidden;

            // Id
            DataGridTemplateColumn idTc = new DataGridTemplateColumn();
            idTc.Header = "Id";
            idTc.Width = 60;
            idTc.CanUserResize = false;
            idTc.CanUserSort = true;
            idTc.SortDirection = ListSortDirection.Descending;
            idTc.SortMemberPath = "Code";
            FrameworkElementFactory idSp = new FrameworkElementFactory(typeof(StackPanel));
            idSp.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);

            FrameworkElementFactory idText = new FrameworkElementFactory(typeof(TextBlock));
            idText.SetBinding(TextBlock.TextProperty, new Binding("Code"));
            idText.SetValue(TextBlock.TextWrappingProperty, TextWrapping.Wrap);
            idText.SetValue(TextBlock.TextAlignmentProperty, TextAlignment.Center);

            idSp.AppendChild(idText);

            var idDt = new DataTemplate();
            idDt.VisualTree = idSp;
            idDt.Resources.Add(idSp, null);
            idTc.CellTemplate = idDt;

            Columns.Add(idTc);

            // Name
            DataGridTemplateColumn nameTc = new DataGridTemplateColumn();
            nameTc.Header = "Name";
            nameTc.Width = 130;
            nameTc.CanUserResize = false;
            nameTc.CanUserSort = true;
            nameTc.SortDirection = ListSortDirection.Descending;
            nameTc.SortMemberPath = "Name";
            FrameworkElementFactory nameSp = new FrameworkElementFactory(typeof(StackPanel));
            nameSp.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);

            FrameworkElementFactory nameText = new FrameworkElementFactory(typeof(TextBlock));
            nameText.SetBinding(TextBlock.TextProperty, new Binding("Name"));
            nameText.SetValue(TextBlock.TextWrappingProperty, TextWrapping.Wrap);
            nameText.SetValue(TextBlock.TextAlignmentProperty, TextAlignment.Center);

            nameSp.AppendChild(nameText);

            var nameDt = new DataTemplate();
            nameDt.VisualTree = nameSp;
            nameDt.Resources.Add(nameSp, null);
            nameTc.CellTemplate = nameDt;

            Columns.Add(nameTc);

            // Position
            DataGridTemplateColumn posTc = new DataGridTemplateColumn();
            posTc.Header = "Position";
            posTc.Width = 90;
            posTc.CanUserResize = false;
            posTc.CanUserSort = true;
            posTc.SortDirection = ListSortDirection.Descending;
            posTc.SortMemberPath = "PositionName";
            FrameworkElementFactory posSp = new FrameworkElementFactory(typeof(StackPanel));
            posSp.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);

            FrameworkElementFactory posText = new FrameworkElementFactory(typeof(TextBlock));
            posText.SetBinding(TextBlock.TextProperty, new Binding("PositionName"));
            posText.SetValue(TextBlock.TextWrappingProperty, TextWrapping.Wrap);
            posText.SetValue(TextBlock.TextAlignmentProperty, TextAlignment.Center);

            posSp.AppendChild(posText);

            var postDt = new DataTemplate();
            postDt.VisualTree = posSp;
            postDt.Resources.Add(posSp, null);
            posTc.CellTemplate = postDt;

            Columns.Add(posTc);

            // MondayHours
            DataGridTemplateColumn mhTc = new DataGridTemplateColumn();
            mhTc.Header = "H";
            mhTc.Width = 50;
            mhTc.CanUserResize = false;
            mhTc.CanUserSort = true;
            mhTc.SortDirection = ListSortDirection.Descending;
            mhTc.SortMemberPath = "MondayHours";
            FrameworkElementFactory mhSp = new FrameworkElementFactory(typeof(StackPanel));
            mhSp.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);

            FrameworkElementFactory mhText = new FrameworkElementFactory(typeof(TextBlock));
            mhText.SetBinding(TextBlock.TextProperty, new Binding("MondayHours"));
            mhSp.AppendChild(mhText);

            var mhDt = new DataTemplate();
            mhDt.VisualTree = mhSp;
            mhDt.Resources.Add(mhSp, null);
            mhTc.CellTemplate = mhDt;

            Columns.Add(mhTc);

            // MondayTime
            DataGridTemplateColumn mtTc = new DataGridTemplateColumn();
            mtTc.Header = "Time";
            mtTc.Width = 70;
            mtTc.CanUserResize = false;
            mtTc.CanUserSort = true;
            mtTc.SortDirection = ListSortDirection.Descending;
            mtTc.SortMemberPath = "MondayTimePm";
            FrameworkElementFactory mtParentSp = new FrameworkElementFactory(typeof(StackPanel));
            mtParentSp.SetValue(MarginProperty, new Thickness(-15, -40, -15, -15));
            mtParentSp.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);

            FrameworkElementFactory mtMonShilfIdText = new FrameworkElementFactory(typeof(TextBlock));
            mtMonShilfIdText.SetBinding(TextBlock.TextProperty, new Binding("MondayShilfIdPm"));
            mtMonShilfIdText.SetValue(NameProperty, "tbShilfId");
            mtMonShilfIdText.SetValue(VisibilityProperty, Visibility.Hidden);
            mtParentSp.AppendChild(mtMonShilfIdText);

            FrameworkElementFactory mtAccIdText = new FrameworkElementFactory(typeof(TextBlock));
            mtAccIdText.SetBinding(TextBlock.TextProperty, new Binding("Id"));
            mtAccIdText.SetValue(NameProperty, "tbStaffId");
            mtAccIdText.SetValue(VisibilityProperty, Visibility.Hidden);
            mtParentSp.AppendChild(mtAccIdText);

            FrameworkElementFactory mtTimeSp = new FrameworkElementFactory(typeof(StackPanel));
            mtTimeSp.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);
            FrameworkElementFactory mtTimeTb = new FrameworkElementFactory(typeof(TextBox));
            mtTimeTb.SetBinding(TextBox.TextProperty, new Binding("MondayTimePm"));
            mtTimeTb.SetBinding(BackgroundProperty, new Binding("MondayColorPm"));
            mtTimeTb.SetValue(TextBox.TextAlignmentProperty, TextAlignment.Center);
            mtTimeTb.SetValue(MarginProperty, new Thickness(0, 0, 0, 0));
            mtTimeTb.SetValue(HeightProperty, 30d);
            mtTimeTb.SetValue(WidthProperty, 70d);
            mtTimeTb.AddHandler(KeyDownEvent, new KeyEventHandler(DateShilfChange));
            mtTimeSp.AppendChild(mtTimeTb);
            mtParentSp.AppendChild(mtTimeSp);

            FrameworkElementFactory mtSpecialSp1 = new FrameworkElementFactory(typeof(StackPanel));
            mtSpecialSp1.SetValue(StackPanel.OrientationProperty, Orientation.Horizontal);
            mtSpecialSp1.SetValue(HeightProperty, 30d);
            FrameworkElementFactory mtSpecialSp2 = new FrameworkElementFactory(typeof(StackPanel));
            mtSpecialSp2.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);
            FrameworkElementFactory mtLateTb = new FrameworkElementFactory(typeof(TextBox));
            mtLateTb.SetBinding(TextBox.TextProperty, new Binding("MondayLatePm"));
            mtLateTb.AddHandler(KeyDownEvent, new KeyEventHandler(DateLateChange));
            mtSpecialSp2.AppendChild(mtLateTb);
            FrameworkElementFactory mtSpecialCb = new FrameworkElementFactory(typeof(CheckBox));
            mtSpecialCb.SetBinding(CheckBox.IsCheckedProperty, new Binding("MondaySpecialPm"));
            mtSpecialCb.SetValue(MarginProperty, new Thickness(50, -30, 0, 0));
            mtSpecialCb.AddHandler(Button.ClickEvent, new RoutedEventHandler(DateSpecialChange));
            FrameworkElementFactory mtShilfIdTb = new FrameworkElementFactory(typeof(TextBlock));
            mtShilfIdTb.SetValue(VisibilityProperty, Visibility.Hidden);
            mtSpecialCb.AppendChild(mtShilfIdTb);
            mtSpecialSp2.AppendChild(mtSpecialCb);
            mtSpecialSp1.AppendChild(mtSpecialSp2);
            mtParentSp.AppendChild(mtSpecialSp1);

            var mtDt = new DataTemplate();
            mtDt.VisualTree = mtParentSp;
            mtDt.Resources.Add(mtParentSp, null);
            mtTc.CellTemplate = mtDt;

            Columns.Add(mtTc);

            // TuesdayHours
            DataGridTemplateColumn thTc = new DataGridTemplateColumn();
            thTc.Header = "H";
            thTc.Width = 50;
            thTc.CanUserResize = false;
            thTc.CanUserSort = true;
            thTc.SortDirection = ListSortDirection.Descending;
            thTc.SortMemberPath = "TuesdayHours";
            FrameworkElementFactory thSp = new FrameworkElementFactory(typeof(StackPanel));
            thSp.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);

            FrameworkElementFactory thText = new FrameworkElementFactory(typeof(TextBlock));
            thText.SetBinding(TextBlock.TextProperty, new Binding("TuesdayHours"));
            thSp.AppendChild(thText);

            var thDt = new DataTemplate();
            thDt.VisualTree = thSp;
            thDt.Resources.Add(thSp, null);
            thTc.CellTemplate = thDt;

            Columns.Add(thTc);

            // TuesdayTime
            DataGridTemplateColumn ttTc = new DataGridTemplateColumn();
            ttTc.Header = "Time";
            ttTc.Width = 70;
            ttTc.CanUserResize = false;
            ttTc.CanUserSort = true;
            ttTc.SortDirection = ListSortDirection.Descending;
            ttTc.SortMemberPath = "TuesdayTimePm";
            FrameworkElementFactory ttParentSp = new FrameworkElementFactory(typeof(StackPanel));
            ttParentSp.SetValue(MarginProperty, new Thickness(-15, -40, -15, -15));
            ttParentSp.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);

            FrameworkElementFactory ttMonShilfIdText = new FrameworkElementFactory(typeof(TextBlock));
            ttMonShilfIdText.SetBinding(TextBlock.TextProperty, new Binding("TuesdayShilfIdPm"));
            ttMonShilfIdText.SetValue(VisibilityProperty, Visibility.Hidden);
            ttParentSp.AppendChild(ttMonShilfIdText);

            FrameworkElementFactory ttAccIdText = new FrameworkElementFactory(typeof(TextBlock));
            ttAccIdText.SetBinding(TextBlock.TextProperty, new Binding("Id"));
            ttAccIdText.SetValue(NameProperty, "tbStaffId");
            ttAccIdText.SetValue(VisibilityProperty, Visibility.Hidden);
            ttParentSp.AppendChild(ttAccIdText);

            FrameworkElementFactory ttTimeSp = new FrameworkElementFactory(typeof(StackPanel));
            ttTimeSp.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);
            FrameworkElementFactory ttTimeTb = new FrameworkElementFactory(typeof(TextBox));
            ttTimeTb.SetBinding(TextBox.TextProperty, new Binding("TuesdayTimePm"));
            ttTimeTb.SetBinding(BackgroundProperty, new Binding("TuesdayColorPm"));
            ttTimeTb.SetValue(TextBox.TextAlignmentProperty, TextAlignment.Center);
            ttTimeTb.SetValue(MarginProperty, new Thickness(0, 0, 0, 0));
            ttTimeTb.SetValue(HeightProperty, 30d);
            ttTimeTb.SetValue(WidthProperty, 70d);
            ttTimeTb.AddHandler(KeyDownEvent, new KeyEventHandler(DateShilfChange));
            ttTimeSp.AppendChild(ttTimeTb);
            ttParentSp.AppendChild(ttTimeSp);

            FrameworkElementFactory ttSpecialSp1 = new FrameworkElementFactory(typeof(StackPanel));
            ttSpecialSp1.SetValue(StackPanel.OrientationProperty, Orientation.Horizontal);
            ttSpecialSp1.SetValue(HeightProperty, 30d);
            FrameworkElementFactory ttSpecialSp2 = new FrameworkElementFactory(typeof(StackPanel));
            ttSpecialSp2.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);
            FrameworkElementFactory ttLateTb = new FrameworkElementFactory(typeof(TextBox));
            ttLateTb.SetBinding(TextBox.TextProperty, new Binding("TuesdayLatePm"));
            ttLateTb.AddHandler(KeyDownEvent, new KeyEventHandler(DateLateChange));
            ttSpecialSp2.AppendChild(ttLateTb);
            FrameworkElementFactory ttSpecialCb = new FrameworkElementFactory(typeof(CheckBox));
            ttSpecialCb.SetBinding(CheckBox.IsCheckedProperty, new Binding("TuesdaySpecialPm"));
            ttSpecialCb.SetValue(MarginProperty, new Thickness(50, -30, 0, 0));
            ttSpecialCb.AddHandler(Button.ClickEvent, new RoutedEventHandler(DateSpecialChange));
            FrameworkElementFactory ttShilfIdTb = new FrameworkElementFactory(typeof(TextBlock));
            ttShilfIdTb.SetValue(VisibilityProperty, Visibility.Hidden);
            ttSpecialCb.AppendChild(ttShilfIdTb);
            ttSpecialSp2.AppendChild(ttSpecialCb);
            ttSpecialSp1.AppendChild(ttSpecialSp2);
            ttParentSp.AppendChild(ttSpecialSp1);

            var ttDt = new DataTemplate();
            ttDt.VisualTree = ttParentSp;
            ttDt.Resources.Add(ttParentSp, null);
            ttTc.CellTemplate = ttDt;

            Columns.Add(ttTc);

            // WednesdayHours
            DataGridTemplateColumn whTc = new DataGridTemplateColumn();
            whTc.Header = "H";
            whTc.Width = 50;
            whTc.CanUserResize = false;
            whTc.CanUserSort = true;
            whTc.SortDirection = ListSortDirection.Descending;
            whTc.SortMemberPath = "WednesdayHours";
            FrameworkElementFactory whSp = new FrameworkElementFactory(typeof(StackPanel));
            whSp.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);

            FrameworkElementFactory whText = new FrameworkElementFactory(typeof(TextBlock));
            whText.SetBinding(TextBlock.TextProperty, new Binding("WednesdayHours"));
            whSp.AppendChild(whText);

            var whDt = new DataTemplate();
            whDt.VisualTree = whSp;
            whDt.Resources.Add(whSp, null);
            whTc.CellTemplate = whDt;

            Columns.Add(whTc);

            // WednesdayTime
            DataGridTemplateColumn wtTc = new DataGridTemplateColumn();
            wtTc.Header = "Time";
            wtTc.Width = 70;
            wtTc.CanUserResize = false;
            wtTc.CanUserSort = true;
            wtTc.SortDirection = ListSortDirection.Descending;
            wtTc.SortMemberPath = "WednesdayTimePm";
            FrameworkElementFactory wtParentSp = new FrameworkElementFactory(typeof(StackPanel));
            wtParentSp.SetValue(MarginProperty, new Thickness(-15, -40, -15, -15));
            wtParentSp.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);

            FrameworkElementFactory wtMonShilfIdText = new FrameworkElementFactory(typeof(TextBlock));
            wtMonShilfIdText.SetBinding(TextBlock.TextProperty, new Binding("WednesdayShilfIdPm"));
            wtMonShilfIdText.SetValue(VisibilityProperty, Visibility.Hidden);
            wtParentSp.AppendChild(wtMonShilfIdText);

            FrameworkElementFactory wtAccIdText = new FrameworkElementFactory(typeof(TextBlock));
            wtAccIdText.SetBinding(TextBlock.TextProperty, new Binding("Id"));
            wtAccIdText.SetValue(NameProperty, "tbStaffId");
            wtAccIdText.SetValue(VisibilityProperty, Visibility.Hidden);
            wtParentSp.AppendChild(wtAccIdText);

            FrameworkElementFactory wtTimeSp = new FrameworkElementFactory(typeof(StackPanel));
            wtTimeSp.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);
            FrameworkElementFactory wtTimeTb = new FrameworkElementFactory(typeof(TextBox));
            wtTimeTb.SetBinding(TextBox.TextProperty, new Binding("WednesdayTimePm"));
            wtTimeTb.SetBinding(BackgroundProperty, new Binding("WednesdayColorPm"));
            wtTimeTb.SetValue(TextBox.TextAlignmentProperty, TextAlignment.Center);
            wtTimeTb.SetValue(MarginProperty, new Thickness(0, 0, 0, 0));
            wtTimeTb.SetValue(HeightProperty, 30d);
            wtTimeTb.SetValue(WidthProperty, 70d);
            wtTimeTb.AddHandler(KeyDownEvent, new KeyEventHandler(DateShilfChange));
            wtTimeSp.AppendChild(wtTimeTb);
            wtParentSp.AppendChild(wtTimeSp);

            FrameworkElementFactory wtSpecialSp1 = new FrameworkElementFactory(typeof(StackPanel));
            wtSpecialSp1.SetValue(StackPanel.OrientationProperty, Orientation.Horizontal);
            wtSpecialSp1.SetValue(HeightProperty, 30d);
            FrameworkElementFactory wtSpecialSp2 = new FrameworkElementFactory(typeof(StackPanel));
            wtSpecialSp2.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);
            FrameworkElementFactory wtLateTb = new FrameworkElementFactory(typeof(TextBox));
            wtLateTb.SetBinding(TextBox.TextProperty, new Binding("WednesdayLatePm"));
            wtLateTb.AddHandler(KeyDownEvent, new KeyEventHandler(DateLateChange));
            wtSpecialSp2.AppendChild(wtLateTb);
            FrameworkElementFactory wtSpecialCb = new FrameworkElementFactory(typeof(CheckBox));
            wtSpecialCb.SetBinding(CheckBox.IsCheckedProperty, new Binding("WednesdaySpecialPm"));
            wtSpecialCb.SetValue(MarginProperty, new Thickness(50, -30, 0, 0));
            wtSpecialCb.AddHandler(Button.ClickEvent, new RoutedEventHandler(DateSpecialChange));
            FrameworkElementFactory wtShilfIdTb = new FrameworkElementFactory(typeof(TextBlock));
            wtShilfIdTb.SetValue(VisibilityProperty, Visibility.Hidden);
            wtSpecialCb.AppendChild(wtShilfIdTb);
            wtSpecialSp2.AppendChild(wtSpecialCb);
            wtSpecialSp1.AppendChild(wtSpecialSp2);
            wtParentSp.AppendChild(wtSpecialSp1);

            var wtDt = new DataTemplate();
            wtDt.VisualTree = wtParentSp;
            wtDt.Resources.Add(wtParentSp, null);
            wtTc.CellTemplate = wtDt;

            Columns.Add(wtTc);

            // ThursdayHours
            DataGridTemplateColumn thhTc = new DataGridTemplateColumn();
            thhTc.Header = "H";
            thhTc.Width = 50;
            thhTc.CanUserResize = false;
            thhTc.CanUserSort = true;
            thhTc.SortDirection = ListSortDirection.Descending;
            thhTc.SortMemberPath = "ThursdayHours";
            FrameworkElementFactory thhSp = new FrameworkElementFactory(typeof(StackPanel));
            thhSp.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);

            FrameworkElementFactory thhText = new FrameworkElementFactory(typeof(TextBlock));
            thhText.SetBinding(TextBlock.TextProperty, new Binding("ThursdayHours"));
            thhSp.AppendChild(thhText);

            var thhDt = new DataTemplate();
            thhDt.VisualTree = thhSp;
            thhDt.Resources.Add(thhSp, null);
            thhTc.CellTemplate = thhDt;

            Columns.Add(thhTc);

            // ThursdayTime
            DataGridTemplateColumn thtTc = new DataGridTemplateColumn();
            thtTc.Header = "Time";
            thtTc.Width = 70;
            thtTc.CanUserResize = false;
            thtTc.CanUserSort = true;
            thtTc.SortDirection = ListSortDirection.Descending;
            thtTc.SortMemberPath = "ThursdayTimePm";
            FrameworkElementFactory thtParentSp = new FrameworkElementFactory(typeof(StackPanel));
            thtParentSp.SetValue(MarginProperty, new Thickness(-15, -40, -15, -15));
            thtParentSp.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);

            FrameworkElementFactory thtMonShilfIdText = new FrameworkElementFactory(typeof(TextBlock));
            thtMonShilfIdText.SetBinding(TextBlock.TextProperty, new Binding("ThursdayShilfIdPm"));
            thtMonShilfIdText.SetValue(VisibilityProperty, Visibility.Hidden);
            thtParentSp.AppendChild(thtMonShilfIdText);

            FrameworkElementFactory thtAccIdText = new FrameworkElementFactory(typeof(TextBlock));
            thtAccIdText.SetBinding(TextBlock.TextProperty, new Binding("Id"));
            thtAccIdText.SetValue(NameProperty, "tbStaffId");
            thtAccIdText.SetValue(VisibilityProperty, Visibility.Hidden);
            thtParentSp.AppendChild(thtAccIdText);

            FrameworkElementFactory thtTimeSp = new FrameworkElementFactory(typeof(StackPanel));
            thtTimeSp.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);
            FrameworkElementFactory thtTimeTb = new FrameworkElementFactory(typeof(TextBox));
            thtTimeTb.SetBinding(TextBox.TextProperty, new Binding("ThursdayTimePm"));
            thtTimeTb.SetBinding(BackgroundProperty, new Binding("ThursdayColorPm"));
            thtTimeTb.SetValue(TextBox.TextAlignmentProperty, TextAlignment.Center);
            thtTimeTb.SetValue(MarginProperty, new Thickness(0, 0, 0, 0));
            thtTimeTb.SetValue(HeightProperty, 30d);
            thtTimeTb.SetValue(WidthProperty, 70d);
            thtTimeTb.AddHandler(KeyDownEvent, new KeyEventHandler(DateShilfChange));
            thtTimeSp.AppendChild(thtTimeTb);
            thtParentSp.AppendChild(thtTimeSp);

            FrameworkElementFactory thtSpecialSp1 = new FrameworkElementFactory(typeof(StackPanel));
            thtSpecialSp1.SetValue(StackPanel.OrientationProperty, Orientation.Horizontal);
            thtSpecialSp1.SetValue(HeightProperty, 30d);
            FrameworkElementFactory thtSpecialSp2 = new FrameworkElementFactory(typeof(StackPanel));
            thtSpecialSp2.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);
            FrameworkElementFactory thtLateTb = new FrameworkElementFactory(typeof(TextBox));
            thtLateTb.SetBinding(TextBox.TextProperty, new Binding("ThursdayLatePm"));
            thtLateTb.AddHandler(KeyDownEvent, new KeyEventHandler(DateLateChange));
            thtSpecialSp2.AppendChild(thtLateTb);
            FrameworkElementFactory thtSpecialCb = new FrameworkElementFactory(typeof(CheckBox));
            thtSpecialCb.SetBinding(CheckBox.IsCheckedProperty, new Binding("ThursdaySpecialPm"));
            thtSpecialCb.SetValue(MarginProperty, new Thickness(50, -30, 0, 0));
            thtSpecialCb.AddHandler(Button.ClickEvent, new RoutedEventHandler(DateSpecialChange));
            FrameworkElementFactory thtShilfIdTb = new FrameworkElementFactory(typeof(TextBlock));
            thtShilfIdTb.SetValue(VisibilityProperty, Visibility.Hidden);
            thtSpecialCb.AppendChild(thtShilfIdTb);
            thtSpecialSp2.AppendChild(thtSpecialCb);
            thtSpecialSp1.AppendChild(thtSpecialSp2);
            thtParentSp.AppendChild(thtSpecialSp1);

            var thtDt = new DataTemplate();
            thtDt.VisualTree = thtParentSp;
            thtDt.Resources.Add(thtParentSp, null);
            thtTc.CellTemplate = thtDt;

            Columns.Add(thtTc);

            // FridayHours
            DataGridTemplateColumn fhTc = new DataGridTemplateColumn();
            fhTc.Header = "H";
            fhTc.Width = 50;
            fhTc.CanUserResize = false;
            fhTc.CanUserSort = true;
            fhTc.SortDirection = ListSortDirection.Descending;
            fhTc.SortMemberPath = "FridayHours";
            FrameworkElementFactory fhSp = new FrameworkElementFactory(typeof(StackPanel));
            fhSp.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);

            FrameworkElementFactory fhText = new FrameworkElementFactory(typeof(TextBlock));
            fhText.SetBinding(TextBlock.TextProperty, new Binding("FridayHours"));
            fhSp.AppendChild(fhText);

            var fhDt = new DataTemplate();
            fhDt.VisualTree = fhSp;
            fhDt.Resources.Add(fhSp, null);
            fhTc.CellTemplate = fhDt;

            Columns.Add(fhTc);

            // FridayTime
            DataGridTemplateColumn ftTc = new DataGridTemplateColumn();
            ftTc.Header = "Time";
            ftTc.Width = 70;
            ftTc.CanUserResize = false;
            ftTc.CanUserSort = true;
            ftTc.SortDirection = ListSortDirection.Descending;
            ftTc.SortMemberPath = "FridayTimePm";
            FrameworkElementFactory ftParentSp = new FrameworkElementFactory(typeof(StackPanel));
            ftParentSp.SetValue(MarginProperty, new Thickness(-15, -40, -15, -15));
            ftParentSp.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);

            FrameworkElementFactory ftMonShilfIdText = new FrameworkElementFactory(typeof(TextBlock));
            ftMonShilfIdText.SetBinding(TextBlock.TextProperty, new Binding("FridayShilfIdPm"));
            ftMonShilfIdText.SetValue(VisibilityProperty, Visibility.Hidden);
            ftParentSp.AppendChild(ftMonShilfIdText);

            FrameworkElementFactory ftAccIdText = new FrameworkElementFactory(typeof(TextBlock));
            ftAccIdText.SetBinding(TextBlock.TextProperty, new Binding("Id"));
            ftAccIdText.SetValue(NameProperty, "tbStaffId");
            ftAccIdText.SetValue(VisibilityProperty, Visibility.Hidden);
            ftParentSp.AppendChild(ftAccIdText);

            FrameworkElementFactory ftTimeSp = new FrameworkElementFactory(typeof(StackPanel));
            ftTimeSp.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);
            FrameworkElementFactory ftTimeTb = new FrameworkElementFactory(typeof(TextBox));
            ftTimeTb.SetBinding(TextBox.TextProperty, new Binding("FridayTimePm"));
            ftTimeTb.SetBinding(BackgroundProperty, new Binding("FridayColorPm"));
            ftTimeTb.SetValue(TextBox.TextAlignmentProperty, TextAlignment.Center);
            ftTimeTb.SetValue(MarginProperty, new Thickness(0, 0, 0, 0));
            ftTimeTb.SetValue(HeightProperty, 30d);
            ftTimeTb.SetValue(WidthProperty, 70d);
            ftTimeTb.AddHandler(KeyDownEvent, new KeyEventHandler(DateShilfChange));
            ftTimeSp.AppendChild(ftTimeTb);
            ftParentSp.AppendChild(ftTimeSp);

            FrameworkElementFactory ftSpecialSp1 = new FrameworkElementFactory(typeof(StackPanel));
            ftSpecialSp1.SetValue(StackPanel.OrientationProperty, Orientation.Horizontal);
            ftSpecialSp1.SetValue(HeightProperty, 30d);
            FrameworkElementFactory ftSpecialSp2 = new FrameworkElementFactory(typeof(StackPanel));
            ftSpecialSp2.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);
            FrameworkElementFactory ftLateTb = new FrameworkElementFactory(typeof(TextBox));
            ftLateTb.SetBinding(TextBox.TextProperty, new Binding("FridayLatePm"));
            ftLateTb.AddHandler(KeyDownEvent, new KeyEventHandler(DateLateChange));
            ftSpecialSp2.AppendChild(ftLateTb);
            FrameworkElementFactory ftSpecialCb = new FrameworkElementFactory(typeof(CheckBox));
            ftSpecialCb.SetBinding(CheckBox.IsCheckedProperty, new Binding("FridaySpecialPm"));
            ftSpecialCb.SetValue(MarginProperty, new Thickness(50, -30, 0, 0));
            ftSpecialCb.AddHandler(Button.ClickEvent, new RoutedEventHandler(DateSpecialChange));
            FrameworkElementFactory ftShilfIdTb = new FrameworkElementFactory(typeof(TextBlock));
            ftShilfIdTb.SetValue(VisibilityProperty, Visibility.Hidden);
            ftSpecialCb.AppendChild(ftShilfIdTb);
            ftSpecialSp2.AppendChild(ftSpecialCb);
            ftSpecialSp1.AppendChild(ftSpecialSp2);
            ftParentSp.AppendChild(ftSpecialSp1);

            var ftDt = new DataTemplate();
            ftDt.VisualTree = ftParentSp;
            ftDt.Resources.Add(ftParentSp, null);
            ftTc.CellTemplate = ftDt;

            Columns.Add(ftTc);

            // SaturdayHours
            DataGridTemplateColumn shTc = new DataGridTemplateColumn();
            shTc.Header = "H";
            shTc.Width = 50;
            shTc.CanUserResize = false;
            shTc.CanUserSort = true;
            shTc.SortDirection = ListSortDirection.Descending;
            shTc.SortMemberPath = "SaturdayHours";
            FrameworkElementFactory shSp = new FrameworkElementFactory(typeof(StackPanel));
            shSp.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);

            FrameworkElementFactory shText = new FrameworkElementFactory(typeof(TextBlock));
            shText.SetBinding(TextBlock.TextProperty, new Binding("SaturdayHours"));
            shSp.AppendChild(shText);

            var shDt = new DataTemplate();
            shDt.VisualTree = shSp;
            shDt.Resources.Add(shSp, null);
            shTc.CellTemplate = shDt;

            Columns.Add(shTc);

            // SaturdayTime
            DataGridTemplateColumn stTc = new DataGridTemplateColumn();
            stTc.Header = "Time";
            stTc.Width = 70;
            stTc.CanUserResize = false;
            stTc.CanUserSort = true;
            stTc.SortDirection = ListSortDirection.Descending;
            stTc.SortMemberPath = "SaturdayTimePm";
            FrameworkElementFactory stParentSp = new FrameworkElementFactory(typeof(StackPanel));
            stParentSp.SetValue(MarginProperty, new Thickness(-15, -40, -15, -15));
            stParentSp.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);

            FrameworkElementFactory stMonShilfIdText = new FrameworkElementFactory(typeof(TextBlock));
            stMonShilfIdText.SetBinding(TextBlock.TextProperty, new Binding("SaturdayShilfIdPm"));
            stMonShilfIdText.SetValue(VisibilityProperty, Visibility.Hidden);
            stParentSp.AppendChild(stMonShilfIdText);

            FrameworkElementFactory stAccIdText = new FrameworkElementFactory(typeof(TextBlock));
            stAccIdText.SetBinding(TextBlock.TextProperty, new Binding("Id"));
            stAccIdText.SetValue(NameProperty, "tbStaffId");
            stAccIdText.SetValue(VisibilityProperty, Visibility.Hidden);
            stParentSp.AppendChild(stAccIdText);

            FrameworkElementFactory stTimeSp = new FrameworkElementFactory(typeof(StackPanel));
            stTimeSp.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);
            FrameworkElementFactory stTimeTb = new FrameworkElementFactory(typeof(TextBox));
            stTimeTb.SetBinding(TextBox.TextProperty, new Binding("SaturdayTimePm"));
            stTimeTb.SetBinding(BackgroundProperty, new Binding("SaturdayColorPm"));
            stTimeTb.SetValue(TextBox.TextAlignmentProperty, TextAlignment.Center);
            stTimeTb.SetValue(MarginProperty, new Thickness(0, 0, 0, 0));
            stTimeTb.SetValue(HeightProperty, 30d);
            stTimeTb.SetValue(WidthProperty, 70d);
            stTimeTb.AddHandler(KeyDownEvent, new KeyEventHandler(DateShilfChange));
            stTimeSp.AppendChild(stTimeTb);
            stParentSp.AppendChild(stTimeSp);

            FrameworkElementFactory stSpecialSp1 = new FrameworkElementFactory(typeof(StackPanel));
            stSpecialSp1.SetValue(StackPanel.OrientationProperty, Orientation.Horizontal);
            stSpecialSp1.SetValue(HeightProperty, 30d);
            FrameworkElementFactory stSpecialSp2 = new FrameworkElementFactory(typeof(StackPanel));
            stSpecialSp2.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);
            FrameworkElementFactory stLateTb = new FrameworkElementFactory(typeof(TextBox));
            stLateTb.SetBinding(TextBox.TextProperty, new Binding("SaturdayLatePm"));
            stLateTb.AddHandler(KeyDownEvent, new KeyEventHandler(DateLateChange));
            stSpecialSp2.AppendChild(stLateTb);
            FrameworkElementFactory stSpecialCb = new FrameworkElementFactory(typeof(CheckBox));
            stSpecialCb.SetBinding(CheckBox.IsCheckedProperty, new Binding("SaturdaySpecialPm"));
            stSpecialCb.SetValue(MarginProperty, new Thickness(50, -30, 0, 0));
            stSpecialCb.AddHandler(Button.ClickEvent, new RoutedEventHandler(DateSpecialChange));
            FrameworkElementFactory stShilfIdTb = new FrameworkElementFactory(typeof(TextBlock));
            stShilfIdTb.SetValue(VisibilityProperty, Visibility.Hidden);
            stSpecialCb.AppendChild(stShilfIdTb);
            stSpecialSp2.AppendChild(stSpecialCb);
            stSpecialSp1.AppendChild(stSpecialSp2);
            stParentSp.AppendChild(stSpecialSp1);

            var stDt = new DataTemplate();
            stDt.VisualTree = stParentSp;
            stDt.Resources.Add(stParentSp, null);
            stTc.CellTemplate = stDt;

            Columns.Add(stTc);

            // SundayHours
            DataGridTemplateColumn suhTc = new DataGridTemplateColumn();
            suhTc.Header = "H";
            suhTc.Width = 50;
            suhTc.CanUserResize = false;
            suhTc.CanUserSort = true;
            suhTc.SortDirection = ListSortDirection.Descending;
            suhTc.SortMemberPath = "SundayHours";
            FrameworkElementFactory suhSp = new FrameworkElementFactory(typeof(StackPanel));
            suhSp.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);

            FrameworkElementFactory suhText = new FrameworkElementFactory(typeof(TextBlock));
            suhText.SetBinding(TextBlock.TextProperty, new Binding("SundayHours"));
            suhSp.AppendChild(suhText);

            var suhDt = new DataTemplate();
            suhDt.VisualTree = suhSp;
            suhDt.Resources.Add(suhSp, null);
            suhTc.CellTemplate = suhDt;

            Columns.Add(suhTc);

            // SundayTime
            DataGridTemplateColumn sutTc = new DataGridTemplateColumn();
            sutTc.Header = "Time";
            sutTc.Width = 70;
            sutTc.CanUserResize = false;
            sutTc.CanUserSort = true;
            sutTc.SortDirection = ListSortDirection.Descending;
            sutTc.SortMemberPath = "SundayTimePm";
            FrameworkElementFactory sutParentSp = new FrameworkElementFactory(typeof(StackPanel));
            sutParentSp.SetValue(MarginProperty, new Thickness(-15, -40, -15, -15));
            sutParentSp.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);

            FrameworkElementFactory sutMonShilfIdText = new FrameworkElementFactory(typeof(TextBlock));
            sutMonShilfIdText.SetBinding(TextBlock.TextProperty, new Binding("SundayShilfIdPm"));
            sutMonShilfIdText.SetValue(VisibilityProperty, Visibility.Hidden);
            sutParentSp.AppendChild(sutMonShilfIdText);

            FrameworkElementFactory sutAccIdText = new FrameworkElementFactory(typeof(TextBlock));
            sutAccIdText.SetBinding(TextBlock.TextProperty, new Binding("Id"));
            sutAccIdText.SetValue(NameProperty, "tbStaffId");
            sutAccIdText.SetValue(VisibilityProperty, Visibility.Hidden);
            sutParentSp.AppendChild(sutAccIdText);

            FrameworkElementFactory sutTimeSp = new FrameworkElementFactory(typeof(StackPanel));
            sutTimeSp.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);
            FrameworkElementFactory sutTimeTb = new FrameworkElementFactory(typeof(TextBox));
            sutTimeTb.SetBinding(TextBox.TextProperty, new Binding("SundayTimePm"));
            sutTimeTb.SetBinding(BackgroundProperty, new Binding("SundayColorPm"));
            sutTimeTb.SetValue(TextBox.TextAlignmentProperty, TextAlignment.Center);
            sutTimeTb.SetValue(MarginProperty, new Thickness(0, 0, 0, 0));
            sutTimeTb.SetValue(HeightProperty, 30d);
            sutTimeTb.SetValue(WidthProperty, 70d);
            sutTimeTb.AddHandler(KeyDownEvent, new KeyEventHandler(DateShilfChange));
            sutTimeSp.AppendChild(sutTimeTb);
            sutParentSp.AppendChild(sutTimeSp);

            FrameworkElementFactory sutSpecialSp1 = new FrameworkElementFactory(typeof(StackPanel));
            sutSpecialSp1.SetValue(StackPanel.OrientationProperty, Orientation.Horizontal);
            sutSpecialSp1.SetValue(HeightProperty, 30d);
            FrameworkElementFactory sutSpecialSp2 = new FrameworkElementFactory(typeof(StackPanel));
            sutSpecialSp2.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);
            FrameworkElementFactory sutLateTb = new FrameworkElementFactory(typeof(TextBox));
            sutLateTb.SetBinding(TextBox.TextProperty, new Binding("SundayLatePm"));
            sutLateTb.AddHandler(KeyDownEvent, new KeyEventHandler(DateLateChange));
            sutSpecialSp2.AppendChild(sutLateTb);
            FrameworkElementFactory sutSpecialCb = new FrameworkElementFactory(typeof(CheckBox));
            sutSpecialCb.SetBinding(CheckBox.IsCheckedProperty, new Binding("SundaySpecialPm"));
            sutSpecialCb.SetValue(MarginProperty, new Thickness(50, -30, 0, 0));
            sutSpecialCb.AddHandler(Button.ClickEvent, new RoutedEventHandler(DateSpecialChange));
            FrameworkElementFactory sutShilfIdTb = new FrameworkElementFactory(typeof(TextBlock));
            sutShilfIdTb.SetValue(VisibilityProperty, Visibility.Hidden);
            sutSpecialCb.AppendChild(sutShilfIdTb);
            sutSpecialSp2.AppendChild(sutSpecialCb);
            sutSpecialSp1.AppendChild(sutSpecialSp2);
            sutParentSp.AppendChild(sutSpecialSp1);

            var sutDt = new DataTemplate();
            sutDt.VisualTree = sutParentSp;
            sutDt.Resources.Add(sutParentSp, null);
            sutTc.CellTemplate = sutDt;

            Columns.Add(sutTc);

            //foreach (var item in data)
            //    Items.Add(item);
        }

        public DgShilfPm(List<Models.ModelView.StaffView> data)
        {
            IsReadOnly = true;
            Background = null;
            Height = 505;

            HorizontalScrollBarVisibility = ScrollBarVisibility.Hidden;
            VerticalScrollBarVisibility = ScrollBarVisibility.Hidden;

            // Id
            DataGridTemplateColumn idTc = new DataGridTemplateColumn();
            idTc.Header = "Id";
            idTc.Width = 60;
            idTc.CanUserResize = false;
            idTc.CanUserSort = true;
            idTc.SortDirection = ListSortDirection.Descending;
            idTc.SortMemberPath = "Code";
            FrameworkElementFactory idSp = new FrameworkElementFactory(typeof(StackPanel));
            idSp.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);

            FrameworkElementFactory idText = new FrameworkElementFactory(typeof(TextBlock));
            idText.SetBinding(TextBlock.TextProperty, new Binding("Code"));
            idText.SetValue(TextBlock.TextWrappingProperty, TextWrapping.Wrap);
            idText.SetValue(TextBlock.TextAlignmentProperty, TextAlignment.Center);

            idSp.AppendChild(idText);

            var idDt = new DataTemplate();
            idDt.VisualTree = idSp;
            idDt.Resources.Add(idSp, null);
            idTc.CellTemplate = idDt;

            Columns.Add(idTc);

            // Name
            DataGridTemplateColumn nameTc = new DataGridTemplateColumn();
            nameTc.Header = "Name";
            nameTc.Width = 130;
            nameTc.CanUserResize = false;
            nameTc.CanUserSort = true;
            nameTc.SortDirection = ListSortDirection.Descending;
            nameTc.SortMemberPath = "Name";
            FrameworkElementFactory nameSp = new FrameworkElementFactory(typeof(StackPanel));
            nameSp.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);

            FrameworkElementFactory nameText = new FrameworkElementFactory(typeof(TextBlock));
            nameText.SetBinding(TextBlock.TextProperty, new Binding("Name"));
            nameText.SetValue(TextBlock.TextWrappingProperty, TextWrapping.Wrap);
            nameText.SetValue(TextBlock.TextAlignmentProperty, TextAlignment.Center);

            nameSp.AppendChild(nameText);

            var nameDt = new DataTemplate();
            nameDt.VisualTree = nameSp;
            nameDt.Resources.Add(nameSp, null);
            nameTc.CellTemplate = nameDt;

            Columns.Add(nameTc);

            // Position
            DataGridTemplateColumn posTc = new DataGridTemplateColumn();
            posTc.Header = "Position";
            posTc.Width = 90;
            posTc.CanUserResize = false;
            posTc.CanUserSort = true;
            posTc.SortDirection = ListSortDirection.Descending;
            posTc.SortMemberPath = "PositionName";
            FrameworkElementFactory posSp = new FrameworkElementFactory(typeof(StackPanel));
            posSp.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);

            FrameworkElementFactory posText = new FrameworkElementFactory(typeof(TextBlock));
            posText.SetBinding(TextBlock.TextProperty, new Binding("PositionName"));
            posText.SetValue(TextBlock.TextWrappingProperty, TextWrapping.Wrap);
            posText.SetValue(TextBlock.TextAlignmentProperty, TextAlignment.Center);

            posSp.AppendChild(posText);

            var postDt = new DataTemplate();
            postDt.VisualTree = posSp;
            postDt.Resources.Add(posSp, null);
            posTc.CellTemplate = postDt;

            Columns.Add(posTc);

            // MondayHours
            DataGridTemplateColumn mhTc = new DataGridTemplateColumn();
            mhTc.Header = "H";
            mhTc.Width = 50;
            mhTc.CanUserResize = false;
            mhTc.CanUserSort = true;
            mhTc.SortDirection = ListSortDirection.Descending;
            mhTc.SortMemberPath = "MondayHours";
            FrameworkElementFactory mhSp = new FrameworkElementFactory(typeof(StackPanel));
            mhSp.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);

            FrameworkElementFactory mhText = new FrameworkElementFactory(typeof(TextBlock));
            mhText.SetBinding(TextBlock.TextProperty, new Binding("MondayHours"));
            mhSp.AppendChild(mhText);

            var mhDt = new DataTemplate();
            mhDt.VisualTree = mhSp;
            mhDt.Resources.Add(mhSp, null);
            mhTc.CellTemplate = mhDt;

            Columns.Add(mhTc);

            // MondayTime
            DataGridTemplateColumn mtTc = new DataGridTemplateColumn();
            mtTc.Header = "Time";
            mtTc.Width = 70;
            mtTc.CanUserResize = false;
            mtTc.CanUserSort = true;
            mtTc.SortDirection = ListSortDirection.Descending;
            mtTc.SortMemberPath = "MondayTimePm";
            FrameworkElementFactory mtParentSp = new FrameworkElementFactory(typeof(StackPanel));
            mtParentSp.SetValue(MarginProperty, new Thickness(-15, -40, -15, -15));
            mtParentSp.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);

            FrameworkElementFactory mtMonShilfIdText = new FrameworkElementFactory(typeof(TextBlock));
            mtMonShilfIdText.SetBinding(TextBlock.TextProperty, new Binding("MondayShilfIdPm"));
            mtMonShilfIdText.SetValue(NameProperty, "tbShilfId");
            mtMonShilfIdText.SetValue(VisibilityProperty, Visibility.Hidden);
            mtParentSp.AppendChild(mtMonShilfIdText);

            FrameworkElementFactory mtAccIdText = new FrameworkElementFactory(typeof(TextBlock));
            mtAccIdText.SetBinding(TextBlock.TextProperty, new Binding("Id"));
            mtAccIdText.SetValue(NameProperty, "tbStaffId");
            mtAccIdText.SetValue(VisibilityProperty, Visibility.Hidden);
            mtParentSp.AppendChild(mtAccIdText);

            FrameworkElementFactory mtTimeSp = new FrameworkElementFactory(typeof(StackPanel));
            mtTimeSp.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);
            FrameworkElementFactory mtTimeTb = new FrameworkElementFactory(typeof(TextBox));
            mtTimeTb.SetBinding(TextBox.TextProperty, new Binding("MondayTimePm"));
            mtTimeTb.SetBinding(BackgroundProperty, new Binding("MondayColorPm"));
            mtTimeTb.SetValue(TextBox.TextAlignmentProperty, TextAlignment.Center);
            mtTimeTb.SetValue(MarginProperty, new Thickness(0, 0, 0, 0));
            mtTimeTb.SetValue(HeightProperty, 30d);
            mtTimeTb.SetValue(WidthProperty, 70d);
            mtTimeTb.AddHandler(KeyDownEvent, new KeyEventHandler(DateShilfChange));
            mtTimeSp.AppendChild(mtTimeTb);
            mtParentSp.AppendChild(mtTimeSp);

            FrameworkElementFactory mtSpecialSp1 = new FrameworkElementFactory(typeof(StackPanel));
            mtSpecialSp1.SetValue(StackPanel.OrientationProperty, Orientation.Horizontal);
            mtSpecialSp1.SetValue(HeightProperty, 30d);
            FrameworkElementFactory mtSpecialSp2 = new FrameworkElementFactory(typeof(StackPanel));
            mtSpecialSp2.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);
            FrameworkElementFactory mtLateTb = new FrameworkElementFactory(typeof(TextBox));
            mtLateTb.SetBinding(TextBox.TextProperty, new Binding("MondayLatePm"));
            mtLateTb.AddHandler(KeyDownEvent, new KeyEventHandler(DateLateChange));
            mtSpecialSp2.AppendChild(mtLateTb);
            FrameworkElementFactory mtSpecialCb = new FrameworkElementFactory(typeof(CheckBox));
            mtSpecialCb.SetBinding(CheckBox.IsCheckedProperty, new Binding("MondaySpecialPm"));
            mtSpecialCb.SetValue(MarginProperty, new Thickness(50, -30, 0, 0));
            mtSpecialCb.AddHandler(Button.ClickEvent, new RoutedEventHandler(DateSpecialChange));
            FrameworkElementFactory mtShilfIdTb = new FrameworkElementFactory(typeof(TextBlock));
            mtShilfIdTb.SetValue(VisibilityProperty, Visibility.Hidden);
            mtSpecialCb.AppendChild(mtShilfIdTb);
            mtSpecialSp2.AppendChild(mtSpecialCb);
            mtSpecialSp1.AppendChild(mtSpecialSp2);
            mtParentSp.AppendChild(mtSpecialSp1);

            var mtDt = new DataTemplate();
            mtDt.VisualTree = mtParentSp;
            mtDt.Resources.Add(mtParentSp, null);
            mtTc.CellTemplate = mtDt;

            Columns.Add(mtTc);

            // TuesdayHours
            DataGridTemplateColumn thTc = new DataGridTemplateColumn();
            thTc.Header = "H";
            thTc.Width = 50;
            thTc.CanUserResize = false;
            thTc.CanUserSort = true;
            thTc.SortDirection = ListSortDirection.Descending;
            thTc.SortMemberPath = "TuesdayHours";
            FrameworkElementFactory thSp = new FrameworkElementFactory(typeof(StackPanel));
            thSp.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);

            FrameworkElementFactory thText = new FrameworkElementFactory(typeof(TextBlock));
            thText.SetBinding(TextBlock.TextProperty, new Binding("TuesdayHours"));
            thSp.AppendChild(thText);

            var thDt = new DataTemplate();
            thDt.VisualTree = thSp;
            thDt.Resources.Add(thSp, null);
            thTc.CellTemplate = thDt;

            Columns.Add(thTc);

            // TuesdayTime
            DataGridTemplateColumn ttTc = new DataGridTemplateColumn();
            ttTc.Header = "Time";
            ttTc.Width = 70;
            ttTc.CanUserResize = false;
            ttTc.CanUserSort = true;
            ttTc.SortDirection = ListSortDirection.Descending;
            ttTc.SortMemberPath = "TuesdayTimePm";
            FrameworkElementFactory ttParentSp = new FrameworkElementFactory(typeof(StackPanel));
            ttParentSp.SetValue(MarginProperty, new Thickness(-15, -40, -15, -15));
            ttParentSp.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);

            FrameworkElementFactory ttMonShilfIdText = new FrameworkElementFactory(typeof(TextBlock));
            ttMonShilfIdText.SetBinding(TextBlock.TextProperty, new Binding("TuesdayShilfIdPm"));
            ttMonShilfIdText.SetValue(VisibilityProperty, Visibility.Hidden);
            ttParentSp.AppendChild(ttMonShilfIdText);

            FrameworkElementFactory ttAccIdText = new FrameworkElementFactory(typeof(TextBlock));
            ttAccIdText.SetBinding(TextBlock.TextProperty, new Binding("Id"));
            ttAccIdText.SetValue(NameProperty, "tbStaffId");
            ttAccIdText.SetValue(VisibilityProperty, Visibility.Hidden);
            ttParentSp.AppendChild(ttAccIdText);

            FrameworkElementFactory ttTimeSp = new FrameworkElementFactory(typeof(StackPanel));
            ttTimeSp.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);
            FrameworkElementFactory ttTimeTb = new FrameworkElementFactory(typeof(TextBox));
            ttTimeTb.SetBinding(TextBox.TextProperty, new Binding("TuesdayTimePm"));
            ttTimeTb.SetBinding(BackgroundProperty, new Binding("TuesdayColorPm"));
            ttTimeTb.SetValue(TextBox.TextAlignmentProperty, TextAlignment.Center);
            ttTimeTb.SetValue(MarginProperty, new Thickness(0, 0, 0, 0));
            ttTimeTb.SetValue(HeightProperty, 30d);
            ttTimeTb.SetValue(WidthProperty, 70d);
            ttTimeTb.AddHandler(KeyDownEvent, new KeyEventHandler(DateShilfChange));
            ttTimeSp.AppendChild(ttTimeTb);
            ttParentSp.AppendChild(ttTimeSp);

            FrameworkElementFactory ttSpecialSp1 = new FrameworkElementFactory(typeof(StackPanel));
            ttSpecialSp1.SetValue(StackPanel.OrientationProperty, Orientation.Horizontal);
            ttSpecialSp1.SetValue(HeightProperty, 30d);
            FrameworkElementFactory ttSpecialSp2 = new FrameworkElementFactory(typeof(StackPanel));
            ttSpecialSp2.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);
            FrameworkElementFactory ttLateTb = new FrameworkElementFactory(typeof(TextBox));
            ttLateTb.SetBinding(TextBox.TextProperty, new Binding("TuesdayLatePm"));
            ttLateTb.AddHandler(KeyDownEvent, new KeyEventHandler(DateLateChange));
            ttSpecialSp2.AppendChild(ttLateTb);
            FrameworkElementFactory ttSpecialCb = new FrameworkElementFactory(typeof(CheckBox));
            ttSpecialCb.SetBinding(CheckBox.IsCheckedProperty, new Binding("TuesdaySpecialPm"));
            ttSpecialCb.SetValue(MarginProperty, new Thickness(50, -30, 0, 0));
            ttSpecialCb.AddHandler(Button.ClickEvent, new RoutedEventHandler(DateSpecialChange));
            FrameworkElementFactory ttShilfIdTb = new FrameworkElementFactory(typeof(TextBlock));
            ttShilfIdTb.SetValue(VisibilityProperty, Visibility.Hidden);
            ttSpecialCb.AppendChild(ttShilfIdTb);
            ttSpecialSp2.AppendChild(ttSpecialCb);
            ttSpecialSp1.AppendChild(ttSpecialSp2);
            ttParentSp.AppendChild(ttSpecialSp1);

            var ttDt = new DataTemplate();
            ttDt.VisualTree = ttParentSp;
            ttDt.Resources.Add(ttParentSp, null);
            ttTc.CellTemplate = ttDt;

            Columns.Add(ttTc);

            // WednesdayHours
            DataGridTemplateColumn whTc = new DataGridTemplateColumn();
            whTc.Header = "H";
            whTc.Width = 50;
            whTc.CanUserResize = false;
            whTc.CanUserSort = true;
            whTc.SortDirection = ListSortDirection.Descending;
            whTc.SortMemberPath = "WednesdayHours";
            FrameworkElementFactory whSp = new FrameworkElementFactory(typeof(StackPanel));
            whSp.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);

            FrameworkElementFactory whText = new FrameworkElementFactory(typeof(TextBlock));
            whText.SetBinding(TextBlock.TextProperty, new Binding("WednesdayHours"));
            whSp.AppendChild(whText);

            var whDt = new DataTemplate();
            whDt.VisualTree = whSp;
            whDt.Resources.Add(whSp, null);
            whTc.CellTemplate = whDt;

            Columns.Add(whTc);

            // WednesdayTime
            DataGridTemplateColumn wtTc = new DataGridTemplateColumn();
            wtTc.Header = "Time";
            wtTc.Width = 70;
            wtTc.CanUserResize = false;
            wtTc.CanUserSort = true;
            wtTc.SortDirection = ListSortDirection.Descending;
            wtTc.SortMemberPath = "WednesdayTimePm";
            FrameworkElementFactory wtParentSp = new FrameworkElementFactory(typeof(StackPanel));
            wtParentSp.SetValue(MarginProperty, new Thickness(-15, -40, -15, -15));
            wtParentSp.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);

            FrameworkElementFactory wtMonShilfIdText = new FrameworkElementFactory(typeof(TextBlock));
            wtMonShilfIdText.SetBinding(TextBlock.TextProperty, new Binding("WednesdayShilfIdPm"));
            wtMonShilfIdText.SetValue(VisibilityProperty, Visibility.Hidden);
            wtParentSp.AppendChild(wtMonShilfIdText);

            FrameworkElementFactory wtAccIdText = new FrameworkElementFactory(typeof(TextBlock));
            wtAccIdText.SetBinding(TextBlock.TextProperty, new Binding("Id"));
            wtAccIdText.SetValue(NameProperty, "tbStaffId");
            wtAccIdText.SetValue(VisibilityProperty, Visibility.Hidden);
            wtParentSp.AppendChild(wtAccIdText);

            FrameworkElementFactory wtTimeSp = new FrameworkElementFactory(typeof(StackPanel));
            wtTimeSp.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);
            FrameworkElementFactory wtTimeTb = new FrameworkElementFactory(typeof(TextBox));
            wtTimeTb.SetBinding(TextBox.TextProperty, new Binding("WednesdayTimePm"));
            wtTimeTb.SetBinding(BackgroundProperty, new Binding("WednesdayColorPm"));
            wtTimeTb.SetValue(TextBox.TextAlignmentProperty, TextAlignment.Center);
            wtTimeTb.SetValue(MarginProperty, new Thickness(0, 0, 0, 0));
            wtTimeTb.SetValue(HeightProperty, 30d);
            wtTimeTb.SetValue(WidthProperty, 70d);
            wtTimeTb.AddHandler(KeyDownEvent, new KeyEventHandler(DateShilfChange));
            wtTimeSp.AppendChild(wtTimeTb);
            wtParentSp.AppendChild(wtTimeSp);

            FrameworkElementFactory wtSpecialSp1 = new FrameworkElementFactory(typeof(StackPanel));
            wtSpecialSp1.SetValue(StackPanel.OrientationProperty, Orientation.Horizontal);
            wtSpecialSp1.SetValue(HeightProperty, 30d);
            FrameworkElementFactory wtSpecialSp2 = new FrameworkElementFactory(typeof(StackPanel));
            wtSpecialSp2.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);
            FrameworkElementFactory wtLateTb = new FrameworkElementFactory(typeof(TextBox));
            wtLateTb.SetBinding(TextBox.TextProperty, new Binding("WednesdayLatePm"));
            wtLateTb.AddHandler(KeyDownEvent, new KeyEventHandler(DateLateChange));
            wtSpecialSp2.AppendChild(wtLateTb);
            FrameworkElementFactory wtSpecialCb = new FrameworkElementFactory(typeof(CheckBox));
            wtSpecialCb.SetBinding(CheckBox.IsCheckedProperty, new Binding("WednesdaySpecialPm"));
            wtSpecialCb.SetValue(MarginProperty, new Thickness(50, -30, 0, 0));
            wtSpecialCb.AddHandler(Button.ClickEvent, new RoutedEventHandler(DateSpecialChange));
            FrameworkElementFactory wtShilfIdTb = new FrameworkElementFactory(typeof(TextBlock));
            wtShilfIdTb.SetValue(VisibilityProperty, Visibility.Hidden);
            wtSpecialCb.AppendChild(wtShilfIdTb);
            wtSpecialSp2.AppendChild(wtSpecialCb);
            wtSpecialSp1.AppendChild(wtSpecialSp2);
            wtParentSp.AppendChild(wtSpecialSp1);

            var wtDt = new DataTemplate();
            wtDt.VisualTree = wtParentSp;
            wtDt.Resources.Add(wtParentSp, null);
            wtTc.CellTemplate = wtDt;

            Columns.Add(wtTc);

            // ThursdayHours
            DataGridTemplateColumn thhTc = new DataGridTemplateColumn();
            thhTc.Header = "H";
            thhTc.Width = 50;
            thhTc.CanUserResize = false;
            thhTc.CanUserSort = true;
            thhTc.SortDirection = ListSortDirection.Descending;
            thhTc.SortMemberPath = "ThursdayHours";
            FrameworkElementFactory thhSp = new FrameworkElementFactory(typeof(StackPanel));
            thhSp.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);

            FrameworkElementFactory thhText = new FrameworkElementFactory(typeof(TextBlock));
            thhText.SetBinding(TextBlock.TextProperty, new Binding("ThursdayHours"));
            thhSp.AppendChild(thhText);

            var thhDt = new DataTemplate();
            thhDt.VisualTree = thhSp;
            thhDt.Resources.Add(thhSp, null);
            thhTc.CellTemplate = thhDt;

            Columns.Add(thhTc);

            // ThursdayTime
            DataGridTemplateColumn thtTc = new DataGridTemplateColumn();
            thtTc.Header = "Time";
            thtTc.Width = 70;
            thtTc.CanUserResize = false;
            thtTc.CanUserSort = true;
            thtTc.SortDirection = ListSortDirection.Descending;
            thtTc.SortMemberPath = "ThursdayTimePm";
            FrameworkElementFactory thtParentSp = new FrameworkElementFactory(typeof(StackPanel));
            thtParentSp.SetValue(MarginProperty, new Thickness(-15, -40, -15, -15));
            thtParentSp.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);

            FrameworkElementFactory thtMonShilfIdText = new FrameworkElementFactory(typeof(TextBlock));
            thtMonShilfIdText.SetBinding(TextBlock.TextProperty, new Binding("ThursdayShilfIdPm"));
            thtMonShilfIdText.SetValue(VisibilityProperty, Visibility.Hidden);
            thtParentSp.AppendChild(thtMonShilfIdText);

            FrameworkElementFactory thtAccIdText = new FrameworkElementFactory(typeof(TextBlock));
            thtAccIdText.SetBinding(TextBlock.TextProperty, new Binding("Id"));
            thtAccIdText.SetValue(NameProperty, "tbStaffId");
            thtAccIdText.SetValue(VisibilityProperty, Visibility.Hidden);
            thtParentSp.AppendChild(thtAccIdText);

            FrameworkElementFactory thtTimeSp = new FrameworkElementFactory(typeof(StackPanel));
            thtTimeSp.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);
            FrameworkElementFactory thtTimeTb = new FrameworkElementFactory(typeof(TextBox));
            thtTimeTb.SetBinding(TextBox.TextProperty, new Binding("ThursdayTimePm"));
            thtTimeTb.SetBinding(BackgroundProperty, new Binding("ThursdayColorPm"));
            thtTimeTb.SetValue(TextBox.TextAlignmentProperty, TextAlignment.Center);
            thtTimeTb.SetValue(MarginProperty, new Thickness(0, 0, 0, 0));
            thtTimeTb.SetValue(HeightProperty, 30d);
            thtTimeTb.SetValue(WidthProperty, 70d);
            thtTimeTb.AddHandler(KeyDownEvent, new KeyEventHandler(DateShilfChange));
            thtTimeSp.AppendChild(thtTimeTb);
            thtParentSp.AppendChild(thtTimeSp);

            FrameworkElementFactory thtSpecialSp1 = new FrameworkElementFactory(typeof(StackPanel));
            thtSpecialSp1.SetValue(StackPanel.OrientationProperty, Orientation.Horizontal);
            thtSpecialSp1.SetValue(HeightProperty, 30d);
            FrameworkElementFactory thtSpecialSp2 = new FrameworkElementFactory(typeof(StackPanel));
            thtSpecialSp2.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);
            FrameworkElementFactory thtLateTb = new FrameworkElementFactory(typeof(TextBox));
            thtLateTb.SetBinding(TextBox.TextProperty, new Binding("ThursdayLatePm"));
            thtLateTb.AddHandler(KeyDownEvent, new KeyEventHandler(DateLateChange));
            thtSpecialSp2.AppendChild(thtLateTb);
            FrameworkElementFactory thtSpecialCb = new FrameworkElementFactory(typeof(CheckBox));
            thtSpecialCb.SetBinding(CheckBox.IsCheckedProperty, new Binding("ThursdaySpecialPm"));
            thtSpecialCb.SetValue(MarginProperty, new Thickness(50, -30, 0, 0));
            thtSpecialCb.AddHandler(Button.ClickEvent, new RoutedEventHandler(DateSpecialChange));
            FrameworkElementFactory thtShilfIdTb = new FrameworkElementFactory(typeof(TextBlock));
            thtShilfIdTb.SetValue(VisibilityProperty, Visibility.Hidden);
            thtSpecialCb.AppendChild(thtShilfIdTb);
            thtSpecialSp2.AppendChild(thtSpecialCb);
            thtSpecialSp1.AppendChild(thtSpecialSp2);
            thtParentSp.AppendChild(thtSpecialSp1);

            var thtDt = new DataTemplate();
            thtDt.VisualTree = thtParentSp;
            thtDt.Resources.Add(thtParentSp, null);
            thtTc.CellTemplate = thtDt;

            Columns.Add(thtTc);

            // FridayHours
            DataGridTemplateColumn fhTc = new DataGridTemplateColumn();
            fhTc.Header = "H";
            fhTc.Width = 50;
            fhTc.CanUserResize = false;
            fhTc.CanUserSort = true;
            fhTc.SortDirection = ListSortDirection.Descending;
            fhTc.SortMemberPath = "FridayHours";
            FrameworkElementFactory fhSp = new FrameworkElementFactory(typeof(StackPanel));
            fhSp.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);

            FrameworkElementFactory fhText = new FrameworkElementFactory(typeof(TextBlock));
            fhText.SetBinding(TextBlock.TextProperty, new Binding("FridayHours"));
            fhSp.AppendChild(fhText);

            var fhDt = new DataTemplate();
            fhDt.VisualTree = fhSp;
            fhDt.Resources.Add(fhSp, null);
            fhTc.CellTemplate = fhDt;

            Columns.Add(fhTc);

            // FridayTime
            DataGridTemplateColumn ftTc = new DataGridTemplateColumn();
            ftTc.Header = "Time";
            ftTc.Width = 70;
            ftTc.CanUserResize = false;
            ftTc.CanUserSort = true;
            ftTc.SortDirection = ListSortDirection.Descending;
            ftTc.SortMemberPath = "FridayTimePm";
            FrameworkElementFactory ftParentSp = new FrameworkElementFactory(typeof(StackPanel));
            ftParentSp.SetValue(MarginProperty, new Thickness(-15, -40, -15, -15));
            ftParentSp.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);

            FrameworkElementFactory ftMonShilfIdText = new FrameworkElementFactory(typeof(TextBlock));
            ftMonShilfIdText.SetBinding(TextBlock.TextProperty, new Binding("FridayShilfIdPm"));
            ftMonShilfIdText.SetValue(VisibilityProperty, Visibility.Hidden);
            ftParentSp.AppendChild(ftMonShilfIdText);

            FrameworkElementFactory ftAccIdText = new FrameworkElementFactory(typeof(TextBlock));
            ftAccIdText.SetBinding(TextBlock.TextProperty, new Binding("Id"));
            ftAccIdText.SetValue(NameProperty, "tbStaffId");
            ftAccIdText.SetValue(VisibilityProperty, Visibility.Hidden);
            ftParentSp.AppendChild(ftAccIdText);

            FrameworkElementFactory ftTimeSp = new FrameworkElementFactory(typeof(StackPanel));
            ftTimeSp.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);
            FrameworkElementFactory ftTimeTb = new FrameworkElementFactory(typeof(TextBox));
            ftTimeTb.SetBinding(TextBox.TextProperty, new Binding("FridayTimePm"));
            ftTimeTb.SetBinding(BackgroundProperty, new Binding("FridayColorPm"));
            ftTimeTb.SetValue(TextBox.TextAlignmentProperty, TextAlignment.Center);
            ftTimeTb.SetValue(MarginProperty, new Thickness(0, 0, 0, 0));
            ftTimeTb.SetValue(HeightProperty, 30d);
            ftTimeTb.SetValue(WidthProperty, 70d);
            ftTimeTb.AddHandler(KeyDownEvent, new KeyEventHandler(DateShilfChange));
            ftTimeSp.AppendChild(ftTimeTb);
            ftParentSp.AppendChild(ftTimeSp);

            FrameworkElementFactory ftSpecialSp1 = new FrameworkElementFactory(typeof(StackPanel));
            ftSpecialSp1.SetValue(StackPanel.OrientationProperty, Orientation.Horizontal);
            ftSpecialSp1.SetValue(HeightProperty, 30d);
            FrameworkElementFactory ftSpecialSp2 = new FrameworkElementFactory(typeof(StackPanel));
            ftSpecialSp2.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);
            FrameworkElementFactory ftLateTb = new FrameworkElementFactory(typeof(TextBox));
            ftLateTb.SetBinding(TextBox.TextProperty, new Binding("FridayLatePm"));
            ftLateTb.AddHandler(KeyDownEvent, new KeyEventHandler(DateLateChange));
            ftSpecialSp2.AppendChild(ftLateTb);
            FrameworkElementFactory ftSpecialCb = new FrameworkElementFactory(typeof(CheckBox));
            ftSpecialCb.SetBinding(CheckBox.IsCheckedProperty, new Binding("FridaySpecialPm"));
            ftSpecialCb.SetValue(MarginProperty, new Thickness(50, -30, 0, 0));
            ftSpecialCb.AddHandler(Button.ClickEvent, new RoutedEventHandler(DateSpecialChange));
            FrameworkElementFactory ftShilfIdTb = new FrameworkElementFactory(typeof(TextBlock));
            ftShilfIdTb.SetValue(VisibilityProperty, Visibility.Hidden);
            ftSpecialCb.AppendChild(ftShilfIdTb);
            ftSpecialSp2.AppendChild(ftSpecialCb);
            ftSpecialSp1.AppendChild(ftSpecialSp2);
            ftParentSp.AppendChild(ftSpecialSp1);

            var ftDt = new DataTemplate();
            ftDt.VisualTree = ftParentSp;
            ftDt.Resources.Add(ftParentSp, null);
            ftTc.CellTemplate = ftDt;

            Columns.Add(ftTc);

            // SaturdayHours
            DataGridTemplateColumn shTc = new DataGridTemplateColumn();
            shTc.Header = "H";
            shTc.Width = 50;
            shTc.CanUserResize = false;
            shTc.CanUserSort = true;
            shTc.SortDirection = ListSortDirection.Descending;
            shTc.SortMemberPath = "SaturdayHours";
            FrameworkElementFactory shSp = new FrameworkElementFactory(typeof(StackPanel));
            shSp.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);

            FrameworkElementFactory shText = new FrameworkElementFactory(typeof(TextBlock));
            shText.SetBinding(TextBlock.TextProperty, new Binding("SaturdayHours"));
            shSp.AppendChild(shText);

            var shDt = new DataTemplate();
            shDt.VisualTree = shSp;
            shDt.Resources.Add(shSp, null);
            shTc.CellTemplate = shDt;

            Columns.Add(shTc);

            // SaturdayTime
            DataGridTemplateColumn stTc = new DataGridTemplateColumn();
            stTc.Header = "Time";
            stTc.Width = 70;
            stTc.CanUserResize = false;
            stTc.CanUserSort = true;
            stTc.SortDirection = ListSortDirection.Descending;
            stTc.SortMemberPath = "SaturdayTimePm";
            FrameworkElementFactory stParentSp = new FrameworkElementFactory(typeof(StackPanel));
            stParentSp.SetValue(MarginProperty, new Thickness(-15, -40, -15, -15));
            stParentSp.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);

            FrameworkElementFactory stMonShilfIdText = new FrameworkElementFactory(typeof(TextBlock));
            stMonShilfIdText.SetBinding(TextBlock.TextProperty, new Binding("SaturdayShilfIdPm"));
            stMonShilfIdText.SetValue(VisibilityProperty, Visibility.Hidden);
            stParentSp.AppendChild(stMonShilfIdText);

            FrameworkElementFactory stAccIdText = new FrameworkElementFactory(typeof(TextBlock));
            stAccIdText.SetBinding(TextBlock.TextProperty, new Binding("Id"));
            stAccIdText.SetValue(NameProperty, "tbStaffId");
            stAccIdText.SetValue(VisibilityProperty, Visibility.Hidden);
            stParentSp.AppendChild(stAccIdText);

            FrameworkElementFactory stTimeSp = new FrameworkElementFactory(typeof(StackPanel));
            stTimeSp.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);
            FrameworkElementFactory stTimeTb = new FrameworkElementFactory(typeof(TextBox));
            stTimeTb.SetBinding(TextBox.TextProperty, new Binding("SaturdayTimePm"));
            stTimeTb.SetBinding(BackgroundProperty, new Binding("SaturdayColorPm"));
            stTimeTb.SetValue(TextBox.TextAlignmentProperty, TextAlignment.Center);
            stTimeTb.SetValue(MarginProperty, new Thickness(0, 0, 0, 0));
            stTimeTb.SetValue(HeightProperty, 30d);
            stTimeTb.SetValue(WidthProperty, 70d);
            stTimeTb.AddHandler(KeyDownEvent, new KeyEventHandler(DateShilfChange));
            stTimeSp.AppendChild(stTimeTb);
            stParentSp.AppendChild(stTimeSp);

            FrameworkElementFactory stSpecialSp1 = new FrameworkElementFactory(typeof(StackPanel));
            stSpecialSp1.SetValue(StackPanel.OrientationProperty, Orientation.Horizontal);
            stSpecialSp1.SetValue(HeightProperty, 30d);
            FrameworkElementFactory stSpecialSp2 = new FrameworkElementFactory(typeof(StackPanel));
            stSpecialSp2.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);
            FrameworkElementFactory stLateTb = new FrameworkElementFactory(typeof(TextBox));
            stLateTb.SetBinding(TextBox.TextProperty, new Binding("SaturdayLatePm"));
            stLateTb.AddHandler(KeyDownEvent, new KeyEventHandler(DateLateChange));
            stSpecialSp2.AppendChild(stLateTb);
            FrameworkElementFactory stSpecialCb = new FrameworkElementFactory(typeof(CheckBox));
            stSpecialCb.SetBinding(CheckBox.IsCheckedProperty, new Binding("SaturdaySpecialPm"));
            stSpecialCb.SetValue(MarginProperty, new Thickness(50, -30, 0, 0));
            stSpecialCb.AddHandler(Button.ClickEvent, new RoutedEventHandler(DateSpecialChange));
            FrameworkElementFactory stShilfIdTb = new FrameworkElementFactory(typeof(TextBlock));
            stShilfIdTb.SetValue(VisibilityProperty, Visibility.Hidden);
            stSpecialCb.AppendChild(stShilfIdTb);
            stSpecialSp2.AppendChild(stSpecialCb);
            stSpecialSp1.AppendChild(stSpecialSp2);
            stParentSp.AppendChild(stSpecialSp1);

            var stDt = new DataTemplate();
            stDt.VisualTree = stParentSp;
            stDt.Resources.Add(stParentSp, null);
            stTc.CellTemplate = stDt;

            Columns.Add(stTc);

            // SundayHours
            DataGridTemplateColumn suhTc = new DataGridTemplateColumn();
            suhTc.Header = "H";
            suhTc.Width = 50;
            suhTc.CanUserResize = false;
            suhTc.CanUserSort = true;
            suhTc.SortDirection = ListSortDirection.Descending;
            suhTc.SortMemberPath = "SundayHours";
            FrameworkElementFactory suhSp = new FrameworkElementFactory(typeof(StackPanel));
            suhSp.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);

            FrameworkElementFactory suhText = new FrameworkElementFactory(typeof(TextBlock));
            suhText.SetBinding(TextBlock.TextProperty, new Binding("SundayHours"));
            suhSp.AppendChild(suhText);

            var suhDt = new DataTemplate();
            suhDt.VisualTree = suhSp;
            suhDt.Resources.Add(suhSp, null);
            suhTc.CellTemplate = suhDt;

            Columns.Add(suhTc);

            // SundayTime
            DataGridTemplateColumn sutTc = new DataGridTemplateColumn();
            sutTc.Header = "Time";
            sutTc.Width = 70;
            sutTc.CanUserResize = false;
            sutTc.CanUserSort = true;
            sutTc.SortDirection = ListSortDirection.Descending;
            sutTc.SortMemberPath = "SundayTimePm";
            FrameworkElementFactory sutParentSp = new FrameworkElementFactory(typeof(StackPanel));
            sutParentSp.SetValue(MarginProperty, new Thickness(-15, -40, -15, -15));
            sutParentSp.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);

            FrameworkElementFactory sutMonShilfIdText = new FrameworkElementFactory(typeof(TextBlock));
            sutMonShilfIdText.SetBinding(TextBlock.TextProperty, new Binding("SundayShilfIdPm"));
            sutMonShilfIdText.SetValue(VisibilityProperty, Visibility.Hidden);
            sutParentSp.AppendChild(sutMonShilfIdText);

            FrameworkElementFactory sutAccIdText = new FrameworkElementFactory(typeof(TextBlock));
            sutAccIdText.SetBinding(TextBlock.TextProperty, new Binding("Id"));
            sutAccIdText.SetValue(NameProperty, "tbStaffId");
            sutAccIdText.SetValue(VisibilityProperty, Visibility.Hidden);
            sutParentSp.AppendChild(sutAccIdText);

            FrameworkElementFactory sutTimeSp = new FrameworkElementFactory(typeof(StackPanel));
            sutTimeSp.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);
            FrameworkElementFactory sutTimeTb = new FrameworkElementFactory(typeof(TextBox));
            sutTimeTb.SetBinding(TextBox.TextProperty, new Binding("SundayTimePm"));
            sutTimeTb.SetBinding(BackgroundProperty, new Binding("SundayColorPm"));
            sutTimeTb.SetValue(TextBox.TextAlignmentProperty, TextAlignment.Center);
            sutTimeTb.SetValue(MarginProperty, new Thickness(0, 0, 0, 0));
            sutTimeTb.SetValue(HeightProperty, 30d);
            sutTimeTb.SetValue(WidthProperty, 70d);
            sutTimeTb.AddHandler(KeyDownEvent, new KeyEventHandler(DateShilfChange));
            sutTimeSp.AppendChild(sutTimeTb);
            sutParentSp.AppendChild(sutTimeSp);

            FrameworkElementFactory sutSpecialSp1 = new FrameworkElementFactory(typeof(StackPanel));
            sutSpecialSp1.SetValue(StackPanel.OrientationProperty, Orientation.Horizontal);
            sutSpecialSp1.SetValue(HeightProperty, 30d);
            FrameworkElementFactory sutSpecialSp2 = new FrameworkElementFactory(typeof(StackPanel));
            sutSpecialSp2.SetValue(StackPanel.OrientationProperty, Orientation.Vertical);
            FrameworkElementFactory sutLateTb = new FrameworkElementFactory(typeof(TextBox));
            sutLateTb.SetBinding(TextBox.TextProperty, new Binding("SundayLatePm"));
            sutLateTb.AddHandler(KeyDownEvent, new KeyEventHandler(DateLateChange));
            sutSpecialSp2.AppendChild(sutLateTb);
            FrameworkElementFactory sutSpecialCb = new FrameworkElementFactory(typeof(CheckBox));
            sutSpecialCb.SetBinding(CheckBox.IsCheckedProperty, new Binding("SundaySpecialPm"));
            sutSpecialCb.SetValue(MarginProperty, new Thickness(50, -30, 0, 0));
            sutSpecialCb.AddHandler(Button.ClickEvent, new RoutedEventHandler(DateSpecialChange));
            FrameworkElementFactory sutShilfIdTb = new FrameworkElementFactory(typeof(TextBlock));
            sutShilfIdTb.SetValue(VisibilityProperty, Visibility.Hidden);
            sutSpecialCb.AppendChild(sutShilfIdTb);
            sutSpecialSp2.AppendChild(sutSpecialCb);
            sutSpecialSp1.AppendChild(sutSpecialSp2);
            sutParentSp.AppendChild(sutSpecialSp1);

            var sutDt = new DataTemplate();
            sutDt.VisualTree = sutParentSp;
            sutDt.Resources.Add(sutParentSp, null);
            sutTc.CellTemplate = sutDt;

            Columns.Add(sutTc);

            foreach (var item in data)
                Items.Add(item);
        }

        private void DateShilfChange(object sender, System.Windows.Input.KeyEventArgs e)
        {
            ShilfChange(sender, e);
        }

        private void DateLateChange(object sender, KeyEventArgs e)
        {
            LateChange(sender, e);
        }

        private void DateSpecialChange(object sender, RoutedEventArgs e)
        {
            SpecialChange(sender);
        }

        private void SelectedDate(object sender, SelectionChangedEventArgs e)
        {
            SetupData();
        }

        private void LateChange(object sender, System.Windows.Input.KeyEventArgs e)
        {
            var tb = sender as TextBox;
            var ck = int.TryParse(tb.Text, out int tmp);

            if (e.Key == Key.Return)
            {
                if (!ck)
                    MessageBox.Show("Number only");
                    return;

                int.TryParse(tb.Text, out int late);

                int.TryParse(tb.TryFindParent<StackPanel>().
                                TryFindParent<StackPanel>().
                                TryFindParent<StackPanel>().
                                FindChild<TextBlock>().Text, out int shiftId);
                int.TryParse(tb.TryFindParent<StackPanel>().
                            TryFindParent<StackPanel>().
                            TryFindParent<StackPanel>().
                            FindChild<TextBlock>("tbStaffId").Text, out int staffId);

                if (Shilfs.IsSwitchShilf)
                {
                    var data = ModelMaker.Instance.GetShilf(shiftId);
                    data.time = NewTime(data.time, data.late, late);
                    data.late = late;
                    ModelMaker.Instance.UpdateShilf(data);
                }
                else
                {
                    var data = ModelMaker.Instance.GetShilfPm(shiftId);
                    data.time = NewTime(data.time, data.late, late);
                    data.late = late;
                    ModelMaker.Instance.UpdateShilfPm(data);
                }
                UpdateData(staffId);
            }
        }

        private string NewTime(string oldTime, int oldLate, int late)
        {
            if (oldTime.Equals("Off") || oldTime.Equals("On")) return oldTime;

            var textFormat = "{0}-{1}";
            var newLate = 0;
            var newStart = 0;
            var newEnd = 0;
            short.TryParse(oldTime.Split('-')[0].Split(':')[0], out short start);
            short.TryParse(oldTime.Split('-')[0].Split(':')[1], out short end);
            if (late > 0)
            {
                newLate = late + end;
                newStart = start + newLate / 60;
                newEnd = newLate % 60;
            }
            else
            {
                newLate = end - oldLate;
                if (newLate >= 0)
                {
                    newEnd = newLate;
                    newStart = start;
                }
                else
                {
                    newEnd = -newLate;
                    newStart = start - newEnd / 60 - 1;
                }
            }
            var newStartTime = $"{newStart:00}:{newEnd:00}";
            return string.Format(textFormat, newStartTime, oldTime.Split('-')[1]);
        }
        private void SpecialChange(object sender)
        {
            var tb = sender as CheckBox;
            int.TryParse(tb.TryFindParent<StackPanel>().
                            TryFindParent<StackPanel>().
                            TryFindParent<StackPanel>().
                            FindChild<TextBlock>().Text, out int shiftId);
            int.TryParse(tb.TryFindParent<StackPanel>().
                            TryFindParent<StackPanel>().
                            TryFindParent<StackPanel>().
                            FindChild<TextBlock>("tbStaffId").Text, out int staffId);
            if (Shilfs.IsSwitchShilf)
            {
                var data = ModelMaker.Instance.GetShilf(shiftId);
                data.special = (bool)tb.IsChecked;
                ModelMaker.Instance.UpdateShilf(data);

                var dataPm = ModelMaker.Instance.GetShilfPm(new ShilfPm(staffId, data.date));
                dataPm.special = (bool)tb.IsChecked;
                ModelMaker.Instance.UpdateShilfPm(dataPm);
            }
            else
            {
                var dataPm = ModelMaker.Instance.GetShilfPm(shiftId);
                dataPm.special = (bool)tb.IsChecked;
                ModelMaker.Instance.UpdateShilfPm(dataPm);

                var data = ModelMaker.Instance.GetShilf(new Shilf(staffId, dataPm.date));
                data.special = (bool)tb.IsChecked;
                ModelMaker.Instance.UpdateShilf(data);
            }
            UpdateData(staffId);
        }

        private void ShilfChange(object sender, System.Windows.Input.KeyEventArgs e)
        {
            var tb = sender as TextBox;
            var match = Regex.Match(tb.Text, @"^\d{2}:\d{2}-\d{2}:\d{2}$");

            if (e.Key == Key.Return)
            {
                if (!tb.Text.ToLower().Equals("on") || !tb.Text.ToLower().Equals("off") || !match.Success)
                {
                    MessageBox.Show("Wrong format.");
                    return;
                }

                int.TryParse(tb.TryFindParent<StackPanel>().TryFindParent<StackPanel>().FindChild<TextBlock>().Text, out int shiftId);
                int.TryParse(tb.TryFindParent<StackPanel>().
                            TryFindParent<StackPanel>().
                            FindChild<TextBlock>("tbStaffId").Text, out int staffId);
                if (Shilfs.IsSwitchShilf)
                {
                    var data = ModelMaker.Instance.GetShilf(shiftId);
                    data.time = DateString(tb.Text);
                    ModelMaker.Instance.UpdateShilf(data);
                }
                else
                {
                    var data = ModelMaker.Instance.GetShilfPm(shiftId);
                    data.time = DateString(tb.Text);
                    ModelMaker.Instance.UpdateShilfPm(data);
                }
                UpdateData(staffId);
            }
        }


        private void NewShilf(object sender, System.Windows.RoutedEventArgs e)
        {
            LoadConfig.Instance.NewShilfs();
        }

        private void SetupData()
        {
            DataGridControl.Instance.AddAll(ModelViewMaker.Instance.GetStaffs(Shilfs.CurrentDate));
        }

        private void UpdateData(int id)
        {
            var index = SelectedIndex;
            Items.Remove(Items.GetItemAt(index));
            Items.Insert(index, ModelViewMaker.Instance.GetStaff(Shilfs.CurrentDate, id));
            ScrollIntoView(Items.GetItemAt(index));
        }

        private string DateString(string text)
        {
            if (text.Equals("") || text.ToLower().Equals("off"))
                return "Off";
            else if (text.ToLower().Equals("on"))
                return "On";
            return text;
        }
    }
}
