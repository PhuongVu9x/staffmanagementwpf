﻿using StaffManagement.Models.ModelPatterns;
using System.Windows.Controls;

namespace StaffManagement.Components
{
    internal class MyComboBox : ComboBox
    {
        public MyComboBox()
        {
            ItemsSource = ModelMaker.Instance.GetPositions();
        }
    }
}
