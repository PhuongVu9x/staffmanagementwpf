﻿//using StaffManagement.Models.Entities;
using StaffManagement.Models.LisaDb.Entities;
using Syncfusion.Windows.Shared;
using System;
using System.Collections.Generic;
using System.Linq;

namespace StaffManagement.Models.ModelView
{
    internal class StaffView
    {
        public StaffView()
        {

        }

        public StaffView(Staff staff)
        {
            Id = staff.id;
            Code = staff.code;
            Name = staff.name;
            Dob = staff.dob;
            StaffId = staff.staff_id;
            PositionId = (int)staff.position_id;
            PositionName = staff.Position.name;
            Train = (decimal)staff.train;
            Salary = (decimal)staff.salary;
            Allowance = (decimal)staff.allowance;
            HireDay = staff.hire_day;
            Note = staff.note;
            Status = (byte)staff.status;
            OffDay = staff.off_day;
        }

        public StaffView(Staff staff, string currentDay)
        {
            Id = staff.id;
            Code = staff.code;
            Name = staff.name;
            Dob = staff.dob;
            StaffId = staff.staff_id;
            PositionId = (int)staff.position_id;
            PositionName = staff.Position.name;
            Salary = (decimal)staff.salary;
            Allowance = (decimal)staff.allowance;
            HireDay = staff.hire_day;
            Note = staff.note;
            Status = (byte)staff.status;
            OffDay = staff.off_day;
            Shilfs = new List<Shilf>();
            ShilfsPm = new List<ShilfPm>();
            Shilfs.AddRange(LisaDb.Entities.LisaDb.Shilfs.Where(item => DateTime.Parse(item.date) >= DateTime.Parse(currentDay) &&
                                                       DateTime.Parse(item.date) < DateTime.Parse(currentDay).AddDays(7) &&
                                                       item.staff_id == Id).ToList());
            ShilfsPm.AddRange(LisaDb.Entities.LisaDb.ShilfsPm.Where(item => DateTime.Parse(item.date) >= DateTime.Parse(currentDay) &&
                                                       DateTime.Parse(item.date) < DateTime.Parse(currentDay).AddDays(7) &&
                                                       item.staff_id == Id).ToList());
            SetupTime();
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Dob { get; set; }
        public string StaffId { get; set; }
        public int PositionId { get; set; }
        public string PositionName { get; set; }
        public decimal Train { get; set; }
        public decimal Salary { get; set; }
        public decimal Allowance { get; set; }
        public string HireDay { get; set; }
        public string Address { get; set; }
        public string Note { get; set; }
        public byte Status { get; set; }
        public string OffDay { get; set; }

        public string Monday { get; set; }
        public string MondayTime { get; set; }
        public int MondayShilfId { get; set; }
        public float MondayHours { get; set; }
        public string MondayColor { get; set; }
        public string MondayPm { get; set; }
        public string MondayTimePm { get; set; }
        public int MondayShilfIdPm { get; set; }
        public string MondayColorPm { get; set; }
        public int MondayLate { get; set; }
        public bool MondaySpecial { get; set; }
        public int MondayLatePm { get; set; }
        public bool MondaySpecialPm { get; set; }

        public string Tuesday { get; set; }
        public string TuesdayTime { get; set; }
        public int TuesdayShilfId { get; set; }
        public float TuesdayHours { get; set; }
        public string TuesdayColor { get; set; }
        public string TuesdayPm { get; set; }
        public string TuesdayTimePm { get; set; }
        public int TuesdayShilfIdPm { get; set; }
        public string TuesdayColorPm { get; set; }
        public int TuesdayLate { get; set; }
        public bool TuesdaySpecial { get; set; }
        public int TuesdayLatePm { get; set; }
        public bool TuesdaySpecialPm { get; set; }

        public string Wednesday { get; set; }
        public string WednesdayTime { get; set; }
        public int WednesdayShilfId { get; set; }
        public float WednesdayHours { get; set; }
        public string WednesdayColor { get; set; }
        public string WednesdayPm { get; set; }
        public string WednesdayTimePm { get; set; }
        public int WednesdayShilfIdPm { get; set; }
        public string WednesdayColorPm { get; set; }
        public int WednesdayLate { get; set; }
        public bool WednesdaySpecial { get; set; }
        public int WednesdayLatePm { get; set; }
        public bool WednesdaySpecialPm { get; set; }

        public string Thursday { get; set; }
        public string ThursdayTime { get; set; }
        public int ThursdayShilfId { get; set; }
        public float ThursdayHours { get; set; }
        public string ThursdayColor { get; set; }
        public string ThursdayPm { get; set; }
        public string ThursdayTimePm { get; set; }
        public int ThursdayShilfIdPm { get; set; }
        public string ThursdayColorPm { get; set; }
        public int ThursdayLate { get; set; }
        public bool ThursdaySpecial { get; set; }
        public int ThursdayLatePm { get; set; }
        public bool ThursdaySpecialPm { get; set; }

        public string Friday { get; set; }
        public string FridayTime { get; set; }
        public int FridayShilfId { get; set; }
        public float FridayHours { get; set; }
        public string FridayColor { get; set; }
        public string FridayPm { get; set; }
        public string FridayTimePm { get; set; }
        public int FridayShilfIdPm { get; set; }
        public string FridayColorPm { get; set; }
        public int FridayLate { get; set; }
        public bool FridaySpecial { get; set; }
        public int FridayLatePm { get; set; }
        public bool FridaySpecialPm { get; set; }

        public string Saturday { get; set; }
        public string SaturdayTime { get; set; }
        public int SaturdayShilfId { get; set; }
        public float SaturdayHours { get; set; }
        public string SaturdayColor { get; set; }
        public string SaturdayPm { get; set; }
        public string SaturdayTimePm { get; set; }
        public int SaturdayShilfIdPm { get; set; }
        public string SaturdayColorPm { get; set; }
        public int SaturdayLate { get; set; }
        public bool SaturdaySpecial { get; set; }
        public int SaturdayLatePm { get; set; }
        public bool SaturdaySpecialPm { get; set; }

        public string Sunday { get; set; }
        public string SundayTime { get; set; }
        public int SundayShilfId { get; set; }
        public float SundayHours { get; set; }
        public string SundayColor { get; set; }
        public string SundayPm { get; set; }
        public string SundayTimePm { get; set; }
        public int SundayShilfIdPm { get; set; }
        public string SundayColorPm { get; set; }
        public int SundayLate { get; set; }
        public bool SundaySpecial { get; set; }
        public int SundayLatePm { get; set; }
        public bool SundaySpecialPm { get; set; }

        public string PositionColor
        {
            get
            {
                if (PositionName.Equals("QL")) return _defaultColor;
                return _greenColor;
            }
        }

        public string NameColor
        {
            get
            {
                if (PositionName.Equals("QL")) return _defaultColor;
                return _yellowColor;
            }
        }

        public List<Shilf> Shilfs { get; set; }
        public List<ShilfPm> ShilfsPm { get; set; }

        private string _Off= "Off";
        private string _On = "On";
        private string _defaultColor = "#FFFFFFFF";
        private string _yellowColor = "#FFF5DD37";
        private string _blueColor = "#FFB4E0F3";
        private string _pinkColor = "#FFF9BCF1";
        private string _greenColor = "#FFA5BFA3";

        private void SetupTime()
        {
            foreach (var item in Shilfs)
                SetupDefault(item.date, item.time, item.id);

            foreach (var item in ShilfsPm)
                SetupDefaultPm(item.date, item.time, item.id);
        }

        private void SetupDefaultPm(string day, string time, int shilfId)
        {
            var date = DateTime.Parse(day).DayOfWeek;
            switch (date)
            {
                case DayOfWeek.Monday:
                    MondayTimePm = time;
                    MondayPm = DateTimeFormat(day);
                    MondayLatePm = LateHoursPm(shilfId);
                    MondayHours += Hours(time);
                    MondayColorPm = ShilfColor(time);
                    MondayShilfIdPm = shilfId;
                    MondaySpecialPm = SpecialPm(shilfId);
                    break;
                case DayOfWeek.Tuesday:
                    TuesdayTimePm = time;
                    TuesdayPm = DateTimeFormat(day);
                    TuesdayLatePm = LateHoursPm(shilfId);
                    TuesdayHours += Hours(time);
                    TuesdayColorPm = ShilfColor(time);
                    TuesdayShilfIdPm = shilfId;
                    TuesdaySpecialPm = SpecialPm(shilfId);
                    break;
                case DayOfWeek.Wednesday:
                    WednesdayTimePm = time;
                    WednesdayPm = DateTimeFormat(day);
                    WednesdayLatePm = LateHoursPm(shilfId);
                    WednesdayHours += Hours(time);
                    WednesdayColorPm = ShilfColor(time);
                    WednesdayShilfIdPm = shilfId;
                    WednesdaySpecialPm = SpecialPm(shilfId);
                    break;
                case DayOfWeek.Thursday:
                    ThursdayTimePm = time;
                    ThursdayPm = DateTimeFormat(day);
                    ThursdayLatePm = LateHoursPm(shilfId);
                    ThursdayHours += Hours(time);
                    ThursdayColorPm = ShilfColor(time);
                    ThursdayShilfIdPm = shilfId;
                    ThursdaySpecialPm = SpecialPm(shilfId);
                    break;
                case DayOfWeek.Friday:
                    FridayTimePm = time;
                    FridayPm = DateTimeFormat(day);
                    FridayLatePm = LateHoursPm(shilfId);
                    FridayHours += Hours(time);
                    FridayColorPm = ShilfColor(time);
                    FridayShilfIdPm = shilfId;
                    FridaySpecialPm = SpecialPm(shilfId);
                    break;
                case DayOfWeek.Saturday:
                    SaturdayTimePm = time;
                    SaturdayPm = DateTimeFormat(day);
                    SaturdayLatePm = LateHoursPm(shilfId);
                    SaturdayHours += Hours(time);
                    SaturdayColorPm = ShilfColor(time);
                    SaturdayShilfIdPm = shilfId;
                    SaturdaySpecialPm = SpecialPm(shilfId);
                    break;
                case DayOfWeek.Sunday:
                    SundayTimePm = time;
                    SundayPm = DateTimeFormat(day);
                    SundayLatePm = LateHoursPm(shilfId);
                    SundayHours += Hours(time);
                    SundayColorPm = ShilfColor(time);
                    SundayShilfIdPm = shilfId;
                    SundaySpecialPm = SpecialPm(shilfId);
                    break;
            }
        }

        private void SetupDefault(string day, string time, int shilfId)
        {
            var date = DateTime.Parse(day).DayOfWeek;
            switch (date)
            {
                case DayOfWeek.Monday:
                    MondayTime = time;
                    Monday = DateTimeFormat(day);
                    MondayLate = LateHours(shilfId);
                    MondayHours = Hours(time);
                    MondayColor = ShilfColor(time);
                    MondayShilfId = shilfId;
                    MondaySpecial = Special(shilfId);
                    break;
                case DayOfWeek.Tuesday:
                    TuesdayTime = time;
                    Tuesday = DateTimeFormat(day);
                    TuesdayLate = LateHours(shilfId);
                    TuesdayHours = Hours(time);
                    TuesdayColor = ShilfColor(time);
                    TuesdayShilfId = shilfId;
                    TuesdaySpecial = Special(shilfId);
                    break;
                case DayOfWeek.Wednesday:
                    WednesdayTime = time;
                    Wednesday = DateTimeFormat(day);
                    WednesdayLate = LateHours(shilfId);
                    WednesdayHours = Hours(time);
                    WednesdayColor = ShilfColor(time);
                    WednesdayShilfId = shilfId;
                    WednesdaySpecial = Special(shilfId);
                    break;
                case DayOfWeek.Thursday:
                    ThursdayTime = time;
                    Thursday = DateTimeFormat(day);
                    ThursdayLate = LateHours(shilfId);
                    ThursdayHours = Hours(time);
                    ThursdayColor = ShilfColor(time);
                    ThursdayShilfId = shilfId;
                    ThursdaySpecial = Special(shilfId);
                    break;
                case DayOfWeek.Friday:
                    FridayTime = time;
                    Friday = DateTimeFormat(day);
                    FridayLate = LateHours(shilfId);
                    FridayHours = Hours(time);
                    FridayColor = ShilfColor(time);
                    FridayShilfId = shilfId;
                    FridaySpecial = Special(shilfId);
                    break;
                case DayOfWeek.Saturday:
                    SaturdayTime = time;
                    Saturday = DateTimeFormat(day);
                    SaturdayLate = LateHours(shilfId);
                    SaturdayHours = Hours(time);
                    SaturdayColor = ShilfColor(time);
                    SaturdayShilfId = shilfId;
                    SaturdaySpecial = Special(shilfId);
                    break;
                case DayOfWeek.Sunday:
                    SundayTime = time;
                    Sunday = DateTimeFormat(day);
                    SundayLate = LateHours(shilfId);
                    SundayHours = Hours(time);
                    SundayColor = ShilfColor(time);
                    SundayShilfId = shilfId;
                    SundaySpecial = Special(shilfId);
                    break;
            }
        }

        private bool Special(int shilfId)
        {
            return LisaDb.Entities.LisaDb.Shilfs.First(item => item.id == shilfId).special;
        }

        private bool SpecialPm(int shilfId)
        {
            return LisaDb.Entities.LisaDb.ShilfsPm.First(item => item.id == shilfId).special;
        }

        private int LateHours(int shilfId)
        {
            return LisaDb.Entities.LisaDb.Shilfs.First(item => item.id == shilfId).late;
        }

        private int LateHoursPm(int shilfId)
        {
            return LisaDb.Entities.LisaDb.ShilfsPm.First(item => item.id == shilfId).late;
        }

        private float Hours(string time)
        {
            if (time.Equals(_Off)) return 0;
            if (time.Equals(_On)) return 12;

            short.TryParse(time.Split('-')[0].Split(':')[0], out short start);
            short.TryParse(time.Split('-')[1].Split(':')[0], out short end);

            short.TryParse(time.Split('-')[0].Split(':')[1], out short start2);
            short.TryParse(time.Split('-')[1].Split(':')[1], out short end2);

            var tmpTime = end - start;
            var tmpTime2 = end2 - start2;
            if (tmpTime2 == 0)
                return tmpTime;

            return (float)Math.Round(tmpTime + (tmpTime2 / 60f),2);
        }

        private float GetLate(int late)
        {
            return float.Parse($"{late / 60f:F2}");
        }

        private string ShilfColor(string time)
        {
            if (time.Equals(_Off) || time.Equals(_On)) return _yellowColor;
            if (short.Parse(time.Split('-')[0].Split(':')[0]) <= 12) return _blueColor;
            if (short.Parse(time.Split('-')[1].Split(':')[0]) > 12) return _pinkColor;
            return _defaultColor;
        }

        private string DateTimeFormat(string day)
        {
            var stringFormat = "{0}/{1}/{2}";
            var data = day.Split('/');
            var newDay = $"{data[1]:00}";
            var newMonth = $"{data[0]:00}";
            return String.Format(stringFormat, newDay, newMonth, data[2]);
        }
    }
}
