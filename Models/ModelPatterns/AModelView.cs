﻿using StaffManagement.Models.Entities;
using System.Collections.Generic;

namespace StaffManagement.Models.ModelPatterns
{
    internal abstract class AModelView<T>
    {
        public abstract List<T> Gets();
        public abstract List<T> Gets(string data);
        public abstract List<T> Search(string data);
    }
}
