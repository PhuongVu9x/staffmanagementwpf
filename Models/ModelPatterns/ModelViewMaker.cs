﻿using StaffManagement.Models.Business;
using StaffManagement.Models.ModelView;
using System;
using System.Collections.Generic;
using System.Linq;
using static System.Net.Mime.MediaTypeNames;
using SalaryView = StaffManagement.Models.LisaDb.ModelView.SalaryView;
namespace StaffManagement.Models.ModelPatterns
{
    internal class ModelViewMaker
    {
        private static readonly Lazy<ModelViewMaker> lazy = new Lazy<ModelViewMaker>(() => new ModelViewMaker());
        public static ModelViewMaker Instance => lazy.Value;

        private StaffViewMgr _staffViewMgr;
        private SalaryViewMgr _salaryViewMgr;

        public ModelViewMaker()
        {
            _staffViewMgr = new StaffViewMgr();
            _salaryViewMgr = new SalaryViewMgr();
        }
        public StaffView GetStaff(string data,int id) => _staffViewMgr.Get(data, id);
        public List<StaffView> GetStaffs() => _staffViewMgr.Gets().OrderBy(item => item.Code).ToList();
        public List<StaffView> GetStaffs(string data) => _staffViewMgr.Gets(data).OrderBy(item => item.Code).ToList();
        public List<StaffView> SearchStaff(string data) => _staffViewMgr.Search(data).OrderBy(item => item.Code).ToList();
        
        public List<SalaryView> GetSalarys() => _salaryViewMgr.Gets().OrderBy(item => item.Code).ToList();
        public List<SalaryView> GetSalarys(string data) => _salaryViewMgr.Gets(data).OrderBy(item => item.Code).ToList();
        public List<SalaryView> SearchSalarys(string fromDate,string ToDate) => _salaryViewMgr.Search(fromDate, ToDate).OrderBy(item => item.Code).ToList();
    }
}
