﻿using StaffManagement.Models.ModelPatterns;
using StaffManagement.Models.ModelView;
using System.Collections.Generic;
using System.Linq;

namespace StaffManagement.Models.Business
{
    internal class StaffViewMgr : AModelView<StaffView>
    {
        public StaffView Get(string data,int id) => new StaffView(LisaDb.ModelPattern.ModelMaker.Instance.GetStaff(id), data);

        public override List<StaffView> Gets() => LisaDb.ModelPattern.ModelMaker.Instance.GetStaffs().Select(item => new StaffView(item)).ToList();

        public override List<StaffView> Gets(string data) => LisaDb.ModelPattern.ModelMaker.Instance.GetActiveStaffs().Select(item => new StaffView(item, data)).ToList();

        public override List<StaffView> Search(string data) => Gets().Where(item => item.Name.ToLower().Contains(data.ToLower()) || item.Code.ToLower().Contains(data.ToLower()) ||
                                                                                  item.Dob.ToLower().Contains(data.ToLower()) || item.PositionName.ToLower().Contains(data.ToLower()) ||
                                                                                  item.Salary.ToString().Contains(data) || item.Allowance.ToString().Contains(data) ||
                                                                                  item.HireDay.ToLower().Contains(data)).ToList();
    }
}
