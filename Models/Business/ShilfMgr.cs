﻿using StaffManagement.Controllers;
using StaffManagement.Models.Entities;
using StaffManagement.Models.ModelPatterns;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Data.Entity.Migrations;
using System.Linq;


namespace StaffManagement.Models.Business
{
    internal class ShilfMgr : AModelDb<Shilf>
    {
        public override Shilf Create(Shilf data)
        {
            try
            {
                if (IsExists(data))
                {
                    return null;
                }
                entities.Shilfs.Add(data);
                entities.SaveChanges();
                return data;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public override bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public override Shilf Get(int id)
        {
            return entities.Shilfs.First(item => item.id == id);
        }

        public override List<Shilf> Gets()
        {
            throw new NotImplementedException();
        }
        public override bool IsExists(string data)
        {
            return entities.Shilfs.Any(item => item.date.Equals(data));
        }

        public bool IsExists(Shilf data)
        {
            return entities.Shilfs.Any(item => item.date.Equals(data.date) && item.staff_id == data.staff_id);
        }

        public override List<Shilf> Search(string data)
        {
            throw new NotImplementedException();
        }

        public override Shilf Update(Shilf data)
        {
            try
            {
                entities.Shilfs.AddOrUpdate(data);
                entities.SaveChanges();
                MessageControl.Instance.Success();
                return data;
            }
            catch (EntityException ex)
            {
                MessageControl.Instance.Fail();
                Console.WriteLine(ex);
                return null;
            }
        }
    }
}
