﻿using StaffManagement.Controllers;
using StaffManagement.Models.Entities;
using StaffManagement.Models.ModelPatterns;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;

namespace StaffManagement.Models.Business
{
    internal class StaffMgr : AModelDb<Staff>
    {
        public override Staff Create(Staff data)
        {
            try
            {
                if (IsExists(data.name))
                {
                    MessageControl.Instance.Exists();
                    return null;
                }
                entities.Staffs.Add(data);
                entities.SaveChanges();
                MessageControl.Instance.Success();
                return data;
            }
            catch (Exception)
            {
                MessageControl.Instance.Fail();
                return null;
            }
        }

        public override bool Delete(int id)
        {
            try
            {
                var data = entities.Staffs.First(item => item.id == id);
                entities.Staffs.Remove(data);
                MessageControl.Instance.Success();
                return true;
            }
            catch (Exception)
            {
                MessageControl.Instance.Fail();
                return false;
            }
        }

        public override Staff Get(int id)
        {
            try
            {
                return entities.Staffs.First(item => item.id == id);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public override List<Staff> Gets()
        {
            try
            {
                return entities.Staffs.ToList();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public override bool IsExists(string data)
        {
            return entities.Staffs.Any(item => item.name.Equals(data));
        }

        public override List<Staff> Search(string data)
        {
            try
            {
                return entities.Staffs.Where(item => item.name.Contains(data)).ToList();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public override Staff Update(Staff data)
        {

            try
            {
                entities.Staffs.AddOrUpdate(data);
                entities.SaveChanges();
                MessageControl.Instance.Success();
                return data;
            }
            catch (Exception)
            {
                MessageControl.Instance.Fail();
                return null;
            }
        }
    }
}
