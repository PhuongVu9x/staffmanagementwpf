﻿using StaffManagement.Controllers;
using StaffManagement.Models.ModelPatterns;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using Position = StaffManagement.Models.Entities.Position;

namespace StaffManagement.Models.Business
{
    internal class PositionMgr : AModelDb<Position>
    {
        public override Position Create(Position data)
        {
            try
            {
                if (IsExists(data.name))
                {
                    MessageControl.Instance.Exists();
                    return null;
                }
                entities.Positions.Add(data);
                entities.SaveChanges();
                MessageControl.Instance.Success();
                return data;
            }
            catch (Exception)
            {
                MessageControl.Instance.Fail();
                return null;
            }
        }

        public override bool Delete(int id)
        {
            try
            {
                var data = entities.Positions.First(item => item.id == id);
                entities.Positions.Remove(data);
                MessageControl.Instance.Success();
                return true;
            }
            catch (Exception)
            {
                MessageControl.Instance.Fail();
                return false;
            }
        }

        public override Position Get(int id)
        {
            try
            {
                return entities.Positions.First(item => item.id == id);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public override List<Position> Gets()
        {
            try
            {
                return entities.Positions.ToList();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public override bool IsExists(string data)
        {
            return entities.Positions.Any(item => item.name.Equals(data));
        }

        public override List<Position> Search(string data)
        {
            try
            {
                return entities.Positions.Where(item => item.name.Contains(data)).ToList();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public override Position Update(Position data)
        {

            try
            {
                entities.Positions.AddOrUpdate(data);
                entities.SaveChanges();
                MessageControl.Instance.Success();
                return data;
            }
            catch (Exception)
            {
                MessageControl.Instance.Fail();
                return null;
            }
        }
    }
}
