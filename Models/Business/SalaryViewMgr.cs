﻿using StaffManagement.Models.LisaDb.ModelView;
using StaffManagement.Models.ModelPatterns;
using System;
using System.Collections.Generic;
using System.Linq;

namespace StaffManagement.Models.Business
{
    internal class SalaryViewMgr : AModelView<SalaryView>
    {
        public override List<SalaryView> Gets()
        {
            return LisaDb.Entities.LisaDb.Staffs.Select(item => new SalaryView(item.code)).ToList();
        }

        public override List<SalaryView> Gets(string data)
        {
            throw new NotImplementedException();
        }

        public override List<SalaryView> Search(string data)
        {
            return LisaDb.Entities.LisaDb.Staffs.Where(i => i.status == 1)
                         .Select(item => new SalaryView(item.id,item.code,data)).Where(it => it.CurrentSalary > 0).ToList();
        }

        public List<SalaryView> Search(string fromDate,string toDate)
        {
            return LisaDb.Entities.LisaDb.Staffs.Where(i => i.status == 1)
                         .Select(item => new SalaryView(item.id,item.code, fromDate,toDate)).Where(it => it.CurrentSalary > 0).ToList();
        }
    }
}
