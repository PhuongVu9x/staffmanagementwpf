﻿using StaffManagement.Models.LisaDb.Entities;
using StaffManagement.Models.LisaDb.ModelPattern;
using Syncfusion.Windows.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace StaffManagement.Models.LisaDb.ModelView
{
    public class SalaryView
    {
        public SalaryView(int id, string code, string fromDate,string toDate)
        {
            var startDay = DateTime.Parse(fromDate);
            var endDay = DateTime.Parse(toDate);
            Id = id;
            Code = code;
            Shilfs = new List<Shilf>();
            ShilfsPm = new List<ShilfPm>();
            Shilfs.AddRange(Entities.LisaDb.Shilfs.Where(i => i.staff_id == id && DateTime.Parse(i.date) >= startDay &&
                                                           DateTime.Parse(i.date) < endDay));
            ShilfsPm.AddRange(Entities.LisaDb.ShilfsPm.Where(i => i.staff_id == id && DateTime.Parse(i.date) >= startDay &&
                                                       DateTime.Parse(i.date) < endDay));
            Fine = GetFine(id);
            Name = ModelMaker.Instance.GetStaff(Code).name;
            PositionName = ModelMaker.Instance.GetStaff(Code).Position.name;
            Salary = ModelMaker.Instance.GetStaff(Code).salary;
            Allowance = ModelMaker.Instance.GetStaff(Code).allowance;
            Hours = (float)Math.Round(TotalHours(), 2);
            TotalAllowance = GetAllowance();
            TotalSalary = GetSalary(fromDate, id);
        }

        public SalaryView(int id,string code, string time)
        {
            var startDay = DateTime.Parse(time);
            var endDay = startDay.AddMonths(1);
            Id = id;
            Code = code;
            Shilfs = new List<Shilf>();
            ShilfsPm = new List<ShilfPm>();
            Shilfs.AddRange(Entities.LisaDb.Shilfs.Where(i => i.staff_id == id && DateTime.Parse(i.date) >= startDay &&
                                                           DateTime.Parse(i.date) < endDay));
            ShilfsPm.AddRange(Entities.LisaDb.ShilfsPm.Where(i => i.staff_id == id && DateTime.Parse(i.date) >= startDay &&
                                                       DateTime.Parse(i.date) < endDay));
            Fine = GetFine(id);
            Name = ModelMaker.Instance.GetStaff(Code).name;
            Address = ModelMaker.Instance.GetStaff(Code).address;
            PositionName = ModelMaker.Instance.GetStaff(Code).Position.name;
            Salary = ModelMaker.Instance.GetStaff(Code).salary;
            Allowance = ModelMaker.Instance.GetStaff(Code).allowance;
            Hours = (float)Math.Round(TotalHours(),2);
            TotalAllowance = GetAllowance();
            TotalSalary = GetSalary(time,id);
        }

        public SalaryView(string code)
        {
            Code = code;
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public List<Shilf> Shilfs { get; set; }
        public List<ShilfPm> ShilfsPm { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string PositionName { get; set; }
        public decimal Salary { get; set; }
        public decimal Allowance { get; set; }
        public string Fine { get; set; }
        public int Late { get; set; }
        public float Hours { get; set; }
        public string TotalAllowance { get; set; }
        public string TotalSalary { get; set; }

        private float TotalHours()
        {
            var totalHours = 0f;

            switch (PositionName)
            {
                case "PC":
                case "PV-TN-BH":
                case "Bếp":
                    totalHours += Shilfs.Sum(item => GetHours(item.time)) - Shilfs.Sum(item => GetLate(item.late))
                                + ShilfsPm.Sum(item => GetHours(item.time)) - ShilfsPm.Sum(item => GetLate(item.late));
                    break;
                case "TC":
                    totalHours += Shilfs.Where(item => item.date.Equals("On")).Count() * 8
                                + ShilfsPm.Where(item => item.date.Equals("On")).Count() * 8;
                    break;
                case "CEO":
                case "QL":
                case "Bảo Vệ":
                    break;
            }
            return totalHours;
        }

        public decimal CurrentFine { get; set; }

        private string GetFine(int id)
        {
            var textFormat = "{0:N0}";
            CurrentFine = (Shilfs.Where(item => item.staff_id == id).Sum(i => i.late) + ShilfsPm.Where(item => item.staff_id == id).Sum(i => i.late)) * Entities.LisaDb.AppConfig.Fine;

            return string.Format(textFormat, CurrentFine);
        }


        private float GetHours(string time)
        {
            if (time.Equals("Off")) return 0;
            if (time.Equals("On")) return 12;

            short.TryParse(time.Split('-')[0].Split(':')[0], out short start);
            short.TryParse(time.Split('-')[1].Split(':')[0], out short end);

            short.TryParse(time.Split('-')[0].Split(':')[1], out short start2);
            short.TryParse(time.Split('-')[1].Split(':')[1], out short end2);

            var tmpTime = end - start;
            var tmpTime2 = end2 - start2;
            if (tmpTime2 == 0)
                return tmpTime;

            return tmpTime + (tmpTime2 / 60f);
        }

        private float GetLate(int late)
        {
            return float.Parse($"{late / 60f:F2}");
        }

        public decimal CurrentAllowance { get; set; }

        private string GetAllowance()
        {
            var textFormat = "{0:N0}";
            var salary = string.Empty;

            switch (PositionName)
            {
                case "CEO":
                case "QL":
                case "TC":
                case "PC":
                case "PV-TN-BH":
                case "Bếp":
                    salary = string.Format(textFormat, Allowance);
                    break;
                case "Bảo vệ":
                    CurrentAllowance = Shilfs.Where(item => item.time.Equals("On")).Count() * Entities.LisaDb.AppConfig.SercurityAllowance;
                    salary = string.Format(textFormat, CurrentAllowance);
                    break;
            }
            return salary;
        }
        public decimal CurrentSalary { get; set; }

        private string GetSalary(string time,int id)
        {
            var textFormat = "{0:N0}";
            decimal salary = 0;
            var date = DateTime.Parse(time);
            var tmpSalary = Salary / DateTime.DaysInMonth(date.Year, date.Month);
            switch (PositionName)
            {
                case "CEO":
                case "QL":
                case "TC":
                    salary += GetManagerSalary(tmpSalary) + Allowance;
                    CurrentSalary = salary;
                    break;
                case "PC":
                case "PV-TN-BH":
                case "Bếp":
                    CurrentSalary = GetStaffSalary() + Allowance;
                    salary += GetStaffSalary() + Allowance - CurrentFine;
                    break;
                case "Bảo vệ":
                    salary += Shilfs.Where(item => item.time.Equals("On")).Count() * (tmpSalary + Entities.LisaDb.AppConfig.SercurityAllowance) + CurrentAllowance;
                    CurrentSalary = salary;
                    break;
            }

            decimal roundSalary = 0;
            if(salary / 1000 % 10 == 0)
                roundSalary = ((int)float.Parse($"{salary / 10000:F1}")) * 10000;
            else if(float.Parse($"{salary / 1000:F1}") % 10 > 5)
                roundSalary = ((int)float.Parse($"{salary / 10000:F1}") + 1) * 10000;
            else if(float.Parse($"{salary / 1000:F1}") % 10 < 5 && float.Parse($"{salary / 1000:F1}") % 10 > 0)
                roundSalary = (int)float.Parse($"{float.Parse($"{(int)(salary / 10000)}")}5") * 1000;
            else
                roundSalary = (int)float.Parse($"{salary / 1000:F1}") * 1000;
            return string.Format(textFormat, roundSalary);
        }

        private decimal GetManagerSalary(decimal tmpSalary)
        {
            decimal salary = 0;

            foreach (var item in Shilfs)
            {
                if(item.time.Equals("On"))
                    if (item.special)
                        salary += tmpSalary * 4;
                    else
                        salary += tmpSalary;
            }   

            return salary;
        }


        private decimal GetStaffSalary()
        {
            decimal salary = 0;

            foreach (var item in Shilfs)
            {
                if (item.special)
                    salary += Salary * 4 * (int)(GetHours(item.time) * 10) / 10;
                else
                    salary += Salary * (int)(GetHours(item.time) * 10) / 10;
            }

            foreach (var item in ShilfsPm)
            {
                if (item.special)
                    salary += Salary * 4 * (int)(GetHours(item.time) * 10) / 10;
                else
                    salary += Salary * (int)(GetHours(item.time) * 10) / 10;
            }

            return salary;
        }
    }
}
