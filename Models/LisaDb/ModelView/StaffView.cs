﻿using StaffManagement.Models.LisaDb.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace StaffManagement.Models.LisaDb.ModelView
{
    internal class StaffView
    {
        public StaffView()
        {

        }

        public StaffView(Staff staff)
        {
            Id = staff.id;
            Code = staff.code;
            Name = staff.name;
            Dob = staff.dob;
            StaffId = staff.staff_id;
            PositionId = (int)staff.position_id;
            PositionName = staff.Position.name;
            Salary = (decimal)staff.salary;
            Allowance = (decimal)staff.allowance;
            HireDay = staff.hire_day;
            Note = staff.note;
            Status = (byte)staff.status;
            OffDay = staff.off_day;
        }

        public StaffView(Staff staff, string currentDay)
        {
            Id = staff.id;
            Code = staff.code;
            Name = staff.name;
            Dob = staff.dob;
            StaffId = staff.staff_id;
            PositionId = (int)staff.position_id;
            PositionName = staff.Position.name;
            Salary = (decimal)staff.salary;
            Allowance = (decimal)staff.allowance;
            HireDay = staff.hire_day;
            Note = staff.note;
            Status = (byte)staff.status;
            OffDay = staff.off_day;
            Shilfs = new List<Shilf>();
            Shilfs.AddRange(staff.Shilfs.Where(item => DateTime.Parse(item.date) >= DateTime.Parse(currentDay) &&
                                                       DateTime.Parse(item.date) < DateTime.Parse(currentDay).AddDays(7) &&
                                                       item.staff_id == Id).ToList());
            ShilfsPm.AddRange(staff.ShilfsPm.Where(item => DateTime.Parse(item.date) >= DateTime.Parse(currentDay) &&
                                                       DateTime.Parse(item.date) < DateTime.Parse(currentDay).AddDays(7) &&
                                                       item.staff_id == Id).ToList());
            SetupTime();
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Dob { get; set; }
        public string StaffId { get; set; }
        public int PositionId { get; set; }
        public string PositionName { get; set; }
        public decimal Salary { get; set; }
        public decimal Allowance { get; set; }
        public string HireDay { get; set; }
        public string Address { get; set; }
        public string Note { get; set; }
        public byte Status { get; set; }
        public string OffDay { get; set; }

        public string Monday { get; set; }
        public string MondayTime { get; set; }
        public int MondayShilfId { get; set; }
        public float MondayHours { get; set; }
        public string MondayColor { get; set; }

        public string Tuesday { get; set; }
        public string TuesdayTime { get; set; }
        public int TuesdayShilfId { get; set; }
        public float TuesdayHours { get; set; }
        public string TuesdayColor { get; set; }

        public string Wednesday { get; set; }
        public string WednesdayTime { get; set; }
        public int WednesdayShilfId { get; set; }
        public float WednesdayHours { get; set; }
        public string WednesdayColor { get; set; }

        public string Thursday { get; set; }
        public string ThursdayTime { get; set; }
        public int ThursdayShilfId { get; set; }
        public float ThursdayHours { get; set; }
        public string ThursdayColor { get; set; }

        public string Friday { get; set; }
        public string FridayTime { get; set; }
        public int FridayShilfId { get; set; }
        public float FridayHours { get; set; }
        public string FridayColor { get; set; }

        public string Saturday { get; set; }
        public string SaturdayTime { get; set; }
        public int SaturdayShilfId { get; set; }
        public float SaturdayHours { get; set; }
        public string SaturdayColor { get; set; }

        public string Sunday { get; set; }
        public string SundayTime { get; set; }
        public int SundayShilfId { get; set; }
        public float SundayHours { get; set; }
        public string SundayColor { get; set; }

        public string PositionColor
        {
            get
            {
                if (PositionName.Equals("QL")) return _defaultColor;
                return _greenColor;
            }
        }

        public string NameColor
        {
            get
            {
                Console.WriteLine(PositionName);
                if (PositionName.Equals("QL")) return _defaultColor;
                return _yellowColor;
            }
        }

        public List<Shilf> Shilfs { get; set; }
        public List<ShilfPm> ShilfsPm { get; set; }

        private string _default = "Off";
        private string _defaultColor = "#FFFFFFFF";
        private string _yellowColor = "#FFF5DD37";
        private string _blueColor = "#FFB4E0F3";
        private string _pinkColor = "#FFF9BCF1";
        private string _greenColor = "#FFA5BFA3";

        private void SetupTime()
        {
            foreach (var item in Shilfs)
                SetupDefault(item.date, item.time, item.id);
        }

        private void SetupDefault(string day, string time, int shilfId)
        {
            var date = DateTime.Parse(day).DayOfWeek;
            switch (date)
            {
                case DayOfWeek.Monday:
                    MondayTime = time;
                    Monday = DateTimeFormat(day);
                    MondayHours = Hours(time);
                    MondayColor = ShilfColor(time);
                    MondayShilfId = shilfId;
                    break;
                case DayOfWeek.Tuesday:
                    TuesdayTime = time;
                    Tuesday = DateTimeFormat(day);
                    TuesdayHours = Hours(time);
                    TuesdayColor = ShilfColor(time);
                    TuesdayShilfId = shilfId;
                    break;
                case DayOfWeek.Wednesday:
                    WednesdayTime = time;
                    Wednesday = DateTimeFormat(day);
                    WednesdayHours = Hours(time);
                    WednesdayColor = ShilfColor(time);
                    WednesdayShilfId = shilfId;
                    break;
                case DayOfWeek.Thursday:
                    ThursdayTime = time;
                    Thursday = DateTimeFormat(day);
                    ThursdayHours = Hours(time);
                    ThursdayColor = ShilfColor(time);
                    ThursdayShilfId = shilfId;
                    break;
                case DayOfWeek.Friday:
                    FridayTime = time;
                    Friday = DateTimeFormat(day);
                    FridayHours = Hours(time);
                    FridayColor = ShilfColor(time);
                    FridayShilfId = shilfId;
                    break;
                case DayOfWeek.Saturday:
                    SaturdayTime = time;
                    Saturday = DateTimeFormat(day);
                    SaturdayHours = Hours(time);
                    SaturdayColor = ShilfColor(time);
                    SaturdayShilfId = shilfId;
                    break;
                case DayOfWeek.Sunday:
                    SundayTime = time;
                    Sunday = DateTimeFormat(day);
                    SundayHours = Hours(time);
                    SundayColor = ShilfColor(time);
                    SundayShilfId = shilfId;
                    break;
            }
        }

        private float Hours(string time)
        {
            if (time.Equals(_default)) return 0;

            short.TryParse(time.Split('-')[0].Split(':')[0], out short start);
            short.TryParse(time.Split('-')[1].Split(':')[0], out short end);

            short.TryParse(time.Split('-')[0].Split(':')[1], out short start2);
            short.TryParse(time.Split('-')[1].Split(':')[1], out short end2);

            if(end2 - start2 > 0)
                return end - start + 0.5f;
            else if (end2 - start2 < 0)
                return end - start - 0.5f;

            return end - start;
        }

        private string ShilfColor(string time)
        {
            if (time.Equals(_default)) return _yellowColor;
            if (short.Parse(time.Split('-')[0].Split(':')[0]) <= 12) return _blueColor;
            if (short.Parse(time.Split('-')[1].Split(':')[0]) > 12) return _pinkColor;
            return _defaultColor;
        }

        private string DateTimeFormat(string day)
        {
            var stringFormat = "{0}/{1}/{2}";
            var data = day.Split('/');
            var newDay = $"{data[1]:00}";
            var newMonth = $"{data[0]:00}";
            return String.Format(stringFormat, newDay, newMonth, data[2]);
        }
    }
}
