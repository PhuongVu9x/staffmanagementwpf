﻿using StaffManagement.Controllers;
using StaffManagement.Models.LisaDb.ModelView;
using System;
using System.Collections.Generic;

namespace StaffManagement.Models.LisaDb.Entities
{
    internal class LisaDb
    {
        public static List<Position> Positions
        {
            get;
            set;
        }

        public static List<Shilf> Shilfs
        {
            get;
            set;
        }
        
        public static List<ShilfPm> ShilfsPm
        {
            get;
            set;
        }

        public static List<Staff> Staffs
        {
            get;
            set;
        }
        
        public static List<TimeKeeping> TimeKeepings
        {
            get;
            set;
        } 
        
        public static AppConfig AppConfig
        {
            get;
            set;
        }
        
        public static List<Receipt> Receipts
        {
            get;
            set;
        }

    }
}
