﻿using StaffManagement.Models.LisaDb.ModelPattern;
using StaffManagement.Views.UserControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StaffManagement.Models.LisaDb.Entities
{
    public class Staff
    {
        public Staff()
        {
            Shilfs = new List<Shilf>();
        }

        public Staff(string code, string name, string dob, string staff_id, int position_id,decimal train, decimal salary, decimal allowance, string hire_day, string address, string note)
        {
            this.id = LisaDb.Staffs.Count() + 1;
            this.code = code;
            this.name = name;
            this.dob = dob;
            this.staff_id = staff_id;
            this.position_id = position_id;
            this.train = train;
            this.salary = salary;
            this.allowance = allowance;
            this.hire_day = hire_day;
            this.address = address;
            this.note = note;
            this.status = 1;
            this.off_day = "";
            Position = ModelMaker.Instance.GetPosition(position_id);
            Shilfs = new List<Shilf>();
            ShilfsPm = new List<ShilfPm>();
        }

        public Staff(int id, string code, string name, string dob, string staff_id, int position_id, decimal salary, decimal allowance, string hire_day, string off_day, string address, string note, byte status)
        {
            this.id = id;
            this.code = code;
            this.name = name;
            this.dob = dob;
            this.staff_id = staff_id;
            this.position_id = position_id;
            this.salary = salary;
            this.allowance = allowance;
            this.hire_day = hire_day;
            this.off_day = off_day;
            this.address = address;
            this.note = note;
            this.status = status;
            Position = ModelMaker.Instance.GetPosition(position_id);
            Shilfs = new List<Shilf>();
            Shilfs.AddRange(ModelMaker.Instance.SearchShilfs(staff_id));
        }

        public int id { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public string dob { get; set; }
        public string staff_id { get; set; }
        public int position_id { get; set; }
        public decimal train { get; set; }
        public decimal salary { get; set; }
        public decimal allowance { get; set; }
        public string hire_day { get; set; }
        public string off_day { get; set; }
        public string address { get; set; }
        public string note { get; set; }
        public byte status { get; set; }

        public Position Position { get; set; }
        public List<Shilf> Shilfs { get; set; }
        public List<ShilfPm> ShilfsPm { get; set; }
    }
}
