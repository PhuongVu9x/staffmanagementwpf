﻿using StaffManagement.Models.LisaDb.ModelPattern;
using System.Collections.Generic;
using System.Linq;

namespace StaffManagement.Models.LisaDb.Entities
{
    public class Position
    {
        public Position()
        {
           
        }

        public Position(int id)
        {
            this.id = id;
        }

        public Position(string name)
        {
            id = LisaDb.Positions.Count() + 1;
            this.name = name;
            status = true;
            Staffs = new List<Staff>();
        }

        public int id { get; set; }
        public string name { get; set; }
        public bool status { get; set; }

        public List<Staff> Staffs { get; set; }
    }
}
