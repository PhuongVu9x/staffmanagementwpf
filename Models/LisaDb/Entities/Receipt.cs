﻿
using StaffManagement.Controllers;
using StaffManagement.Models.LisaDb.ModelView;
using System.Linq;

namespace StaffManagement.Models.LisaDb.Entities
{
    internal class Receipt
    {
        public Receipt()
        {
           
        }

        public Receipt(SalaryView salaryView, string fromDate, string toDate, string path)
        {
            Id = LisaDb.Receipts.Count() + 1;
            Code = salaryView.Code;
            Name = salaryView.Name;
            PositionName = salaryView.PositionName;
            File = AppSettings.FileName(Name, fromDate, toDate,path);
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string PositionName { get; set; }
        public string File { get; set; }
    }
}
