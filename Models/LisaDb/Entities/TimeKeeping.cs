﻿
using static System.Net.Mime.MediaTypeNames;
using System;
using System.Threading;
using System.Windows.Media.Animation;

namespace StaffManagement.Models.LisaDb.Entities
{
    public class TimeKeeping
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Date { get; set; }
        public string Time1 { get; set; }
        public string Time2 { get; set; }
        public float Hours { get; set; }
    }
}
