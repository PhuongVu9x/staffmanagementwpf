﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.TaskbarClock;

namespace StaffManagement.Models.LisaDb.Entities
{
    public class ShilfPm
    {
        public ShilfPm()
        {

        }

        public ShilfPm(int staff_id, string date)
        {
            this.id = LisaDb.Shilfs.Count() + 1;
            this.date = date;
            this.special = false;
            this.time = "Off";
            this.staff_id = staff_id;
        }

        public ShilfPm(int id, string date, string time, int staff_id)
        {
            this.id = id;
            this.date = date;
            this.time = time;
            this.staff_id = staff_id;
            Staff = LisaDb.Staffs.First(item => item.id == staff_id);
        }

        public int id { get; set; }
        public string date { get; set; }
        public string time { get; set; }
        public int late { get; set; }
        public bool special { get; set; }
        public int staff_id { get; set; }

        public virtual Staff Staff { get; set; }
    }
}
