﻿using StaffManagement.Models.LisaDb.Business;
using StaffManagement.Models.LisaDb.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace StaffManagement.Models.LisaDb.ModelPattern
{
    internal class ModelMaker
    {
        private static readonly Lazy<ModelMaker> lazy = new Lazy<ModelMaker>(() => new ModelMaker());
        public static ModelMaker Instance => lazy.Value;

        private StaffMgr _staffMgr;
        private PositionMgr _positionMgr;
        private ShilfMgr _shilfMgr;
        private ShilfPmMgr _shilfPmMgr;
        private TimeKeepingMgr _timeKeepingMgr;
        private ReceiptMgr _receiptMgr;

        private ModelMaker()
        {
            _staffMgr = new StaffMgr();
            _positionMgr = new PositionMgr();
            _shilfMgr = new ShilfMgr();
            _shilfPmMgr = new ShilfPmMgr();
            _timeKeepingMgr = new TimeKeepingMgr();
            _receiptMgr = new ReceiptMgr();
        }

        //#region Staff
        public Staff CreateStaff(Staff data)
        {
            return _staffMgr.Create(data);
        }

        public Staff UpdateStaff(Staff data)
        {
            return _staffMgr.Update(data);
        }

        public bool DeleteStaff(int data)
        {
            return _staffMgr.Delete(data);
        }

        public Staff GetStaff(int data)
        {
            return _staffMgr.Get(data); ;
        }

        public Staff GetStaff(string code)
        {
            return GetStaffs().First(item => item.code.Equals(code));
        }

        public List<Staff> GetStaffs()
        {
            return _staffMgr.Gets();
        }

        public List<Staff> GetActiveStaffs()
        {
            return _staffMgr.Gets().Where(item => item.status == 1).ToList();
        }

        public List<Staff> SearchStaffs(string data)
        {
            return _staffMgr.Search(data);
        }


        //public bool CheckStaff(string data)
        //{
        //    return _staffMgr.IsExists(data);
        //}

        //#endregion

        #region Position
        public Position CreatePosition(Position data)
        {
            return _positionMgr.Create(data);
        }

        public Position UpdatePosition(Position data)
        {
            return _positionMgr.Update(data);
        }

        //public bool DeletePosition(int data)
        //{
        //    return _positionMgr.Delete(data);
        //}

        public Position GetPosition(int data)
        {
            return _positionMgr.Get(data);
        }

        public Position GetPosition(string data)
        {
            try
            {
                return _positionMgr.Gets().First(item => item.name.Equals(data));
            }
            catch (Exception)
            {
                return _positionMgr.Gets().First();
            }
        }

        public List<Position> GetPositions()
        {
            return _positionMgr.Gets();
        }
        public List<Position> SearchPosition(string data)
        {
            return _positionMgr.Search(data);
        }

        //public bool CheckPosition(string data)
        //{
        //    return _positionMgr.IsExists(data);
        //}

        #endregion

        #region Shilf
        public Shilf CreateShilf(Shilf data)
        {
            return _shilfMgr.Create(data);
        }

        public Shilf UpdateShilf(Shilf data)
        {

            return _shilfMgr.Update(data);
        }

        public bool DeleteShilf(int data)
        {
            return _shilfMgr.Delete(data);
        }

        public Shilf GetShilf(int data)
        {
            return _shilfMgr.Get(data);
        }

        public Shilf GetShilf(Shilf data)
        {
            return _shilfMgr.Get(data);
        }

        //public Shilf GetShilf(string data)
        //{
        //    try
        //    {
        //        return _shilfMgr.Gets().First(item => item.date.Equals(data));
        //    }
        //    catch (Exception)
        //    {
        //        return _shilfMgr.Gets().First();
        //    }
        //}

        //public List<Shilf> GetShilfs()
        //{
        //    return _shilfMgr.Gets();
        //}

        public List<Shilf> SearchShilfs(string data)
        {
            return _shilfMgr.Search(data);
        }

        //public bool CheckShilf(string data)
        //{
        //    return _shilfMgr.IsExists(data);
        //}
        #endregion

        #region ShilfPm
        public ShilfPm CreateShilfPm(ShilfPm data)
        {
            return _shilfPmMgr.Create(data);
        }

        public ShilfPm UpdateShilfPm(ShilfPm data)
        {

            return _shilfPmMgr.Update(data);
        }

        public bool DeleteShilfPm(int data)
        {
            return _shilfPmMgr.Delete(data);
        }

        public ShilfPm GetShilfPm(int data)
        {
            return _shilfPmMgr.Get(data);
        }

        public ShilfPm GetShilfPm(ShilfPm data)
        {
            return _shilfPmMgr.Get(data);
        }

        //public Shilf GetShilf(string data)
        //{
        //    try
        //    {
        //        return _shilfPmMgr.Gets().First(item => item.date.Equals(data));
        //    }
        //    catch (Exception)
        //    {
        //        return _shilfPmMgr.Gets().First();
        //    }
        //}

        //public List<Shilf> GetShilfs()
        //{
        //    return _shilfPmMgr.Gets();
        //}

        public List<ShilfPm> SearchShilfsPm(string data)
        {
            return _shilfPmMgr.Search(data);
        }

        //public bool CheckShilf(string data)
        //{
        //    return _shilfPmMgr.IsExists(data);
        //}
        #endregion

        #region TimeKeeping
        public TimeKeeping CreateTimeKeeping(TimeKeeping data)
        {
            return _timeKeepingMgr.Create(data);
        }

        //public Position UpdatePosition(Position data)
        //{

        //    return _positionMgr.Update(data);
        //}

        //public bool DeletePosition(int data)
        //{
        //    return _positionMgr.Delete(data);
        //}

        //public Position GetPosition(int data)
        //{
        //    return _positionMgr.Get(data);
        //}

        //public Position GetPosition(string data)
        //{
        //    try
        //    {
        //        return _positionMgr.Gets().First(item => item.name.Equals(data));
        //    }
        //    catch (Exception)
        //    {
        //        return _positionMgr.Gets().First();
        //    }
        //}

        public List<TimeKeeping> GetTimeKeepings()
        {
            return _timeKeepingMgr.Gets();
        }

        public List<TimeKeeping> SearchTimeKeeping(string data)
        {
            return _timeKeepingMgr.Search(data);
        }

        //public bool CheckPosition(string data)
        //{
        //    return _positionMgr.IsExists(data);
        //}

        #endregion

        #region Shilf
        public Receipt CreateReceipt(Receipt data)
        {
            return _receiptMgr.Create(data);
        }

        public bool CreateReceipt(List<Receipt> data)
        {
            return _receiptMgr.Create(data);
        }

        //public Receipt UpdateShilf(Receipt data)
        //{

        //    return _shilfMgr.Update(data);
        //}

        //public bool DeleteShilf(int data)
        //{
        //    return _shilfMgr.Delete(data);
        //}

        //public Receipt GetShilf(int data)
        //{
        //    return _shilfMgr.Get(data);
        //}

        //public Receipt GetShilf(string data)
        //{
        //    try
        //    {
        //        return _shilfMgr.Gets().First(item => item.date.Equals(data));
        //    }
        //    catch (Exception)
        //    {
        //        return _shilfMgr.Gets().First();
        //    }
        //}

        //public List<Receipt> GetShilfs()
        //{
        //    return _shilfMgr.Gets();
        //}

        public List<Receipt> SearchReceipt(string fromDate, string toDate)
        {
            return _receiptMgr.Search(fromDate, toDate);
        }

        //public bool CheckShilf(string data)
        //{
        //    return _shilfMgr.IsExists(data);
        //}
        #endregion
    }
}
