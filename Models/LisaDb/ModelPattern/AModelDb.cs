﻿using System.Collections.Generic;

namespace StaffManagement.Models.LisaDb.ModelPattern
{
    internal abstract class AModelDb<T>
    {
        public abstract T Create(T data);
        public abstract T Update(T data);
        public abstract bool Delete(int id);
        public abstract T Get(int id);
        public abstract List<T> Gets();
        public abstract bool IsExists(string data);
        public abstract List<T> Search(string data);
    }
}
