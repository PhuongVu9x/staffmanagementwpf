﻿using StaffManagement.Controllers;
using StaffManagement.Models.LisaDb.Entities;
using StaffManagement.Models.LisaDb.ModelPattern;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Linq;

namespace StaffManagement.Models.LisaDb.Business
{
    internal class ShilfPmMgr : AModelDb<ShilfPm>
    {
        public override ShilfPm Create(ShilfPm data)
        {
            try
            {
                if (IsExists(data))
                    return null;

                Entities.LisaDb.ShilfsPm.Add(data);
                FileControl.Instance.Write(UriId.Instance.ShilfPmTable, Entities.LisaDb.ShilfsPm);
                return data;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public override bool Delete(int id)
        {
            try
            {
                var data = Entities.LisaDb.ShilfsPm.Where(item => item.staff_id == id && item.time.Equals("Off")).ToList();
                foreach (var item in data)
                    Entities.LisaDb.ShilfsPm.Remove(item);

                FileControl.Instance.Write(UriId.Instance.ShilfPmTable, Entities.LisaDb.ShilfsPm);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public override ShilfPm Get(int id)
        {
            return Entities.LisaDb.ShilfsPm.First(item => item.id == id);
        }

        public ShilfPm Get(ShilfPm data)
        {
            return Entities.LisaDb.ShilfsPm.First(item => item.staff_id == data.staff_id && item.date.Equals(data.date));
        }

        public override List<ShilfPm> Gets()
        {
            return Entities.LisaDb.ShilfsPm;
        }

        public override bool IsExists(string data)
        {
            return Entities.LisaDb.ShilfsPm.Any(item => item.date.Equals(data));
        }

        public bool IsExists(ShilfPm data)
        {
            return Entities.LisaDb.ShilfsPm.Any(item => item.date.Equals(data.date) && item.staff_id == data.staff_id);
        }

        public override List<ShilfPm> Search(string data)
        {
            try
            {
                return Entities.LisaDb.ShilfsPm.Where(item => item.id.ToString().Equals(data)).ToList();
            }
            catch (Exception)
            {
                return new List<ShilfPm>();
            }
        }

        public override ShilfPm Update(ShilfPm data)
        {
            try
            {
                var oldData = Get(data.id);
                oldData.time = data.time;

                FileControl.Instance.Write(UriId.Instance.ShilfPmTable, Gets());
                return data;
            }
            catch (EntityException ex)
            {
                MessageControl.Instance.Fail();
                Console.WriteLine(ex);
                return null;
            }
        }
    }
}
