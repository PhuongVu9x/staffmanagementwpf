﻿using StaffManagement.Controllers;
using StaffManagement.Models.LisaDb.Entities;
using StaffManagement.Models.LisaDb.ModelPattern;
using System;
using System.Collections.Generic;
using System.Linq;

namespace StaffManagement.Models.LisaDb.Business
{
    internal class TimeKeepingMgr : AModelDb<TimeKeeping>
    {
        public override TimeKeeping Create(TimeKeeping data)
        {
            try
            {
                if (IsExists(data))
                    return null;

                LisaDb.Entities.LisaDb.TimeKeepings.Add(data);
                FileControl.Instance.Write(UriId.Instance.TimekeepingTable, LisaDb.Entities.LisaDb.TimeKeepings);

                return data;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public override bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public override TimeKeeping Get(int id)
        {
            throw new NotImplementedException();
        }

        public override List<TimeKeeping> Gets()
        {
            throw new NotImplementedException();
        }

        public override bool IsExists(string data)
        {
            return LisaDb.Entities.LisaDb.TimeKeepings.Any(item => item.Code.Equals(data));
        }

        public bool IsExists(TimeKeeping data)
        {
            return LisaDb.Entities.LisaDb.TimeKeepings.Any(item => item.Code.Equals(data.Code) && item.Date == data.Date);
        }

        public override List<TimeKeeping> Search(string data)
        {
            throw new NotImplementedException();
        }

        public override TimeKeeping Update(TimeKeeping data)
        {
            throw new NotImplementedException();
        }
    }
}
