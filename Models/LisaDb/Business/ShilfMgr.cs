﻿using StaffManagement.Controllers;
using StaffManagement.Models.LisaDb.Entities;
using StaffManagement.Models.LisaDb.ModelPattern;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Linq;

namespace StaffManagement.Models.LisaDb.Business
{
    internal class ShilfMgr : AModelDb<Shilf>
    {
        public override Shilf Create(Shilf data)
        {
            try
            {
                if (IsExists(data))
                    return null;

                Entities.LisaDb.Shilfs.Add(data);
                FileControl.Instance.Write(UriId.Instance.ShilfTable, Entities.LisaDb.Shilfs);

                return data;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public override bool Delete(int id)
        {
            try
            {
                var data = Entities.LisaDb.Shilfs.Where(item => item.staff_id == id && item.time.Equals("Off")).ToList();
                foreach (var item in data)
                    Entities.LisaDb.Shilfs.Remove(item);

                FileControl.Instance.Write(UriId.Instance.ShilfTable, Entities.LisaDb.Shilfs);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public override Shilf Get(int id)
        {
            return Entities.LisaDb.Shilfs.First(item => item.id == id);
        }

        public Shilf Get(Shilf data)
        {
            return Entities.LisaDb.Shilfs.First(item => item.staff_id == data.staff_id && item.date.Equals(data.date));
        }

        public override List<Shilf> Gets()
        {
            return Entities.LisaDb.Shilfs;
        }

        public override bool IsExists(string data)
        {
            return Entities.LisaDb.Shilfs.Any(item => item.date.Equals(data));
        }

        public bool IsExists(Shilf data)
        {
            return Entities.LisaDb.Shilfs.Any(item => item.date.Equals(data.date) && item.staff_id == data.staff_id);
        }

        public override List<Shilf> Search(string data)
        {
            try
            {
                return Entities.LisaDb.Shilfs.Where(item => item.id.ToString().Equals(data)).ToList();
            }
            catch (Exception)
            {
                return new List<Shilf>();
            }
        }

        public override Shilf Update(Shilf data)
        {
            try
            {
                var oldData = Get(data.id);
                oldData.time = data.time;

                FileControl.Instance.Write(UriId.Instance.ShilfTable, Gets());
                return data;
            }
            catch (EntityException ex)
            {
                MessageControl.Instance.Fail();
                Console.WriteLine(ex);
                return null;
            }
        }
    }
}
