﻿using Microsoft.EntityFrameworkCore.Internal;
using StaffManagement.Controllers;
using StaffManagement.Models.LisaDb.Entities;
using StaffManagement.Models.LisaDb.ModelPattern;
using System;
using System.Collections.Generic;
using System.Linq;

namespace StaffManagement.Models.LisaDb.Business
{
    internal class PositionMgr : AModelDb<Position>
    {
        public override Position Create(Position data)
        {
            try
            {
                if (IsExists(data.name))
                {
                    MessageControl.Instance.Exists();
                    return null;
                }

                Entities.LisaDb.Positions.Add(data);
                FileControl.Instance.Write(UriId.Instance.PositionTable, Gets());
                return data;
            }
            catch (Exception)
            {
                MessageControl.Instance.Fail();
                return null;
            }
        }

        public override bool Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public override Position Get(int id)
        {
            try
            {
                return Entities.LisaDb.Positions.First(item => item.id == id);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public override List<Position> Gets()
        {
            try
            {
                return Entities.LisaDb.Positions;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public override bool IsExists(string data)
        {
            return Entities.LisaDb.Positions.Any(item => item.name.Equals(data));
        }

        public override List<Position> Search(string data)
        {
            return Entities.LisaDb.Positions.Where(item => item.name.Equals(data)).ToList();
        }

        public override Position Update(Position data)
        {
            try
            {
                var oldData = Get(data.id);
                oldData.name = data.name;
                oldData.status = data.status;

                FileControl.Instance.Write(UriId.Instance.PositionTable, Gets());
                return data;
            }
            catch (Exception)
            {
                MessageControl.Instance.Fail();
                return null;
            }
        }
    }
}
