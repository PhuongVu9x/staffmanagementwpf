﻿using StaffManagement.Controllers;
using StaffManagement.Models.LisaDb.Entities;
using StaffManagement.Models.LisaDb.ModelPattern;
using System;
using System.Collections.Generic;
using System.Linq;

namespace StaffManagement.Models.LisaDb.Business
{
    internal class StaffMgr : AModelDb<Staff>
    {
        public override Staff Create(Staff data)
        {
            try
            {
                if (IsExists(data.name))
                {
                    MessageControl.Instance.Exists();
                    return null;
                }

                Entities.LisaDb.Staffs.Add(data);
                FileControl.Instance.Write(UriId.Instance.StaffTable, Entities.LisaDb.Staffs);
                for (int i = 0; i < 30; i++)
                {
                    var shilf = new Shilf(data.id, DateTime.Now.AddDays(i).ToShortDateString());
                    ModelMaker.Instance.CreateShilf(shilf);
                    var shilfPm = new ShilfPm(data.id, DateTime.Now.AddDays(i).ToShortDateString());
                    ModelMaker.Instance.CreateShilfPm(shilfPm);
                }
                return data;
            }
            catch (Exception)
            {
                MessageControl.Instance.Fail();
                return null;
            }
        }

        public override bool Delete(int id)
        {
            try
            {
                var data = Entities.LisaDb.Staffs.First(item => item.id == id);
                Entities.LisaDb.Staffs.Remove(data);
                FileControl.Instance.Write(UriId.Instance.StaffTable, Entities.LisaDb.Staffs);
                return true;
            }
            catch (Exception)
            {
                MessageControl.Instance.Fail();
                return false;
            }
        }

        public override Staff Get(int id)
        {
            try
            {
                return Entities.LisaDb.Staffs.First(item => item.id == id);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public override List<Staff> Gets()
        {
            try
            {
                return Entities.LisaDb.Staffs.ToList();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public override bool IsExists(string data)
        {
            return Entities.LisaDb.Staffs.Any(item => item.name.Equals(data));
        }

        public override List<Staff> Search(string data)
        {
            try
            {
                return Entities.LisaDb.Staffs.Where(item => item.name.Contains(data)).ToList();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public override Staff Update(Staff data)
        {
            try
            {
                var oldData = Get(data.id);
                oldData.code = data.code;
                oldData.name = data.name;
                oldData.dob = data.dob;
                oldData.staff_id = data.staff_id;
                oldData.position_id = data.position_id;
                oldData.salary = data.salary;
                oldData.allowance = data.allowance;
                oldData.hire_day = data.hire_day;
                oldData.address = data.address;
                oldData.note = data.note;
                oldData.status = data.status;
                oldData.Position = data.Position;

                FileControl.Instance.Write(UriId.Instance.StaffTable, Gets());
                return data;
            }
            catch (Exception)
            {
                MessageControl.Instance.Fail();
                return null;
            }
        }
    }
}
