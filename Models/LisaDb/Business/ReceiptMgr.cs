﻿using StaffManagement.Controllers;
using StaffManagement.Models.LisaDb.Entities;
using StaffManagement.Models.LisaDb.ModelPattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StaffManagement.Models.LisaDb.Business
{
    internal class ReceiptMgr : AModelDb<Receipt>
    {
        public override Receipt Create(Receipt data)
        {
            try
            {
                if (IsExists(data.File))
                {
                    MessageControl.Instance.Exists();
                    return null;
                }

                Entities.LisaDb.Receipts.Add(data);
                FileControl.Instance.Write(UriId.Instance.ReceiptTable, Gets());
                return data;
            }
            catch (Exception)
            {
                MessageControl.Instance.Fail();
                return null;
            }
        }

        public bool Create(List<Receipt> data)
        {
            try
            {
                foreach (var item in data)
                {
                    if (IsExists(item.File))
                        continue;

                    Entities.LisaDb.Receipts.Add(item);
                }

                FileControl.Instance.Write(UriId.Instance.ReceiptTable, Gets());
                return true;
            }
            catch (Exception)
            {
                MessageControl.Instance.Fail();
                return true;
            }
        }

        public override bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public override Receipt Get(int id)
        {
            throw new NotImplementedException();
        }

        public override List<Receipt> Gets()
        {
            return Entities.LisaDb.Receipts.ToList();
        }

        public override bool IsExists(string data)
        {
            return Entities.LisaDb.Receipts.Any(item => item.File.Equals(data));
        }

        public override List<Receipt> Search(string data)
        {
            return Entities.LisaDb.Receipts.Where(item => item.File.Contains(data)).ToList();
        }

        public List<Receipt> Search(string fromDate,string toDate)
        {
            

            return Entities.LisaDb.Receipts.Where(item => item.File.Contains(AppSettings.ConvertDate(fromDate)) 
                                                && item.File.Contains(AppSettings.ConvertDate(toDate))).ToList();
        }

        public override Receipt Update(Receipt data)
        {
            throw new NotImplementedException();
        }
    }
}
