﻿using System;
using System.Windows.Threading;

namespace StaffManagement.Settings
{
    internal class TimeManager
    {
        private static readonly Lazy<TimeManager> lazy = new Lazy<TimeManager>(() => new TimeManager());
        public static TimeManager Instance { get { return lazy.Value; } }

        public void Wait(EventHandler eventHandler, DispatcherTimer stopWatch, int interval)
        {
            stopWatch.Interval = new TimeSpan(0, 0, interval);
            stopWatch.Tick += eventHandler;
            stopWatch.Start();
        }
    }
}
