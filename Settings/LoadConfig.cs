﻿using StaffManagement.Controllers;
using StaffManagement.Models.LisaDb.Entities;
using System;
using System.Collections.Generic;
using ModelMaker = StaffManagement.Models.LisaDb.ModelPattern.ModelMaker;

namespace StaffManagement.Settings
{
    internal class LoadConfig
    {
        private static readonly Lazy<LoadConfig> lazy = new Lazy<LoadConfig>(() => new LoadConfig());
        public static LoadConfig Instance { get { return lazy.Value; } }

        public void LoadData()
        {
            LisaDb.Positions = (List<Position>)(FileControl.Instance.Read<List<Position>>(UriId.Instance.PositionTable) == null ?
                new List<Position>() : FileControl.Instance.Read<List<Position>>(UriId.Instance.PositionTable));

            LisaDb.Staffs = (List<Staff>)(FileControl.Instance.Read<List<Staff>>(UriId.Instance.StaffTable) == null ?
                new List<Staff>() : FileControl.Instance.Read<List<Staff>>(UriId.Instance.StaffTable));

            LisaDb.Shilfs = (List<Shilf>)(FileControl.Instance.Read<List<Shilf>>(UriId.Instance.ShilfTable) == null ?
                new List<Shilf>() : FileControl.Instance.Read<List<Shilf>>(UriId.Instance.ShilfTable));

            LisaDb.ShilfsPm = (List<ShilfPm>)(FileControl.Instance.Read<List<ShilfPm>>(UriId.Instance.ShilfPmTable) == null ?
                new List<ShilfPm>() : FileControl.Instance.Read<List<ShilfPm>>(UriId.Instance.ShilfPmTable));
            
            LisaDb.TimeKeepings = (List<TimeKeeping>)(FileControl.Instance.Read<List<TimeKeeping>>(UriId.Instance.TimekeepingTable) == null ?
                new List<TimeKeeping>() : FileControl.Instance.Read<List<TimeKeeping>>(UriId.Instance.TimekeepingTable));

            LisaDb.Receipts = (List<Receipt>)(FileControl.Instance.Read<List<Receipt>>(UriId.Instance.ReceiptTable) == null ?
                new List<Receipt>() : FileControl.Instance.Read<List<Receipt>>(UriId.Instance.ReceiptTable));

            LisaDb.AppConfig = (AppConfig)FileControl.Instance.Read<AppConfig>(UriId.Instance.AppConfig);
        }

        public void NewShilfs()
        {
            foreach (var item in ModelMaker.Instance.GetActiveStaffs())
            {
                var index = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);
                for (int i = 0; i < index; i++)
                {
                    //var data = new Shilf(item.id, $"2/{i+1}/2023");
                    //ModelMaker.Instance.CreateShilf(data);
                    //var dataPm = new ShilfPm(item.id, $"2/{i + 1}/2023");
                    //ModelMaker.Instance.CreateShilfPm(dataPm);
                    var data = new Shilf(item.id, DateTime.Now.AddDays(i).ToShortDateString());
                    ModelMaker.Instance.CreateShilf(data);
                    var dataPm = new ShilfPm(item.id, DateTime.Now.AddDays(i).ToShortDateString());
                    ModelMaker.Instance.CreateShilfPm(dataPm);
                }
            }
        }
    }
}
