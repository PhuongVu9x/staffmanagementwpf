﻿using MahApps.Metro.Controls;
using Microsoft.Win32;
using StaffManagement.Controllers;
using StaffManagement.Models.LisaDb.Entities;
using StaffManagement.Models.ModelPatterns;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using ModelMaker = StaffManagement.Models.LisaDb.ModelPattern.ModelMaker;
using Position = StaffManagement.Models.LisaDb.Entities.Position;
using ExcelDataReader;
using System.Collections.Generic;
using System.Linq;
using Staff = StaffManagement.Models.LisaDb.Entities.Staff;
using TextBox = System.Windows.Controls.TextBox;
using FastReport;
using FastReport.Export.PdfSimple;

namespace StaffManagement.Views.UserControls
{
    /// <summary>
    /// Interaction logic for AccountView.xaml
    /// </summary>
    public partial class AccountView : UserControl
    {
        public AccountView()
        {
            InitializeComponent();
            SetupDefault();
        }

        private void SetupDefault() =>
            cbPosition.ItemsSource = ModelMaker.Instance.GetPositions();

        private void NewData(object sender, RoutedEventArgs e)
        {
            var position = cbPosition.SelectedItem as Position;
            decimal.TryParse(tbSalary.Text, out decimal train);
            decimal.TryParse(tbSalary.Text, out decimal salary);
            decimal.TryParse(tbAllowance.Text, out decimal allowance);
            var range = new TextRange(tbNote.Document.ContentStart, tbNote.Document.ContentEnd);
            //ModelMaker.Instance.CreateStaff(new Staff(tbCode.Text, tbName.Text, tbDob.Text, tbCccd.Text, position.id, salary, allowance, tbHireDay.Text, tbAddress.Text, range.Text));
            //DataGridControl.Instance.AddAll(ModelViewMaker.Instance.GetStaffs());

            ModelMaker.Instance.CreateStaff(new Staff(tbCode.Text, tbName.Text, tbDob.Text, tbCccd.Text, position.id, train, salary, allowance, tbHireDay.Text, tbAddress.Text, range.Text));
        }
        
        private void ImportExcel(object sender, RoutedEventArgs e)
        {
            var op = new OpenFileDialog();
            op.Title = "Select a File";
            op.Filter = "All supported graphics|*.xls;*.xlsx";
            if (op.ShowDialog() is true)
            {
                var excelFile = op.FileName;
                var file = File.Open(excelFile, FileMode.Open, FileAccess.Read);
                var reader = ExcelReaderFactory.CreateReader(file);
                var result = reader.AsDataSet(new ExcelDataSetConfiguration()
                {
                    ConfigureDataTable = (_) =>
                    {
                        return new ExcelDataTableConfiguration() { };
                    }
                });
                //tbSheet.Text
                var data = reader.AsDataSet().Tables["Nhân viên"];
                var staffs = new List<Staff>();
                for (int i = 2; i < data.Rows.Count; i++)
                {
                    if (data.Rows[i][3].ToString().Split('/').Length < 3)
                        continue;

                    var staff = new Staff();
                    staff.id = LisaDb.Staffs.Count() + 1;
                    staff.name = data.Rows[i][1].ToString();
                    staff.code = data.Rows[i][2].ToString();
                    staff.dob = GetDate(data.Rows[i][3].ToString().Split(' ')[0]);
                    staff.staff_id = data.Rows[i][4].ToString().Substring(1);
                    staff.Position = LisaDb.Positions.First(item => item.name.Equals(data.Rows[i][5].ToString()));
                    staff.position_id = staff.Position.id;
                    var ckSalary = decimal.TryParse(data.Rows[i][6].ToString(),out decimal salary);
                    staff.salary = ckSalary == true ? salary : 0;
                    var ckAllowance = decimal.TryParse(data.Rows[i][7].ToString(), out decimal allowance);
                    staff.allowance = ckAllowance == true ? allowance : 0;
                    staff.hire_day = GetDate(data.Rows[i][8].ToString().Split(' ')[0]);
                    staff.address = data.Rows[i][9].ToString();
                    staff.note = data.Rows[i][10].ToString();
                    staff.status = 1;

                    ModelMaker.Instance.CreateStaff(staff);
                }

                DataGridControl.Instance.AddAll(ModelViewMaker.Instance.GetStaffs(), 1);
            }
        }

        private string GetDate(string text)
        {
            var tmp = text.Split('/');
            return $"{tmp[1]}/{tmp[0]}/{tmp[2]}";
        }
    }
}
