﻿using MahApps.Metro.Controls;
using StaffManagement.Controllers;
using StaffManagement.Models.LisaDb.ModelPattern;
using Syncfusion.Windows.Shared;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;

namespace StaffManagement.Views.UserControls
{
    /// <summary>
    /// Interaction logic for Receipt.xaml
    /// </summary>
    public partial class Receipt : UserControl
    {
        public Receipt()
        {
            InitializeComponent();
            SetupDefaul();
        }

        private void SetupDefaul()
        {
            DataGridControl.Instance.DgReceipt = dgReceiptView;  
        }

        private void ApplySearch(object sender, RoutedEventArgs e)
        {
            if (dpToDate.Text.IsNullOrWhiteSpace())
                return;
            DataGridControl.Instance.AddAll(ModelMaker.Instance.SearchReceipt(dpFromDate.Text, dpToDate.Text));
        }

        private void SelectedDate(object sender, SelectionChangedEventArgs e)
        {
            dpToDate.Text = DateTime.Parse(dpFromDate.Text).AddMonths(1).ToString();
        }

        private void ViewReceipt(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;
            var file = btn.TryFindParent<StackPanel>()
                          .FindChild<TextBlock>("tbReceiptFile").Text;
            Process.Start(file);
        }

        private void DeleteReceipt(object sender, RoutedEventArgs e)
        {

        }
    }
}
