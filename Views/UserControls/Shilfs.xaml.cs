﻿using MahApps.Metro.Controls;
using MaterialDesignThemes.Wpf.Converters;
using Microsoft.Office.Interop.Access;
using StaffManagement.AppThread;
using StaffManagement.Components;
using StaffManagement.Controllers;
using StaffManagement.Models.LisaDb.Entities;
using StaffManagement.Models.ModelPatterns;
using StaffManagement.Settings;
using Syncfusion.Windows.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Threading;
using CheckBox = System.Windows.Controls.CheckBox;
using ModelMaker = StaffManagement.Models.LisaDb.ModelPattern.ModelMaker;
using TextBox = System.Windows.Controls.TextBox;

namespace StaffManagement.Views.UserControls
{
    /// <summary>
    /// Interaction logic for Shilfs.xaml
    /// </summary>
    public partial class Shilfs : UserControl
    {
        public static string CurrentDate = "";
        public static bool IsSwitchShilf = false;


        public Shilfs()
        {
            InitializeComponent();
            DataGridControl.Instance.DgHeader = dgHeader;
            dgHeader.Background = null;
            IsSwitchShilf = (bool)cbSwitchShilf.IsChecked;
            DataGridControl.Instance.SpShilfView = spShilfView;
            DataGridControl.SpShilf = spShilfView;
            //DataGridControl.Instance.DgShilfStaff = dgShilfsView;
            //DataGridControl.Instance.DgShilfPmStaff = dgShilfsPmView;
        }

        DispatcherTimer _stopWatch = new DispatcherTimer();
        private int sec = 3;
        private int interval = 1;
        private List<StaffView> _staffView = new List<StaffView>();
        public void WaitReload(object sender, EventArgs e)
        {
            Console.WriteLine(sec);
            sec--;
            if (sec == 0)
            {
                UpdateData();
                sec = 3;
                _stopWatch.Stop();
            };
        }

        private void DateShilfChange(object sender, System.Windows.Input.KeyEventArgs e)
        {
            ShilfChange(sender, e);
        }

        private void DateLateChange(object sender, KeyEventArgs e)
        {
            LateChange(sender, e);
        }

        private void DateSpecialChange(object sender, RoutedEventArgs e)
        {
            SpecialChange(sender);
        }

        private void SelectedDate(object sender, SelectionChangedEventArgs e)
        {
            CurrentDate = dpDate.Text;
            UpdateData();
        }

        private void LateChange(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                var tb = sender as TextBox;
                int.TryParse(tb.Text, out int late);

                int.TryParse(tb.TryFindParent<StackPanel>().
                                TryFindParent<StackPanel>().
                                TryFindParent<StackPanel>().
                                FindChild<TextBlock>().Text, out int shiftId);

                if ((bool)cbSwitchShilf.IsChecked)
                {
                    var data = ModelMaker.Instance.GetShilf(shiftId);
                    data.time = NewTime(data.time, data.late, late);
                    data.late = late;
                    ModelMaker.Instance.UpdateShilf(data);
                }
                else
                {
                    var data = ModelMaker.Instance.GetShilfPm(shiftId);
                    data.time = NewTime(data.time, data.late, late);
                    data.late = late;
                    ModelMaker.Instance.UpdateShilfPm(data);
                }
                UpdateData();
            }
        }

        private string NewTime(string oldTime, int oldLate, int late)
        {
            if (oldTime.Equals("Off") || oldTime.Equals("On")) return oldTime;

            var textFormat = "{0}-{1}";
            var newLate = 0;
            var newStart = 0;
            var newEnd = 0;
            short.TryParse(oldTime.Split('-')[0].Split(':')[0], out short start);
            short.TryParse(oldTime.Split('-')[0].Split(':')[1], out short end);
            if (late > 0)
            {
                newLate = late + end;
                newStart = start + newLate / 60;
                newEnd = newLate % 60;
            }
            else
            {
                newLate = end - oldLate;
                if (newLate >= 0)
                {
                    newEnd = newLate;
                    newStart = start;
                }
                else
                {
                    newEnd = -newLate;
                    newStart = start - newEnd / 60 - 1;
                }
            }
            var newStartTime = $"{newStart:00}:{newEnd:00}";
            return string.Format(textFormat, newStartTime, oldTime.Split('-')[1]);
        }
        private void SpecialChange(object sender)
        {
            var tb = sender as CheckBox;
            int.TryParse(tb.TryFindParent<StackPanel>().
                            TryFindParent<StackPanel>().
                            TryFindParent<StackPanel>().
                            FindChild<TextBlock>().Text, out int shiftId);
            int.TryParse(tb.TryFindParent<StackPanel>().
                                TryFindParent<StackPanel>().
                                TryFindParent<StackPanel>().
                                FindChild<TextBlock>("tbStaffId").Text, out int staffId);

            if ((bool)cbSwitchShilf.IsChecked)
            {
                var data = ModelMaker.Instance.GetShilf(shiftId);
                data.special = (bool)tb.IsChecked;
                ModelMaker.Instance.UpdateShilf(data);

                var dataPm = ModelMaker.Instance.GetShilfPm(new ShilfPm(staffId, data.date));
                dataPm.special = (bool)tb.IsChecked;
                ModelMaker.Instance.UpdateShilfPm(dataPm);
            }
            else
            {
                var dataPm = ModelMaker.Instance.GetShilfPm(shiftId);
                dataPm.special = (bool)tb.IsChecked;
                ModelMaker.Instance.UpdateShilfPm(dataPm);

                var data = ModelMaker.Instance.GetShilf(new Shilf(staffId, dataPm.date));
                data.special = (bool)tb.IsChecked;
                ModelMaker.Instance.UpdateShilf(data);
            }
            UpdateData();
        }

        private void ShilfChange(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                var tb = sender as TextBox;
                int.TryParse(tb.TryFindParent<StackPanel>().TryFindParent<StackPanel>().FindChild<TextBlock>().Text, out int shiftId);
                if ((bool)cbSwitchShilf.IsChecked)
                {
                    var data = ModelMaker.Instance.GetShilf(shiftId);
                    data.time = DateString(tb.Text);
                    ModelMaker.Instance.UpdateShilf(data);
                }
                else
                {
                    var data = ModelMaker.Instance.GetShilfPm(shiftId);
                    data.time = DateString(tb.Text);
                    ModelMaker.Instance.UpdateShilfPm(data);
                }
                UpdateData();
            }
        }

        private void SwitchShilf(object sender, System.Windows.RoutedEventArgs e)
        {
            IsSwitchShilf = (bool)cbSwitchShilf.IsChecked;
            if (CurrentDate.IsNullOrWhiteSpace())
                return;
            UpdateData();
           
        }

        private void NewShilf(object sender, System.Windows.RoutedEventArgs e)
        {
            LoadConfig.Instance.NewShilfs();
        }

        private void Reload(object sender, System.Windows.RoutedEventArgs e)
        {
            if (CurrentDate.IsNullOrWhiteSpace())
                return;

            UpdateData();
        }

        private async void LoadData()
        {
            Dispatcher.Invoke((Action)(() =>
            {
                
            }));
        }

        BackgroundWorker bw;

        private void UpdateData()
        {
            //ShilfViewThread.data = ModelViewMaker.Instance.GetStaffs(dpDate.Text);
            //DataGridControl.Instance.AddAll(ModelViewMaker.Instance.GetStaffs(dpDate.Text));
            DataGridControl.Instance.AddLayoutDgShilf();
            //var data = ModelViewMaker.Instance.GetStaffs(dpDate.Text);
            //var dgTest = DataGridControl.Instance.SpShilfView.FindChild<DataGrid>();
            //foreach (var item in data)
            //    dgTest.Items.Add(item);
            EventControl.EmitData("Test1", dpDate.Text);
            var thread = new ShilfViewThread();
            //thread.Start(ModelViewMaker.Instance.GetStaffs(dpDate.Text),spShilfView, (bool)cbSwitchShilf.IsChecked);
            thread.Start();
            //bw = new BackgroundWorker();
            //bw.WorkerReportsProgress = true;
            //bw.DoWork += (obj, ea) => LoadData();
            //bw.RunWorkerAsync();
        }

        private string GetDate
        {
            get
            {
                return dpDate.Text;
            }
        }

        private string DateString(string text)
        {
            if (text.Equals("") || text.ToLower().Equals("off"))
                return "Off";
            else if (text.ToLower().Equals("on"))
                return "On";
            return text;
        }
    }
}
