﻿using ExcelDataReader;
using Microsoft.Win32;
using StaffManagement.Controllers;
using StaffManagement.Models.LisaDb.Entities;
using StaffManagement.Models.ModelPatterns;
using Syncfusion.Windows.Shared;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using ModelMaker = StaffManagement.Models.LisaDb.ModelPattern.ModelMaker;
using OpenFileDialog = Microsoft.Win32.OpenFileDialog;
using UserControl = System.Windows.Controls.UserControl;

namespace StaffManagement.Views.UserControls
{
    /// <summary>
    /// Interaction logic for Salary.xaml
    /// </summary>
    public partial class Salary : UserControl
    {
        public Salary()
        {
            InitializeComponent();
            SetupDefault();
        }

        private void SetupDefault()
        {
            DataGridControl.Instance.DgSalary = dgSalaryView;
        }

        private void ImportExcel(object sender, RoutedEventArgs e)
        {
            var op = new OpenFileDialog();
            op.Title = "Select a File";
            op.Filter = "All supported graphics|*.xls;*.xlsx";
            if (op.ShowDialog() is true)
            {
                var excelFile = op.FileName;
                var file = File.Open(excelFile, FileMode.Open, FileAccess.Read);
                var reader = ExcelReaderFactory.CreateReader(file);
                var result = reader.AsDataSet(new ExcelDataSetConfiguration()
                {
                    ConfigureDataTable = (_) =>
                    {
                        return new ExcelDataTableConfiguration() { };
                    }
                });
                //tbSheet.Text
                var data = reader.AsDataSet().Tables["Chi tiết"];
                var timeKeepings = new List<TimeKeeping>();
                for (int i = 4; i < data.Rows.Count; i++)
                {
                    if (data.Rows[i][7].ToString().Equals(""))
                        continue;

                    var tk = new TimeKeeping();
                    tk.Id = LisaDb.TimeKeepings.Count() + 1;
                    tk.Code = data.Rows[i][1].ToString();
                    tk.Date = data.Rows[i][5].ToString();
                    //tk.TimeIn1 = GetDate(tk.Date, data.Rows[i][7].ToString());
                    //tk.TimeOut1 = GetDate(tk.Date, data.Rows[i][8].ToString());
                    //tk.TimeIn2 = GetDate(tk.Date, data.Rows[i][9].ToString());
                    //tk.TimeOut2 = GetDate(tk.Date, data.Rows[i][10].ToString());
                    //tk.Hours = GetHours(tk.TimeIn1, tk.TimeOut1, tk.TimeIn2, tk.TimeOut2);
                    ModelMaker.Instance.CreateTimeKeeping(tk);
                }

                //DataGridControl.Instance.AddAll(ModelViewMaker.Instance.GetStaffs(), 1);
            }
        }

        public float GetHours(string TimeIn1, string TimeOut1, string TimeIn2, string TimeOut2)
        {
            TimeSpan tmp1;
            TimeSpan tmp2;
            float hours = 0;

            if (!TimeIn1.Equals("") && !TimeOut1.Equals(""))
            {
                DateTime.TryParse(TimeIn1, out DateTime timeIn1);
                DateTime.TryParse(TimeOut1, out DateTime timeOut1);
                tmp1 = timeOut1 - timeIn1;
                hours += tmp1.Hours + tmp1.Minutes / 60f;
            }

            if (!TimeIn2.Equals("") && !TimeOut2.Equals(""))
            {
                DateTime.TryParse(TimeIn2, out DateTime timeIn2);
                DateTime.TryParse(TimeOut2, out DateTime timeOut2);
                tmp2 = timeOut2 - timeIn2;
                hours += tmp2.Hours + tmp2.Minutes / 60f;
            }
            return hours;
        }

        private string GetDate(string date, string text)
        {
            DateTime.TryParse(text, out DateTime dateTime);
            DateTime.TryParse(date, out DateTime currentDate);
            if(text.Length > 5 || text.Equals("")) return text;
            var tmpText = text.Split(':');
            var time = (int.Parse(tmpText[0]) > 12 && int.Parse(tmpText[1]) > 0) == true? "PM" : "AM";
            return $"{currentDate.Month}/{currentDate.Day}/{currentDate.Year} {text}:00 {time}";
        }

        private void SelectedDate(object sender, SelectionChangedEventArgs e)
        {
            dpToDate.Text = DateTime.Parse(dpFromDate.Text).AddMonths(1).ToString();
        }

        private void ApplySearch(object sender, RoutedEventArgs e)
        {
            if (dpToDate.Text.IsNullOrWhiteSpace())
                return;
            DataGridControl.Instance.AddAll(ModelViewMaker.Instance.SearchSalarys(dpFromDate.Text, dpToDate.Text));
        }

        private void ExportAll(object sender, RoutedEventArgs e)
        {
            if (dpToDate.Text.IsNullOrWhiteSpace())
                return;

            var openDialog = new FolderBrowserDialog();

            if(openDialog.ShowDialog() is DialogResult.OK)
                AppSettings.ExportSalaryPdf(ModelViewMaker.Instance.SearchSalarys(dpFromDate.Text, dpToDate.Text), dpFromDate.Text, dpToDate.Text, openDialog.SelectedPath);

            MessageControl.Instance.Success();
        }

        private void PrintAll(object sender, RoutedEventArgs e)
        {

        }
    }
}
