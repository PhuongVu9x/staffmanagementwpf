﻿using StaffManagement.Controllers;
using System.Windows;
using System.Windows.Controls;

namespace StaffManagement.Views.UserControls
{
    /// <summary>
    /// Interaction logic for Navigator.xaml
    /// </summary>
    public partial class Navigator : UserControl
    {
        public Navigator()
        {
            InitializeComponent();
        }

        private void ShilfsView(object sender, RoutedEventArgs e)
        {
            UIFlow.Instance.SwitchMainView(UIID.Instance.Shilfs);
        }

        private void StaffsView(object sender, RoutedEventArgs e)
        {
            UIFlow.Instance.SwitchMainView(UIID.Instance.Staffs);
        }

        private void SalaryView(object sender, RoutedEventArgs e)
        {
            UIFlow.Instance.SwitchMainView(UIID.Instance.Salary);
        }

        private void ReceiptsView(object sender, RoutedEventArgs e)
        {
            UIFlow.Instance.SwitchMainView(UIID.Instance.Receipts);
        }
    }
}
