﻿using MahApps.Metro.Controls;
using StaffManagement.Controllers;
using StaffManagement.Models.LisaDb.Entities;
using StaffManagement.Models.ModelPatterns;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ModelMaker = StaffManagement.Models.LisaDb.ModelPattern.ModelMaker;
using Position = StaffManagement.Models.LisaDb.Entities.Position;

namespace StaffManagement.Views.UserControls
{
    /// <summary>
    /// Interaction logic for PositionView.xaml
    /// </summary>
    public partial class PositionView : UserControl
    {
        public PositionView()
        {
            InitializeComponent();
            SetupDefault();
        }

        private void SetupDefault()
        {
            DataGridControl.Instance.DgPosition = dgPosition;
            DataGridControl.Instance.AddAll(ModelMaker.Instance.GetPositions());
        }

        private void NewData(object sender, RoutedEventArgs e)
        {
            ModelMaker.Instance.CreatePosition(new Position(tbDataName.Text));
            DataGridControl.Instance.AddAll(ModelMaker.Instance.GetPositions());
            //ModelMaker.Instance.CreatePosition(new Position(tbDataName.Text));
            //DataGridControl.Instance.AddAll(ModelMaker.Instance.GetPositions());
        }

        private void DataUpdateName(object sender, KeyEventArgs e)
        {
            if (e.Key is Key.Return)
            {
                //var tb = sender as TextBox;
                //int.TryParse(tb.TryFindParent<StackPanel>().FindChild<TextBlock>().Text, out int dataId);
                //System.Console.WriteLine(dataId);
                //var data = ModelMaker.Instance.GetPosition(dataId);
                //data.name = tb.Text;
                //ModelMaker.Instance.UpdatePosition(data);
                //DataGridControl.Instance.AddAll(ModelMaker.Instance.GetPositions());

                Reload(sender);
            }
        }

        private void DataUpdateStatus(object sender, RoutedEventArgs e)
        {
            var cb = sender as CheckBox;
            int.TryParse(cb.FindChild<TextBlock>().Text, out int dataId);
            var data = ModelMaker.Instance.GetPosition(dataId);
            data.status = (bool)cb.IsChecked;
            ModelMaker.Instance.UpdatePosition(data);
            DataGridControl.Instance.AddAll(ModelMaker.Instance.GetPositions());
        }

        private void DataSearch(object sender, TextChangedEventArgs e)
        {
            DataGridControl.Instance.AddAll(ModelMaker.Instance.SearchPosition(tbDataSearch.Text));
        }

        private Position GetData(TextBox sender)
        {
            int.TryParse(sender.TryFindParent<StackPanel>().FindChild<TextBlock>().Text, out int dataId);
            return ModelMaker.Instance.GetPosition(dataId);
        }

        private void Reload(object sender)
        {
            var tb = sender as TextBox;
            var data = GetData(tb);
            data.name = tb.Text;
            ModelMaker.Instance.UpdatePosition(data);
            DataGridControl.Instance.AddAll(ModelMaker.Instance.GetPositions());
        }
    }
}
