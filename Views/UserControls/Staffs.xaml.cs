﻿using StaffManagement.Controllers;
using System.Windows;
using System.Windows.Controls;

namespace StaffManagement.Views.UserControls
{
    /// <summary>
    /// Interaction logic for Staffs.xaml
    /// </summary>
    public partial class Staffs : UserControl
    {
        public Staffs()
        {
            InitializeComponent();
            UIFlow.Instance.SetupSubView(grdSubView);
            UIFlow.Instance.SwitchSubView(UIID.Instance.AccountView);
        }

        private void AccountView(object sender, RoutedEventArgs e)
        {
            UIFlow.Instance.SwitchSubView(UIID.Instance.AccountView);
        }

        private void PositionView(object sender, RoutedEventArgs e)
        {
            UIFlow.Instance.SwitchSubView(UIID.Instance.PositionView);
        }

        private void StaffView(object sender, RoutedEventArgs e)
        {
            UIFlow.Instance.SwitchSubView(UIID.Instance.StaffView);
        }
    }
}
