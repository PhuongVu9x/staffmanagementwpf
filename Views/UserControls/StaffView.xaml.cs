﻿using MahApps.Metro.Controls;
using StaffManagement.Controllers;
using StaffManagement.Models.LisaDb.Entities;
using StaffManagement.Models.ModelPatterns;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ModelMaker = StaffManagement.Models.LisaDb.ModelPattern.ModelMaker;

namespace StaffManagement.Views.UserControls
{
    /// <summary>
    /// Interaction logic for StaffView.xaml
    /// </summary>
    public partial class StaffView : UserControl
    {
        public StaffView()
        {
            InitializeComponent();
            SetupDefault();
        }

        private void SetupDefault()
        {
            DataGridControl.Instance.DgStaff = dgStaff;
            DataGridControl.Instance.AddAll(ModelViewMaker.Instance.GetStaffs(), 1);
        }

        private void DataSearch(object sender, KeyEventArgs e)
        {
            if (e.Key is Key.Return)
                DataGridControl.Instance.AddAll(ModelViewMaker.Instance.SearchStaff(tbDataSearch.Text), 1);
        }

        private void DataUpdateCode(object sender, KeyEventArgs e)
        {
            if (e.Key is Key.Return)
            {
                var tb = sender as TextBox;
                var data = GetStaff(tb);
                data.code = tb.Text;
                Reload(data);
            }
        }

        private void DataUpdateName(object sender, KeyEventArgs e)
        {
            if (e.Key is Key.Return)
            {
                var tb = sender as TextBox;
                var data = GetStaff(tb);
                data.name = tb.Text;
                Reload(data);
            }
        }

        private void DataUpdateStatus(object sender, RoutedEventArgs e)
        {
            var cb = sender as CheckBox;
            int.TryParse(cb.FindChild<TextBlock>().Text, out int dataId);
            //var data = ModelMaker.Instance.GetStaff(dataId);
            var data = ModelMaker.Instance.GetStaff(dataId);
            data.status = (byte)(cb.IsChecked is true ? 1 : 0);
            Reload(data);
        }

        private void DataUpdatePosition(object sender, KeyEventArgs e)
        {
            if (e.Key is Key.Return)
            {
                var tb = sender as TextBox;
                var data = GetStaff(tb);
                var position = ModelMaker.Instance.GetPosition(tb.Text);
                data.position_id = position.id;
                data.Position = position;
                Reload(data);
            }
        }

        private void DataUpdateHireDay(object sender, KeyEventArgs e)
        {
            if (e.Key is Key.Return)
            {
                var tb = sender as TextBox;
                var data = GetStaff(tb);
                data.hire_day = tb.Text;
                Reload(data);
            }
        }
        private void DataUpdateTrain(object sender, KeyEventArgs e)
        {
            if (e.Key is Key.Return)
            {
                var tb = sender as TextBox;
                var data = GetStaff(tb);
                decimal.TryParse(tb.Text, out decimal money);
                data.train = money;
                Reload(data);
            }
        }

        private void DataUpdateSalary(object sender, KeyEventArgs e)
        {
            if (e.Key is Key.Return)
            {
                var tb = sender as TextBox;
                var data = GetStaff(tb);
                decimal.TryParse(tb.Text, out decimal money);
                data.salary = money;
                Reload(data);
            }
        }

        private void DataUpdateAllowance(object sender, KeyEventArgs e)
        {
            if (e.Key is Key.Return)
            {
                var tb = sender as TextBox;
                var data = GetStaff(tb);
                decimal.TryParse(tb.Text, out decimal money);
                data.allowance = money;
                Reload(data);
            }
        }

        private void DataUpdateNote(object sender, KeyEventArgs e)
        {
            if (e.Key is Key.Return)
            {
                //var tb = sender as TextBox;
                //var data = GetStaff(tb);
                //data.note = tb.Text;
                //Reload(sender);
            }
        }

        private Staff GetStaff(TextBox sender)
        {
            int.TryParse(sender.TryFindParent<StackPanel>().FindChild<TextBlock>().Text, out int dataId);
            return ModelMaker.Instance.GetStaff(dataId);
        }

        private void Reload(Staff data)
        {
            ModelMaker.Instance.UpdateStaff(data);
            DataGridControl.Instance.AddAll(ModelViewMaker.Instance.GetStaffs(), 1);
            FileControl.Instance.Write(UriId.Instance.StaffTable, LisaDb.Staffs);
        }

        private void DeleteAccount(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;
            int.TryParse(btn.TryFindParent<StackPanel>().FindChild<TextBlock>().Text, out int dataId);
            ModelMaker.Instance.DeleteShilf(dataId);
            ModelMaker.Instance.DeleteShilfPm(dataId);
            ModelMaker.Instance.DeleteStaff(dataId);
            DataGridControl.Instance.AddAll(ModelViewMaker.Instance.GetStaffs(), 1);
        }
    }
}
