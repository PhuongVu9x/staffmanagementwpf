﻿using StaffManagement.Models.Entities;
using System;
using System.Data.Entity.Core;
using System.Linq;
using System.Threading;

namespace StaffManagement.AppThread
{
    internal class CreateShilfThread
    {
        private static Lazy<CreateShilfThread> lazy = new Lazy<CreateShilfThread>(() => new CreateShilfThread());
        public static CreateShilfThread Instance { get { return lazy.Value; } }

        public void Start()
        {
            var thread = new Thread(Wait);
            thread.Start();
        }

        private lisaCoffeeEntities db = new lisaCoffeeEntities();

        private void Wait()
        {
            foreach (var item in db.Staffs)
            {
                var index = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);
                for (int i = 0; i < index; i++)
                {
                    Console.WriteLine(item.id);
                    Console.WriteLine(DateTime.Now.AddDays(i).ToShortDateString());

                    var data = new Shilf(item.id, DateTime.Now.AddDays(i).ToShortDateString());
                    try
                    {
                        if (IsExists(data))
                            continue;
                        db.Shilfs.Add(data);
                        db.SaveChanges();
                    }
                    catch (EntityException ex)
                    {
                        Console.WriteLine(ex);
                        continue;
                    }
                }
            }
        }

        private bool IsExists(Shilf data)
        {
            return db.Shilfs.Any(item => item.date.Equals(data.date) && item.staff_id == data.staff_id && item.time.Equals(data.time));
        }
    }
}
