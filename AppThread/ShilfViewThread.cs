﻿using MahApps.Metro.Controls;
using StaffManagement.Components;
using StaffManagement.Controllers;
using StaffManagement.Models.LisaDb.Entities;
using StaffManagement.Models.ModelPatterns;
using StaffManagement.Views.UserControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Controls;

namespace StaffManagement.AppThread
{
    internal class ShilfViewThread
    {
        public List<Models.ModelView.StaffView> data = new List<Models.ModelView.StaffView>();

        private StackPanel spShilf;
        private bool isSwitchShilf;
        public void Start(List<Models.ModelView.StaffView> data,StackPanel spShilf, bool isSwitchShilf)
        {
            //this.data = data;
            //this.spShilf = spShilf;
            //this.isSwitchShilf = isSwitchShilf;
            var thread = new Thread(ToDo);
            thread.Start();
        }

        public void Start()
        {
            var thread = new Thread(ToDo);
            thread.Start();
        }

        private void ToDo()
        {
            var date = EventControl.GetData<string>("Test1");
            var allStaff = (List<Staff>)(FileControl.Instance.Read<List<Staff>>(UriId.Instance.StaffTable) == null ?
                new List<Staff>() : FileControl.Instance.Read<List<Staff>>(UriId.Instance.StaffTable));
            var activeStaff = allStaff.Where(item => item.status == 1).ToList();
            
            var data = activeStaff.Select(item => new Models.ModelView.StaffView(item, date)).ToList().OrderBy(item => item.Code).ToList();
            DataGridControl.Instance.SpShilfView.Invoke(new Action(() =>
            {
                var dgTest = DataGridControl.Instance.SpShilfView.FindChild<DataGrid>();
                dgTest.ItemsSource = data;
                //foreach (var item in data)
                //{
                //    dgTest.Items.Add(item);
                //}
            }));
        }
    }
}
