﻿using StaffManagement.Controllers;
using StaffManagement.Settings;
using System.Windows;

namespace StaffManagement
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Setup();
        }

        private void Setup()
        {
            LoadConfig.Instance.LoadData();
            LoadConfig.Instance.NewShilfs();
            UIFlow.Instance.SetupMainView(grdMainView);
            UIFlow.Instance.SwitchMainView(UIID.Instance.Shilfs);
            UIFlow.Instance.SetupNavigator(grdNavigator);
        }
    }
}
